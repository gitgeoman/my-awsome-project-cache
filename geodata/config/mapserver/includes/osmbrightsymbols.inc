#-------------------------------------------------------------------------------
# OSM SYMBOLS
#-------------------------------------------------------------------------------

SYMBOL
  NAME img-icon-oneway-reverse-svg
  IMAGE "/opt/mapserver/includes/symbols/osmbright/icon/oneway-reverse.svg"
  TYPE svg
END
SYMBOL
  NAME img-icon-oneway-svg
  IMAGE "/opt/mapserver/includes/symbols/osmbright/icon/oneway.svg"
  TYPE svg
END
SYMBOL
  NAME img-shield-motorway-1-png
  IMAGE "/opt/mapserver/includes/symbols/osmbright/shield-motorway-1.png"
  TYPE pixmap
END
SYMBOL
  NAME img-icon-rail-18-png
  IMAGE "/opt/mapserver/includes/symbols/osmbright/icon/rail-18.png"
  TYPE pixmap
END
SYMBOL
  NAME img-icon-rail-12-png
  IMAGE "/opt/mapserver/includes/symbols/osmbright/icon/rail-12.png"
  TYPE pixmap
END
SYMBOL
  NAME img-marsh-16-png
  IMAGE "/opt/mapserver/includes/symbols/osmbright/marsh-16.png"
  TYPE pixmap
END
SYMBOL
  NAME img-marsh-32-png
  IMAGE "/opt/mapserver/includes/symbols/osmbright/marsh-32.png"
  TYPE pixmap
END
