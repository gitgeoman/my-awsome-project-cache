#-------------------------------------------------------------------------------
# DEFAULT SYMBOLS
#-------------------------------------------------------------------------------

SYMBOL
  NAME "hatch"
  TYPE HATCH
END

SYMBOL
  NAME   "circle"
  TYPE   ELLIPSE
  FILLED TRUE
  POINTS
    1 1
  END
END

SYMBOL
  NAME    "square"
  TYPE    VECTOR
  FILLED  TRUE
  POINTS
      0 1
      0 0
      1 0
      1 1
      0 1
  END
END

SYMBOL
  NAME    "rectangle"
  TYPE    VECTOR
  FILLED  TRUE
  POINTS
      0 0.5
      0 0
      2 0
      2 0.5
      0 0.5
  END
END

SYMBOL
  NAME  "triangle"
  TYPE  VECTOR
  POINTS
    0 4
    2 0
    4 4
    0 4
  END
END

SYMBOL
  NAME  "triangle_filled"
  TYPE  VECTOR
  FILLED TRUE
  POINTS
    0 4
    2 0
    4 4
    0 4
  END
END

SYMBOL
  NAME  "cross"
  TYPE  VECTOR
  POINTS
    2.0 0.0
    2.0 4.0
    -99 -99
    0.0 2.0
    4.0 2.0
  END
END

SYMBOL
  NAME "star"
  TYPE VECTOR
  FILLED TRUE
  POINTS
    0 .375
    .35 .375
    .5 0
    .65 .375
    1 .375
    .75 .625
    .875 1
    .5 .75
    .125 1
    .25 .625
  END
END 

SYMBOL
  NAME "marsh_poly"
  TYPE vector
  FILLED true
  POINTS
    # Half line
    0 2
    4.5 2
    4.5 3
    0 3
    0 2
    -99 -99
    # Half line
    7 2
    11.5 2
    11.5 3
    7 3
    7 2
    -99 -99
    # Hole line
    1.25 5
    10.25 5
    10.25 6
    1.25 6
    1.25 5
  END
END

#-------------------------------------------------------------------------------
# SVG SYMBOLS
#-------------------------------------------------------------------------------

SYMBOL
	NAME "change_detection_less_2m"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/change_detection_less_2m.svg"
END

SYMBOL
	NAME "change_detection_more_2m"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/change_detection_more_2m.svg"
END

SYMBOL
	NAME "muinaisjaannos"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/muinaisjaannos.svg"
END

SYMBOL
	NAME "kiintea_muinaisjaannos"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/kiintea_muinaisjaannos.svg"
END

SYMBOL
	NAME "muu_kulttuuriperintokohde"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/muu_kulttuuriperintokohde.svg"
END

SYMBOL
	NAME "muinaisjaannos_cluster"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/muinaisjaannos_cluster.svg"
END

SYMBOL
	NAME "rky_piste"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/rky_piste.svg"
END

SYMBOL
	NAME "lehtimetsa"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/Slm.svg"
END

SYMBOL
    NAME "havumetsa"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Shm.svg"
END

SYMBOL
    NAME "sekametsa"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Ssm.svg"
END

SYMBOL
    NAME "masto"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Smasto.svg"
END

SYMBOL
    NAME "peruskartta_masto"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/SPKmasto.svg"
END

SYMBOL
    NAME "sahkopylvas"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Sahkopylvas.svg"
END

SYMBOL
    NAME "kivikko"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Pkivikko_0_0_60.svg"
END

SYMBOL
    NAME "hietikko"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Phko.svg"
END

SYMBOL
    NAME "lahde"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/Slahde.svg"
END

SYMBOL
    NAME "kiinteistotunnus"
    TYPE svg
    IMAGE "/opt/mapserver/includes/symbols/kiinteistotunnus.svg"
END

SYMBOL
	NAME "uhanalaislaji"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/uhanalaislaji.svg"
END

SYMBOL
	NAME "se_metry_hcv_cluster"
	TYPE svg
	IMAGE "/opt/mapserver/includes/symbols/se_metry_hcv_cluster.svg"
END