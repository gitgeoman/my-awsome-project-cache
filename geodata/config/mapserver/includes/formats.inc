# OGR OUTPUT FORMATS

OUTPUTFORMAT
  NAME          "GEOJSON"
  DRIVER        "OGR/GEOJSON"
  MIMETYPE      "application/json"
  FORMATOPTION  "STORAGE=memory"
  FORMATOPTION  "FORM=simple"
  FORMATOPTION  "USE_FEATUREID=true"
END

OUTPUTFORMAT
  NAME          "CSV"
  DRIVER        "OGR/CSV"
  MIMETYPE      "text/csv"
  FORMATOPTION  "LCO:GEOMETRY=AS_WKT"
  FORMATOPTION  "STORAGE=memory"
  FORMATOPTION  "FORM=simple"
  FORMATOPTION  "FILENAME=result.csv"
END

OUTPUTFORMAT
  NAME          "SHAPEZIP"
  DRIVER        "OGR/ESRI Shapefile"
  MIMETYPE      "application/zip; subtype=shapezip"
  FORMATOPTION  "STORAGE=memory"
  FORMATOPTION  "FORM=zip"
  FORMATOPTION  "FILENAME=result.zip"
END

OUTPUTFORMAT
  NAME          "SPATIALITEZIP"
  DRIVER        "OGR/SQLITE"
  MIMETYPE      "application/zip; subtype=spatialitezip"
  FORMATOPTION  "DSCO:SPATIALITE=YES"
  FORMATOPTION  "STORAGE=memory"
  FORMATOPTION  "FORM=zip"
  FORMATOPTION  "FILENAME=result.db.zip"
END

# RASTER OUTPUT FORMATS

OUTPUTFORMAT
  NAME          "png"
  DRIVER        "AGG/PNG"
  MIMETYPE      "image/png"
  IMAGEMODE     "RGB"
  EXTENSION     "png"
  FORMATOPTION  "GAMMA=0.75"
END

OUTPUTFORMAT
  NAME          "gif"
  DRIVER        "GD/GIF"
  MIMETYPE      "image/gif"
  IMAGEMODE     "PC256"
  EXTENSION     "gif"
END

OUTPUTFORMAT
  NAME          "png8"
  DRIVER        "AGG/PNG8"
  MIMETYPE      "image/png8"
  IMAGEMODE     "RGB"
  EXTENSION     "png"
  FORMATOPTION  "QUANTIZE_FORCE=on"
  FORMATOPTION  "QUANTIZE_COLORS=256"
  FORMATOPTION  "GAMMA=0.75"
END

OUTPUTFORMAT
  NAME          "jpeg"
  DRIVER        "AGG/JPEG"
  MIMETYPE      "image/jpeg"
  IMAGEMODE     "RGB"
  EXTENSION     "jpg"
  FORMATOPTION  "GAMMA=0.75"
  FORMATOPTION  "QUALITY=75"
END

OUTPUTFORMAT
  NAME          "svg"
  DRIVER        "CAIRO/SVG"
  MIMETYPE      "image/svg+xml"
  IMAGEMODE     "RGB"
  EXTENSION     "svg"
END

OUTPUTFORMAT
  NAME          "pdf"
  DRIVER        "CAIRO/PDF"
  MIMETYPE      "application/x-pdf"
  IMAGEMODE     "RGB"
  EXTENSION     "pdf"
END