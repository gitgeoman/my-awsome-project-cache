#-----------------------------------------------------
# Demonstration maplayers: hpr layer group
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 600000

    NAME          hpr_track

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       1042006.61 6486461.57 1053076.28 6494814.70

    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "HPR Track"
        "ows_abstract"          "HPR Track"
        "ows_group_title"       "HPR Layer Group"
        "ows_group_abstract"    "HPR Layer Group"

        "wms_srs"               "EPSG:3857"
        "wms_enable_request"    "*"
        "wms_include_items"     "all"
        "WMS_LAYER_GROUP"       "/hpr_layer_group/hpr_track/"

        "wfs_srs"               "EPSG:3857"
        "wfs_enable_request"    "*"
        "gml_include_items"     "all"
        "gml_featureid"         "bag_id"

    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        SELECT
            bag_id,
            fid,
            total_volume,
            pine_volume,
            spruce_volume,
            beech_volume,
            other_volume,
            total_stems,
            pine_stems,
            spruce_stems,
            beech_stems,
            other_stems,
            mean_size,
            geom
        FROM
            bag_ai_hpr_pilot.hpr_track
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 75
    END

    CLASSITEM "bag_id"

    CLASS
        NAME "Track"
        STYLE
            COLOR "#eba0ac"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 600000
    
    NAME          hpr_trees_buffer
    
    TYPE          POLYGON
    TEMPLATE      "foo"

    EXTENT       909598.00 6485931.68 1133939.73 6662348.12
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"             "HPR Trees Buffer"
        "ows_abstract"          "HPR Trees Buffer"
        "ows_group_title"       "HPR Layer Group"
        "ows_group_abstract"    "HPR Layer Group"

        "wms_srs"               "EPSG:3857"
        "wms_enable_request"    "*"
        "wms_include_items"     "all"
        "WMS_LAYER_GROUP"       "/hpr_layer_group/hpr_trees_buffer/"

        "wfs_srs"               "EPSG:3857"
        "wfs_enable_request"    "*"
        "gml_include_items"     "all"
        "gml_featureid"         "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        SELECT
            bag_id,
            fid,
            total_volume,
            pine_volume,
            spruce_volume,
            beech_volume,
            other_volume,
            total_stems,
            pine_stems,
            spruce_stems,
            beech_stems,
            other_stems,
            geom
        FROM
            bag_ai_hpr_pilot.hpr_points_buffer
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 90
    END

    CLASSITEM "bag_id"

    CLASS
        NAME "Tree Buffer"
        STYLE
            COLOR "#865691"
            OUTLINECOLOR "#000000"
        END
    END
END