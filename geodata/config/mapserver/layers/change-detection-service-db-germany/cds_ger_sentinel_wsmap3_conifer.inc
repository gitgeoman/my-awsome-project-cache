LAYER
    STATUS          OFF
    # MINSCALEDENOM   100000

    NAME            "cds_ger_sentinel_wsmap3_conifer_tileindex"

    METADATA
        "ows_title"             "CDS Sentinel WSMAP3 CONIFER (Germany)"
        "ows_group_title"       "CDS Sentinel WSMAP3 CONIFER"
        "ows_group_abstract"    "CDS Sentinel WSMAP3 CONIFER"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities"
        "gml_include_items"     "date"
        #"WMS_LAYER_GROUP"       "/cds_eu_sentinel_wsmap3_conifer/cds_ger_sentinel_wsmap3_conifer"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
 
   DATA "geometry from (
        SELECT
            bag_id, 
            location, 
            src_srs, 
            geom as geometry, 
            date
        FROM
            bag_cds_tileindex.tileindex_wsmap3_con_ger
        where  
            date::date >= '2022-05-09'
    ) as foo using unique bag_id using srid=3857"

    # LABELITEM "date"

    # COMPOSITE
    #    OPACITY 50
    # END

    # CLASS
    #    STYLE
    #        OUTLINECOLOR "#808080"
    #        COLOR "#b2df8a"
    #        WIDTH 2
    #    END
    #    LABEL
    #        MINSCALEDENOM 5000
    #        MAXSCALEDENOM 200000
    #        FONT scb
    #        TYPE truetype
    #        SIZE 10
    #        COLOR "#746E70"
    #        OUTLINECOLOR "#FFFFFF"
    #        OUTLINEWIDTH 1
    #        ALIGN CENTER
    #    END
    # END
END

LAYER
    STATUS          OFF
    MAXSCALEDENOM   200000
    
    NAME            "cds_ger_sentinel_wsmap3_conifer"
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "CDS Sentinel WSMAP3 CONIFER (Germany)"
        "ows_group_title"       "CDS Sentinel WSMAP3 CONIFER"
        "ows_group_abstract"    "CDS Sentinel WSMAP3 CONIFER"
        "wms_srs"               "EPSG:3857"
        "gml_include_items"     "all"
        "WMS_LAYER_GROUP"       "/cds_eu_sentinel_wsmap3_conifer/cds_ger_sentinel_wsmap3_conifer"
    END

    TILEINDEX  "cds_ger_sentinel_wsmap3_conifer_tileindex"
    TILEITEM   "location"

    PROCESSING "RESAMPLE=NEAREST"

    CLASS
        NAME "WSMAP3 (Indexwert)" #WSMAP3 (indx value)#
        EXPRESSION ([pixel] <= 100)
        STYLE
            COLOR "#00000000"
        END
    END
    CLASS
        NAME "1 - Niedrig" #1 - Low#
        EXPRESSION ([pixel] > 100 AND [pixel] <= 113)
        STYLE
            COLORRANGE "#FFFFFF" "#fcc5c0"
            DATARANGE 101 113
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "2"
        EXPRESSION ([pixel] > 113 AND [pixel] <= 137)
        STYLE
            COLORRANGE "#FFFFFF" "#fa9fb5"
            DATARANGE 114 137
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3"
        EXPRESSION ([pixel] > 137 AND [pixel] <= 160)
        STYLE
            COLORRANGE "#fcc5c0" "#f768a1"
            DATARANGE 138 160
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "4"
        EXPRESSION ([pixel] > 160 AND [pixel] <= 183)
        STYLE
            COLORRANGE "#fa9fb5" "#dd3497"
            DATARANGE 161 183
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5"
        EXPRESSION ([pixel] > 183 AND [pixel] <= 206)
        STYLE
            COLORRANGE "#f768a1" "#ae017e"
            DATARANGE 184 206
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6"
        EXPRESSION ([pixel] > 206 AND [pixel] <= 229)
        STYLE
            COLORRANGE "#dd3497" "#7a0177"
            DATARANGE 207 229
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "7 - Hoch" #7 - High#
        EXPRESSION ([pixel] > 229 AND [pixel] <= 255)
        STYLE
            COLORRANGE "#ae017e" "#570156"
            DATARANGE 230 255
            RANGEITEM "pixel"
        END
    END

END