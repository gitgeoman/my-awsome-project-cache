#-----------------------------------------------------
# DEMO - Segmented forest attributes - USA
#-----------------------------------------------------

LAYER
    STATUS          OFF
    MAXSCALEDENOM   1000000
    
    NAME            "segmented_forest_attributes"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          -9430780.1715 5610967.3384 -9342995.4787 5699042.3374
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO - Segmented forest attributes"
        "ows_abstract"        "DEMO - Segmented forest attributes"
        "ows_group_title"     "DEMO - Segmented forest attributes"
        "ows_group_abstract"  "DEMO - Segmented forest attributes"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select
            bag_id,
            fid,
            ba_mean,
            dbhg_mean,
            hg_mean,
            ifty_majority,
            spe_majority,
            tpa_mean,
            vitality_index_mean,
            geom as geometry
        from
            bag_ai_demonstration_maplayers_segments.forest_attributes_pred_demo
        )as foo using unique bag_id using srid=3857"
    
    CLASSITEM "bag_id"
    
    CLASS
        NAME "Forest attribute segments"
        STYLE
            OUTLINECOLOR "#d00000"
            WIDTH 1.5
        END
        STYLE
            COLOR "#ffffff"
            OPACITY 15
        END
    END
END