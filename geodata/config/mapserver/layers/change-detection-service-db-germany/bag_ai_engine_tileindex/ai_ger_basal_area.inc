
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_ger_basal_area_index
    #GROUP         ai_ger_basal_area

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "AI Engine – Basal area (Index)"
        "ows_abstract"          "AI Engine – Basal area (Index)"
        "ows_group_title"       "AI Engine – Basal area (m2/ha)"
        "ows_group_abstract"    "AI Engine – Basal area (m2/ha)"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities !GetMap !GetFeatureInfo !GetLegendGraphic"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021_new.tileindex_g_ger
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
            MINSCALEDENOM 75000
        END
        LABEL
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 75000
    
    NAME          ai_ger_basal_area_raster
    GROUP         ai_ger_basal_area
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – Basal area"
        "ows_abstract"        "AI Engine – Basal area"
        "ows_group_title"     "AI Engine – Basal area (m2/ha)"
        "ows_group_abstract"  "AI Engine – Basal area (m2/ha)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_ger_basal_area_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 8"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 8)
        STYLE
            COLORRANGE "#f1fcee" "#a5bd8e"
            DATARANGE 0 8
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "8-16"
        EXPRESSION ([pixel] > 8 AND [pixel] <= 16)
        STYLE
            COLORRANGE "#a5bd8e" "#68a926"
            DATARANGE 8 16
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "16-24"
        EXPRESSION ([pixel] > 16 AND [pixel] <= 24)
        STYLE
            COLORRANGE "#68a926" "#4d7b1b"
            DATARANGE 16 24
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "24-32"
        EXPRESSION ([pixel] > 24 AND [pixel] <= 32)
        STYLE
            COLORRANGE "#4d7b1b" "#f45d92"
            DATARANGE 24 32
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "32-40"
        EXPRESSION ([pixel] > 32 AND [pixel] <= 40)
        STYLE
            COLORRANGE "#f45d92" "#d0356e"
            DATARANGE 32 40
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "40-48"
        EXPRESSION ([pixel] > 40 AND [pixel] <= 48)
        STYLE
            COLORRANGE "#d0356e" "#9c1348"
            DATARANGE 40 48
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 48"
        EXPRESSION ([pixel] > 48 AND [pixel] <= 100)
        STYLE
            COLORRANGE "#9c1348" "#580325"
            DATARANGE 48 80
            RANGEITEM "pixel"
        END
    END
END