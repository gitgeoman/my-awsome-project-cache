
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_ger_biomass_below_index
    #GROUP         ai_ger_biomass_below

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "AI Engine – Below ground biomass (Index)"
        "ows_abstract"          "AI Engine – Below ground biomass (Index)"
        "ows_group_title"       "AI Engine – Below ground biomass (tn/ha)"
        "ows_group_abstract"    "AI Engine – Below ground biomass (tn/ha)"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities !GetMap !GetFeatureInfo !GetLegendGraphic"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021_new.tileindex_rt_ger
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
            MINSCALEDENOM 75000
        END
        LABEL
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 75000
    
    NAME          ai_ger_biomass_below_raster
    GROUP         ai_ger_biomass_below
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – Below ground biomass"
        "ows_abstract"        "AI Engine – Below ground biomass"
        "ows_group_title"     "AI Engine – Below ground biomass (tn/ha)"
        "ows_group_abstract"  "AI Engine – Below ground biomass (tn/ha)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_ger_biomass_below_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"


    CLASS
        NAME "Biomasse (tn/ha)" #Biomass (tn/ha)#
        EXPRESSION ([pixel] < 0)
    END
    CLASS
        NAME "0-30"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 30)
        STYLE
            COLORRANGE "#FFFFFF" "#D9EF8B"
            DATARANGE 0 30
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "30-60"
        EXPRESSION ([pixel] > 30 AND [pixel] <= 60)
        STYLE
            COLORRANGE "#a6d96a" "#66bd63"
            DATARANGE 30 60
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "60-90"
        EXPRESSION ([pixel] > 60 AND [pixel] <= 90)
        STYLE
            COLORRANGE "#1a9850" "#006837"
            DATARANGE 60 90
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "90-120"
        EXPRESSION ([pixel] > 90 AND [pixel] <= 120)
        STYLE
            COLORRANGE "#fdae61" "#f46d43"
            DATARANGE 90 120
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 120"
        EXPRESSION ([pixel] > 120 AND [pixel] <= 200)
        STYLE
            COLORRANGE "#a50026" "#000000"
            DATARANGE 120 200
            RANGEITEM "pixel"
        END
    END
END