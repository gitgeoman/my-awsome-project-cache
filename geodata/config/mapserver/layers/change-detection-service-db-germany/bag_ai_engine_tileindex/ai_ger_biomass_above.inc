
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_ger_biomass_above_index
    #GROUP         ai_ger_biomass_above

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "AI Engine – Above ground biomass (Index)"
        "ows_abstract"          "AI Engine – Above ground biomass (Index)"
        "ows_group_title"       "AI Engine – Above ground biomass (tn/ha)"
        "ows_group_abstract"    "AI Engine – Above ground biomass (tn/ha)"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities !GetMap !GetFeatureInfo !GetLegendGraphic"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021_new.tileindex_ab_ger
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
            MINSCALEDENOM 75000
        END
        LABEL
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 75000
    
    NAME          ai_ger_biomass_above_raster
    GROUP         ai_ger_biomass_above
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – Above ground biomass"
        "ows_abstract"        "AI Engine – Above ground biomass"
        "ows_group_title"     "AI Engine – Above ground biomass (tn/ha)"
        "ows_group_abstract"  "AI Engine – Above ground biomass (tn/ha)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "height_m"
    END

    TILEINDEX   "ai_ger_biomass_above_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "Biomasse (tn/ha)" #Biomass (tn/ha)#
        EXPRESSION ([pixel] < 0)
    END
    CLASS
        NAME "0-100"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 100)
        STYLE
            COLORRANGE "#D9EF8B" "#a6d96a"
            DATARANGE 0 100
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "100-200"
        EXPRESSION ([pixel] > 100 AND [pixel] <= 200)
        STYLE
            COLORRANGE "#66bd63" "#1a9850"
            DATARANGE 100 200
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "200-300"
        EXPRESSION ([pixel] > 200 AND [pixel] <= 300)
        STYLE
            COLORRANGE "#006837" "#fdae61"
            DATARANGE 200 300
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "300-400"
        EXPRESSION ([pixel] > 300 AND [pixel] <= 400)
        STYLE
            COLORRANGE "#f46d43" "#d73027"
            DATARANGE 300 400
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "400-500"
        EXPRESSION ([pixel] > 400 AND [pixel] <= 500)
        STYLE
            COLORRANGE "#d73027" "#a50026"
            DATARANGE 400 500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 500"
        EXPRESSION ([pixel] > 500 AND [pixel] <= 1000)
        STYLE
            COLORRANGE "#a50026" "#000000"
            DATARANGE 500 1000
            RANGEITEM "pixel"
        END
    END
END