
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_ger_volume_index
    #GROUP         ai_ger_volume

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "AI Engine - Volume (Index)"
        "ows_abstract"          "AI Engine - Volume (Index)"
        "ows_group_title"       "AI Engine - Volume (m3/ha)"
        "ows_group_abstract"    "AI Engine - Volume (m3/ha)"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities !GetMap !GetFeatureInfo !GetLegendGraphic"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021_new.tileindex_v_ger
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
            MINSCALEDENOM 75000
        END
        LABEL
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 75000
    
    NAME          ai_ger_volume_raster
    GROUP         ai_ger_volume
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine - Volume"
        "ows_abstract"        "AI Engine - Volume"
        "ows_group_title"     "AI Engine - Volume (m3/ha)"
        "ows_group_abstract"  "AI Engine - Volume (m3/ha)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_ger_volume_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

	CLASS
        NAME "< 50"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#f7fbff" "#c6dbef"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "50-100"
        EXPRESSION ([pixel] > 50  AND [pixel] <= 100)
        STYLE
            COLORRANGE "#c6dbef" "#6baed6"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "100-150"
        EXPRESSION ([pixel] > 100 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#6baed6" "#4292c6"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "150-200"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 200)
        STYLE
            COLORRANGE "#4292c6" "#2171b5"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "200-250"
        EXPRESSION ([pixel] > 200 AND [pixel] <= 250)
        STYLE
            COLORRANGE "#2171b5" "#fee0d2"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "250-300"
        EXPRESSION ([pixel] > 250 AND [pixel] <= 300)
        STYLE
            COLORRANGE "#fee0d2" "#fc9272"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "300-350"
        EXPRESSION ([pixel] > 300 AND [pixel] <= 350)
        STYLE
            COLORRANGE "#fc9272" "#fb6a4a"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END 
	CLASS
        NAME "350-400"
        EXPRESSION ([pixel] > 350 AND [pixel] <= 400)
        STYLE
            COLORRANGE "#fb6a4a" "#ef3b2c"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END  
	CLASS
        NAME "> 400"
        EXPRESSION ([pixel] > 400)
        STYLE
            COLORRANGE "#ef3b2c" "#cb181d"
            DATARANGE 0 1000
            RANGEITEM "pixel"
        END
    END
END