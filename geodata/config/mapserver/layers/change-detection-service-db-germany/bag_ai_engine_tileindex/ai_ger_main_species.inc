
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_ger_main_species_index
    #GROUP         ai_ger_main_species

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "AI Engine – Main species (Index)"
        "ows_abstract"          "AI Engine – Main species (Index)"
        "ows_group_title"       "AI Engine – Main species"
        "ows_group_abstract"    "AI Engine – Main species"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities !GetMap !GetFeatureInfo !GetLegendGraphic"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021_new.tileindex_mainspecies_ger
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
            MINSCALEDENOM 75000
        END
        LABEL
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 75000
    
    NAME          ai_ger_main_species_raster
    GROUP         ai_ger_main_species
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – Main species"
        "ows_abstract"        "AI Engine – Main species"
        "ows_group_title"     "AI Engine – Main species"
        "ows_group_abstract"  "AI Engine – Main species"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_ger_main_species_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "Fichte/Tanne"
        #NAME "Spruce/Fir"
        EXPRESSION ([pixel] > 0.5 AND [pixel] < 2.5)
        STYLE
            COLOR "#509c7a"
        END
    END
    CLASS
        NAME "Kiefer/Lärche"
        #NAME "Pine/Larch"
        EXPRESSION ([pixel] > 2.5 AND [pixel] < 4.5)
        STYLE
            COLOR "#36ff24"
        END
    END
    CLASS
        NAME "Sonstiges Laubholz"
        #NAME "Other conifers"
        EXPRESSION ([pixel] > 4.5 AND [pixel] < 5.5) 
        STYLE
            COLOR "#004a3c"
        END
    END
    CLASS
        NAME "Buche/Eiche"
        #NAME "Beech/Oak"
        EXPRESSION ([pixel] > 5.5 AND [pixel] < 7.5)
        STYLE
            COLOR "#ffe110"
        END
    END
    CLASS
        NAME "Edellaubholz"
        #NAME "Noble hardwood"
        EXPRESSION ([pixel] > 7.5 AND [pixel] < 8.5)
        STYLE
            COLOR "#7e84af"
        END
    END
    CLASS
        NAME "Sonstiges Nadelholz"
        #NAME "Other deciduous"
        EXPRESSION ([pixel] > 8.5 AND [pixel] < 9.5)
        STYLE
            COLOR "#7d316b"
        END
    END
END