
#-----------------------------------------------------
# Iceye radar Germany
#-----------------------------------------------------

LAYER
    STATUS        OFF
    MINSCALEDENOM 100000

    NAME          iceye_radar_ger_index

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       1312897 6112027 1341807 6141662
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Iceye radar (Index)"
        "ows_abstract"          "DEMO – Iceye radar (Index)"
        "ows_group_title"       "DEMO – Iceye radar"
        "ows_group_abstract"    "DEMO – Iceye radar"

        "wms_srs"               "EPSG:3857"
        "wms_enable_request"    "*"
        "wms_include_items"     "all"
        "WMS_LAYER_GROUP"       "/iceye_radar_ger/iceye_radar_ger_index/"

        "wfs_srs"               "EPSG:3857"
        "wfs_enable_request"    "*"
        "gml_include_items"     "all"
        "gml_featureid"         "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers_iceye.tileindex_iceye_radar_demo
    ) as foo using unique bag_id using srid=3857"

    LABELITEM "bag_id"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    MAXSCALEDENOM 100000
    
    NAME          iceye_radar_ger_raster
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       1312897 6112027 1341807 6141662
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"             "DEMO – Iceye radar (Raster)"
        "ows_abstract"          "DEMO – Iceye radar (Raster)"
        "ows_group_title"       "DEMO – Iceye radar"
        "ows_group_abstract"    "DEMO – Iceye radar"

        "wms_srs"               "EPSG:3857"
        "wms_enable_request"    "*"
        "wms_include_items"     "all"
        "WMS_LAYER_GROUP"       "/iceye_radar_ger/iceye_radar_ger_raster/"

        "wfs_srs"               "EPSG:3857"
        "wfs_enable_request"    "*"
        "gml_include_items"     "all"
        "gml_featureid"         "bag_id"
    END


    TILEINDEX   "iceye_radar_ger_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
       NAME "Band 1"
        EXPRESSION ([pixel] > -35.65 AND [pixel] <= 37.17)
        STYLE
            COLORRANGE "#000000" "#ffffff"
            DATARANGE -35.65 37.17
            RANGEITEM "pixel"
        END
    END
END