LAYER
    STATUS          OFF
    # MINSCALEDENOM   100000

    NAME            "ger_sentinel_wsmap3_tileindex"
    GROUP           "ger_sentinel_wsmap3"

    METADATA
        "ows_title"             "Sentinel WSMAP3"
        "ows_group_title"       "Sentinel WSMAP3"
        "ows_group_abstract"    "Sentinel WSMAP3"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities"
        "gml_include_items"     "date"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"

    DATA "geometry from mat_map_raster_germany_wsmap3 using unique id using srid=3857"

    # LABELITEM "date"

    # COMPOSITE
    #    OPACITY 50
    # END

    # CLASS
    #    STYLE
    #        OUTLINECOLOR "#808080"
    #        COLOR "#b2df8a"
    #        WIDTH 2
    #    END
    #    LABEL
    #        MINSCALEDENOM 5000
    #        MAXSCALEDENOM 200000
    #        FONT scb
    #        TYPE truetype
    #        SIZE 10
    #        COLOR "#746E70"
    #        OUTLINECOLOR "#FFFFFF"
    #        OUTLINEWIDTH 1
    #        ALIGN CENTER
    #    END
    # END

END
LAYER
    STATUS          OFF
    MAXSCALEDENOM   200000
    
    NAME            ger_sentinel_wsmap3
    GROUP           eu_sentinel_wsmap3
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "Sentinel WSMAP3 Germany"
        "ows_group_title"       "Sentinel WSMAP3"
        "ows_group_abstract"    "Sentinel WSMAP3"
        "wms_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    TILEINDEX  "ger_sentinel_wsmap3_tileindex"
    TILEITEM    "location"

    PROCESSING "RESAMPLE=NEAREST"

    CLASS
        NAME "WSMAP3 (Indexwert)" #WSMAP3 (Index value)#
        EXPRESSION ([pixel] <= 113)
        STYLE
            COLOR "#00000000"
        END
    END
    CLASS
        NAME "1 - Niedrig" #1 - Low#
        EXPRESSION ([pixel] > 113 AND [pixel] <= 137)
        STYLE
            COLORRANGE "#FFFFFF" "#fcc5c0"
            DATARANGE 114 137
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "2"
        EXPRESSION ([pixel] > 137 AND [pixel] <= 160)
        STYLE
            COLORRANGE "#fcc5c0" "#fa9fb5"
            DATARANGE 137 160
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3"
        EXPRESSION ([pixel] > 160 AND [pixel] <= 183)
        STYLE
            COLORRANGE "#fa9fb5" "#f768a1"
            DATARANGE 160 183
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "4"
        EXPRESSION ([pixel] > 183 AND [pixel] <= 206)
        STYLE
            COLORRANGE "#f768a1" "#dd3497"
            DATARANGE 183 206
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5"
        EXPRESSION ([pixel] > 206 AND [pixel] <= 229)
        STYLE
            COLORRANGE "#dd3497" "#ae017e"
            DATARANGE 206 229
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6 -Hoch" #6 - High#
        EXPRESSION ([pixel] > 229 AND [pixel] <= 255)
        STYLE
            COLORRANGE "#ae017e" "#7a0177"
            DATARANGE 229 255
            RANGEITEM "pixel"
        END
    END

END