LAYER
    STATUS       OFF

    MAXSCALEDENOM 100000

    NAME        "ai_ger_vitality_risk_index_areas"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – vitality risk areas (vitality index method)"
        "ows_abstract"        "AI Engine – vitality risk areas (vitality index method)"
        "ows_group_title"     "AI Engine – vitality risk areas and cluster (vitality index method)"
        "ows_group_abstract"  "AI Engine – vitality risk areas and cluster (vitality index method)"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,             
            det_type, 
            det_value, 
            det_rel, 
            source,
            model_ver,
            geom as geometry 
        from 
            bag_ai_engine_output.vitality_risk_index_areas_ger
        )as foo using unique bag_id using srid=3857"
    
    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))

    COMPOSITE
        OPACITY 50
    END
    
    CLASS
        NAME "Vitalitätsflächen" #Risk areas#
        STYLE
            COLOR "#cb181d"
            OUTLINECOLOR "#000000"
            WIDTH 0.5
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   100000
    NAME           "ai_ger_vitality_risk_index_areas_cluster"
    GROUP          "ai_ger_vitality_risk_index_areas"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Health stress areas - Cluster"
        "ows_abstract"        "Health stress areas - Cluster"
        "ows_group_title"     "AI Engine – vitality risk areas and cluster (vitality index method)"
        "ows_group_abstract"  "AI Engine – vitality risk areas and cluster (vitality index method)"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (select 
            bag_id,             
            pointforcluster as geometry
        from 
            bag_ai_engine_output.vitality_risk_index_areas_ger
        ) as foo using unique bag_id using srid=3857"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 150
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_ALGORITHM=SIMPLE"
    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id"

    CLASS
      NAME "Cluster" #Feature cluster#
      EXPRESSION ("[Cluster_FeatureCount]" != "1")
      STYLE
        SIZE 50
        SYMBOL "muinaisjaannos_cluster"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END