LAYER
    STATUS       OFF

    MAXSCALEDENOM 100000

    NAME        "ai_ger_change_detection_sentinel1"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – change detection areas (Sentinel 1)"
        "ows_abstract"        "AI Engine – change detection areas (Sentinel 1)"
        "ows_group_title"     "AI Engine – change detection areas and clusters (Sentinel 1)"
        "ows_group_abstract"  "AI Engine – change detection areas and clusters (Sentinel 1)"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,
            fid,
            uuid,
            det_type, 
            det_value, 
            det_rel, 
            det_date,
            source,
            model_ver,
            date1,
            date2,
            to_char(date2::date, 'DD.MM.YYYY') as date_label,
            now()::date - date2::date AS age,
            epsg,
            xtile,
            ytile,
            jobid,
            jobdate,
            'sentinel1_change_model' as output,
            filename,
            geom as geometry 
        from 
            bag_ai_engine_output.sentinel_1_chg_ger
        )as foo using unique bag_id using srid=3857"
    
    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))

    COMPOSITE
        OPACITY 50
    END

   CLASS
        NAME "< 2 Monate" #< 2 months#
        EXPRESSION ([age] <= 60)
        STYLE
            COLOR "#782ea2"
            OUTLINECOLOR "#ffffff"
            WIDTH 2
        END
        LABEL
            TEXT "[date_label]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "> 2 Monate" #> 2 months#
        EXPRESSION ([age] > 60)
        STYLE
            COLOR "#a595cc"
            OUTLINECOLOR "#503761"
            WIDTH 2
        END
        LABEL
            TEXT "[date_label]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   100000
    NAME           "ai_ger_change_detection_sentinel1_cluster"
    GROUP          "ai_ger_change_detection_sentinel1"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine – change detection areas clusters (Sentinel 1)"
        "ows_abstract"        "AI Engine – change detection areas clusters (Sentinel 1)"
        "ows_group_title"     "AI Engine – change detection areas and clusters (Sentinel 1)"
        "ows_group_abstract"  "AI Engine – change detection areas and clusters (Sentinel 1)"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (select 
            bag_id,             
            pointforcluster as geometry
        from 
            bag_ai_engine_output.sentinel_1_chg_ger
        ) as foo using unique bag_id using srid=3857"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 150
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_ALGORITHM=SIMPLE"
    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id"

    CLASS
      NAME "Cluster" #Feature cluster#
      EXPRESSION ("[Cluster_FeatureCount]" != "1")
      STYLE
        SIZE 50
        SYMBOL "muinaisjaannos_cluster"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END