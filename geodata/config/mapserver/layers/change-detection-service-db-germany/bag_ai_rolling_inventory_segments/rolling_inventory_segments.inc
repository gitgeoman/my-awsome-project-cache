#-----------------------------------------------------
# Rolling inventory segments - Germany
#-----------------------------------------------------

LAYER
    STATUS       OFF

    MAXSCALEDENOM 1000000

    NAME        "ai_ger_rolling_inventory_segments"
    TYPE        POLYGON
    TEMPLATE    "foo"

    #EXTENT       570403 5676651 1966753 7506238
    EXTENT       1111029.9 6031119.7 1383648.3 6202944.7

    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "AI Engine - rolling inventory segments"
        "ows_abstract"        "AI Engine - rolling inventory segments"
        "ows_group_title"     "AI Engine - rolling inventory segments"
        "ows_group_abstract"  "AI Engine - rolling inventory segments"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,
            fid,
            meanheight,
            stemcount,
            volume,
            meandiameter,
            basalarea,
            abovegroundbiomass,
            belowgroundbiomass,
            carbon,
            mainspecies,
            geom as geometry 
        from 
            bag_ai_germany_rolling_inventory_output.segments
        )as foo using unique bag_id using srid=3857"

    CLASSITEM "bag_id"
    
    CLASS
        NAME "Rolling inventory segments"
        STYLE
            OUTLINECOLOR "#d00000"
            WIDTH 1.5
        END
        STYLE
            COLOR "#ffffff"
            OPACITY 15
        END
    END
END