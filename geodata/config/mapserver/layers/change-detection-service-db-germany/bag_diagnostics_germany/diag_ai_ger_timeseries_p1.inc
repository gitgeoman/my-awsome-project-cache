LAYER
    STATUS       OFF

    NAME        "diag_ai_ger_timeseries_p1"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Timeseries - date of latest tile (p1)"
        "ows_abstract"        "date represent latest date that tile has been accepted (p1)"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (select 
            bag_id, 
            origin, 
            scene, 
            product1da,
            product2da,
            product3da,
            count,
            count_1yr,
            count_3yr,
            created_on,
            geom as geometry 
        from 
            bag_diagnostics_germany.timeseries
        ) as foo using unique bag_id using srid=3857"
        
    LABELITEM "product1da"

    CLASS
        NAME "Accepted tiles 2022"
        EXPRESSION ('[product1da]' >= "2022-01-01")
        STYLE
            COLOR "#52BE80CC"
            OPACITY 50
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        LABEL
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
    END

    CLASS
        NAME "Accepted tiles 2021"
        EXPRESSION ('[product1da]' >= "2021-01-01" AND '[product1da]' < "2022-01-01")
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#FFED00"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
    END

    CLASS
        NAME "Accepted tiles 2020"
        EXPRESSION ('[product1da]' >= "2020-01-01" AND '[product1da]' < "2021-01-01")
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#FF6E00"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
    END

    CLASS
        NAME "Accepted tiles before 2020"
        EXPRESSION ('[product1da]' < "2020-01-01")
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#ff0012"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
    END

END