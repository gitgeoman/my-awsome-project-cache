LAYER
    STATUS          OFF
    MINSCALEDENOM   700000
    MAXSCALEDENOM   11000000
    NAME            "marketing_cds_ger_change_detection_features_simple_ger_cluster_2021"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "wms_title"           "Marketing - Change detection features Germany (2021)"
        "wms_abstract"        "Marketing - Change detection features Germany (2021)"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetLegendGraphic !GetFeatureInfo !GetFeature"
        "wfs_enable_request"   "!*"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        select
            id,
            change_typ,
            now()::date - date2::date AS age,
            pointforcluster as geom
        from
            public.change_detection_features_germany_marketing
        where
           date2::date > '1.1.2021'
    ) as foo using unique id using srid=3857"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 300
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_ALGORITHM=SIMPLE"
    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=id"

    CLASS
      NAME "Muutokset"
      STYLE
        SIZE 50
        SYMBOL "includes/symbols/circle_red_gradiant.svg"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        OUTLINECOLOR 0 0 0
        ALIGN CENTER
        PRIORITY 10
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END