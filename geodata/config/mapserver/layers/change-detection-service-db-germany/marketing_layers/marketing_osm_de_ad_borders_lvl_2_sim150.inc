LAYER
    STATUS          OFF

    MINSCALEDENOM   2000
    MAXSCALEDENOM   192000000

    NAME            "marketing_osm_de_ad_borders_lvl_2_sim150"
    GROUP           "marketing_osm_de_ad_borders"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "wms_title"           "Land"
        "wms_abstract"        "Marketing - DE administrative borders OSM lvl 2 simplified"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetLegendGraphic !GetFeatureInfo !GetFeature"
        "wfs_enable_request"   "!*"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.marketing_de_ad_borders_lvl_2_simplified
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
      OPACITY 100
    END
    
    CLASS
        NAME "Land" #Country#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 4
        END #style
        STYLE
            OUTLINECOLOR "#3b3b3b"
            WIDTH 2
        END #style
    END #class
END