LAYER
    STATUS          OFF
    MINSCALEDENOM   2000
    MAXSCALEDENOM   1600000

    NAME            "marketing_osm_de_ad_borders_lvl_6_sim150"
    GROUP           "marketing_osm_de_ad_borders"

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "wms_title"           "Landkreis"
        "wms_abstract"        "Marketing - DE administrative borders OSM lvl 6 simplified"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetLegendGraphic !GetFeatureInfo !GetFeature"
        "wfs_enable_request"   "!*"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.marketing_de_ad_borders_lvl_6_simplified
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
      OPACITY 100
    END
    
    LABELITEM   "name"
    POSTLABELCACHE FALSE
    PROCESSING "LABEL_NO_CLIP=ON"
    LABELCACHE ON
    
    CLASS
        NAME "Landkreis" #District#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 1.5
        END #style
        LABEL
            MINSCALEDENOM 800000
            MAXSCALEDENOM 1600000
            FONT scb
            TYPE truetype
            MINSIZE 6
            MAXSIZE 8
            COLOR "#3b3b3b"
            OUTLINECOLOR "#ffffff"
            OUTLINEWIDTH 1
            ALIGN CENTER
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            POSITION cc
        END #label
    END #class
END