LAYER
    STATUS OFF
    MINSCALEDENOM   700000
    MAXSCALEDENOM   11000000
    
    NAME "marketing_cds_ger_change_detection_sentinel2"
    TYPE raster

    CONNECTIONTYPE kerneldensity
    CONNECTION "point_layer"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Change detection heatmap (Sentinel 2) GER"
        "ows_abstract"        "Change detection heatmap (Sentinel 2) GER"


        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetLegendGraphic !GetFeatureInfo !GetFeature"
    END
   
    
    PROCESSING "RANGE_COLORSPACE=HSL"
    PROCESSING "KERNELDENSITY_RADIUS=50"
    PROCESSING "KERNELDENSITY_COMPUTE_BORDERS=ON"
    PROCESSING "KERNELDENSITY_NORMALIZATION=numeric"
    OFFSITE 0 0 0

    COMPOSITE
        OPACITY 60
    END

    CLASS
      STYLE
        COLORRANGE  "#0000ff00"  "#0000ffff"
        DATARANGE 0 32
      END 
      STYLE
        COLORRANGE  "#0000ffff"  "#ff0000ff"
        DATARANGE 32 255
      END 
    END 
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   700000
    MAXSCALEDENOM   11000000    
    
    NAME            "point_layer"
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Point layer for change detection heatmap (Sentinel 2) GER"
        "ows_abstract"        "Point layer for change detection heatmap (Sentinel 2) GER"


        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (
        select 
            id,
            change_typ,
            pointforcluster as geometry 
        from 
            public.change_detection_features_germany_marketing
        where
            date2::date > '1.1.2019'
        )as foo using unique id using srid=3857"

    CLASS
        STYLE
            SIZE [change_typ]
        END
    END
END
