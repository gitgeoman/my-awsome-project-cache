LAYER
    STATUS          OFF

    MINSCALEDENOM   2000
    MAXSCALEDENOM   13000000

    NAME            "marketing_osm_de_ad_borders_lvl_4_sim150"
    GROUP           "marketing_osm_de_ad_borders"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "wms_title"           "Bundesland"
        "wms_abstract"        "Marketing - DE administrative borders OSM lvl 4 simplified"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "!GetLegendGraphic !GetFeatureInfo !GetFeature"
        "wfs_enable_request"   "!*"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.marketing_de_ad_borders_lvl_4_simplified
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
      OPACITY 100
    END
    
    LABELITEM   "name"
    POSTLABELCACHE FALSE
    PROCESSING "LABEL_NO_CLIP=ON"
    LABELCACHE ON

    CLASS
        NAME "Bundesland" #State#
        STYLE
            OUTLINECOLOR "#ffa353"
            WIDTH 3
        END #style
        STYLE
            OUTLINECOLOR "#3b3b3b"
            WIDTH 1
        END #style
        LABEL
            MINSCALEDENOM 1600000
            MAXSCALEDENOM 13000000
            FONT scb
            TYPE truetype
            MINSIZE 7
            MAXSIZE 9
            COLOR "#ffffff"
            OUTLINECOLOR "#3b3b3b"
            OUTLINEWIDTH 1
            ALIGN CENTER
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            POSITION cc
        END #label
    END #class
END