
#-----------------------------------------------------
# Demonstration maplayers: Forest type
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          fty_pred_demo_index
    GROUP         fty_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Forest type (Index)"
        "ows_abstract"          "DEMO – Forest type (Index)"
        "ows_group_title"       "DEMO – Forest type"
        "ows_group_abstract"    "DEMO – Forest type"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_ifty_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          fty_pred_demo_raster
    GROUP         fty_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Forest type"
        "ows_abstract"        "DEMO – Forest type"
        "ows_group_title"     "DEMO – Forest type"
        "ows_group_abstract"  "DEMO – Forest type"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "fty_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "101 - Aspen"
        #EXPRESSION ([pixel] = 101)
        EXPRESSION ([pixel] > 100.5 AND [pixel] < 101.5)
        STYLE
            COLOR "#93dc87"
        END
    END
    CLASS
        NAME "102 - Conifer"
        #EXPRESSION ([pixel] = 102)
        EXPRESSION ([pixel] > 101.5 AND [pixel] < 102.5)
        STYLE
            COLOR "#0a8010"
        END
    END
    CLASS
        NAME "103 - ConiferBog"
        #EXPRESSION ([pixel] = 103)
        EXPRESSION ([pixel] > 102.5 AND [pixel] < 103.5)
        STYLE
            COLOR "#07deb8"
        END
    END
    CLASS
        NAME "104 - Hardwood"
        #EXPRESSION ([pixel] = 104)
        EXPRESSION ([pixel] > 103.5 AND [pixel] < 104.5)
        STYLE
            COLOR "#31f916"
        END
    END
    CLASS
        NAME "105 - Non-Forest"
        #EXPRESSION ([pixel] = 105)
        EXPRESSION ([pixel] > 104.5 AND [pixel] < 105.5)
        STYLE
            COLOR "#ef9b4d"
        END
    END
END