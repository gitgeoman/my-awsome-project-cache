
#-----------------------------------------------------
# Demonstration maplayers: Stem Count - Trees per acre
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          tpa_pred_demo_index
    GROUP         tpa_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042

    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Stem count TPA (Index)"
        "ows_abstract"          "DEMO – Stem count TPA (Index)"
        "ows_group_title"       "DEMO – Stem count TPA (trees/acre)"
        "ows_group_abstract"    "DEMO – Stem count TPA (trees/acre)"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_tpa_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          tpa_pred_demo_raster
    GROUP         tpa_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Stem count TPA"
        "ows_abstract"        "DEMO – Stem count TPA"
        "ows_group_title"     "DEMO – Stem count TPA (trees/acre)"
        "ows_group_abstract"  "DEMO – Stem count TPA (trees/acre)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "tpa_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=-1"

    CLASS
        NAME "< 180"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 180)
        STYLE
            COLORRANGE "#f7fcf5" "#ddf2d7"
            DATARANGE 0 180
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "180-360"
        EXPRESSION ([pixel] > 180 AND [pixel] <= 360)
        STYLE
            COLORRANGE "#ddf2d7" "#b2e0ab"
            DATARANGE 180 360
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "360-540"
        EXPRESSION ([pixel] > 360 AND [pixel] <= 540)
        STYLE
            COLORRANGE "#b2e0ab" "#7bc77c"
            DATARANGE 360 540
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "540-720"
        EXPRESSION ([pixel] > 540 AND [pixel] <= 720)
        STYLE
            COLORRANGE "#7bc77c" "#3da75a"
            DATARANGE 540 720
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "720-900"
        EXPRESSION ([pixel] > 720 AND [pixel] <= 900)
        STYLE
            COLORRANGE "#3da75a" "#137e3a"
            DATARANGE 720 900
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "900-1080"
        EXPRESSION ([pixel] > 900 AND [pixel] <= 1080)
        STYLE
            COLORRANGE "#137e3a" "#00441b"
            DATARANGE 1000 3000
            RANGEITEM "pixel"
        END
    END
        CLASS
        NAME "> 1080"
        EXPRESSION ([pixel] > 1080 AND [pixel] <= 3000)
        STYLE
            COLORRANGE "#00441b" "#001809"
            DATARANGE 1080 3000
            RANGEITEM "pixel"
        END
    END
END