
#-----------------------------------------------------
# Demonstration maplayers: Tree species
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          spe_pred_demo_index
    GROUP         spe_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Tree species (Index)"
        "ows_abstract"          "DEMO – Tree species (Index)"
        "ows_group_title"       "DEMO – Tree species"
        "ows_group_abstract"    "DEMO – Tree species"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_spe_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          spe_pred_demo_raster
    GROUP         spe_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Tree species"
        "ows_abstract"        "DEMO – Tree species"
        "ows_group_title"     "DEMO – Tree species"
        "ows_group_abstract"  "DEMO – Tree species"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "spe_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "12 - Balsam fir"
        EXPRESSION ([pixel] > 11.5 AND [pixel] < 12.5)
        STYLE
            COLOR "#749961"
        END
    END
    CLASS
        NAME "71 - Tamarack"
        EXPRESSION ([pixel] > 70.5 AND [pixel] < 71.5)
        STYLE
            COLOR "#3c9e99"
        END
    END
    CLASS
        NAME "95 - Black spruce"
        EXPRESSION ([pixel] > 94.5 AND [pixel] < 95.5)
        STYLE
            COLOR "#556544"
        END
    END
    CLASS
        NAME "105 - Jack pine"
        EXPRESSION ([pixel] > 104.5 AND [pixel] < 105.5)
        STYLE
            COLOR "#374244"
        END
    END
    CLASS
        NAME "125 - Red pine"
        EXPRESSION ([pixel] > 124.5 AND [pixel] < 125.5)
        STYLE
            COLOR "#4f629b"
        END
    END
    CLASS
        NAME "129 - Eastern white pine"
        EXPRESSION ([pixel] > 128.5 AND [pixel] < 129.5)
        STYLE
            COLOR "#162b19"
        END
    END
    CLASS
        NAME "241 - Northern white-cedar"
        EXPRESSION ([pixel] > 240.5 AND [pixel] < 241.5)
        STYLE
            COLOR "#6c4141"
        END
    END
    CLASS
        NAME "261 - Eastern hemlock"
        EXPRESSION ([pixel] > 260.5 AND [pixel] < 261.5)
        STYLE
            COLOR "#422836"
        END
    END
    CLASS
        NAME "316 - Red maple"
        EXPRESSION ([pixel] > 315.5 AND [pixel] < 316.5)
        STYLE
            COLOR "#639374"
        END
    END
    CLASS
        NAME "318 - Sugar maple"
        EXPRESSION ([pixel] > 317.5 AND [pixel] < 318.5)
        STYLE
            COLOR "#2a2a2d"
        END
    END
    CLASS
        NAME "531 - American beech"
        EXPRESSION ([pixel] > 530.5 AND [pixel] < 531.5)
        STYLE
            COLOR "#544e46"
        END
    END
    CLASS
        NAME "701 - Eastern hophornbeam"
        EXPRESSION ([pixel] > 700.5 AND [pixel] < 701.5)
        STYLE
            COLOR "#e0d5c9"
        END
    END
    CLASS
        NAME "741 - Balsam poplar"
        EXPRESSION ([pixel] > 740.5 AND [pixel] < 741.5)
        STYLE
            COLOR "#7a5f6d"
        END
    END
    CLASS
        NAME "743 - Bigtooth aspen"
        EXPRESSION ([pixel] > 742.5 AND [pixel] < 743.5)
        STYLE
            COLOR "#083b0b"
        END
    END
    CLASS
        NAME "746 - Quaking aspen"
        EXPRESSION ([pixel] > 745.5 AND [pixel] < 746.5)
        STYLE
            COLOR "#2b2d2d"
        END
    END
    CLASS
        NAME "833 - Northern red oak"
        EXPRESSION ([pixel] > 832.5 AND [pixel] < 833.5)
        STYLE
            COLOR "#9b7381"
        END
    END
    CLASS
        NAME "951 - American basswood"
        EXPRESSION ([pixel] > 950.5 AND [pixel] < 951.5)
        STYLE
            COLOR "#c1d0ef"
        END
    END
END