
#-----------------------------------------------------
# Demonstration maplayers: Basal Area
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ba_pred_demo_index
    GROUP         ba_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Basal area (Index)"
        "ows_abstract"          "DEMO – Basal area (Index)"
        "ows_group_title"       "DEMO – Basal area (ft2/acre)"
        "ows_group_abstract"    "DEMO – Basal area (ft2/acre)"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_ba_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ba_pred_demo_raster
    GROUP         ba_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Basal area"
        "ows_abstract"        "DEMO – Basal area"
        "ows_group_title"     "DEMO – Basal area (ft2/acre)"
        "ows_group_abstract"  "DEMO – Basal area (ft2/acre)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ba_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=-1"

    CLASS
        NAME "< 30"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 30)
        STYLE
            COLORRANGE "#f1fcee" "#a5bd8e"
            DATARANGE 0 30
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "30-60"
        EXPRESSION ([pixel] > 30 AND [pixel] <= 60)
        STYLE
            COLORRANGE "#a5bd8e" "#68a926"
            DATARANGE 30 60
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "60-90"
        EXPRESSION ([pixel] > 60 AND [pixel] <= 90)
        STYLE
            COLORRANGE "#68a926" "#4d7b1b"
            DATARANGE 60 90
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "90-120"
        EXPRESSION ([pixel] > 90 AND [pixel] <= 120)
        STYLE
            COLORRANGE "#4d7b1b" "#f45d92"
            DATARANGE 90 120
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "120-150"
        EXPRESSION ([pixel] > 120 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#f45d92" "#d0356e"
            DATARANGE 110 130
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "150-180"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 180)
        STYLE
            COLORRANGE "#d0356e" "#9c1348"
            DATARANGE 150 180
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 180"
        EXPRESSION ([pixel] > 180 AND [pixel] <= 250)
        STYLE
            COLORRANGE "#9c1348" "#580325"
            DATARANGE 180 250
            RANGEITEM "pixel"
        END
    END
END