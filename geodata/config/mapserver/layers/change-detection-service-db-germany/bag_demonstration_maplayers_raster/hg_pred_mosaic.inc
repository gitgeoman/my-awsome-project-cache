
#-----------------------------------------------------
# Demonstration maplayers: Mean Height Hg
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          hg_pred_demo_index
    GROUP         hg_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042

    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Mean height Hg (Index)"
        "ows_abstract"          "DEMO – Mean height Hg (Index)"
        "ows_group_title"       "DEMO – Mean height Hg (ft)"
        "ows_group_abstract"    "DEMO – Mean height Hg (ft)"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_hg_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          hg_pred_demo_raster
    GROUP         hg_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Mean height Hg"
        "ows_abstract"        "DEMO – Mean height Hg"
        "ows_group_title"     "DEMO – Mean height Hg (ft)"
        "ows_group_abstract"  "DEMO – Mean height Hg (ft)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "hg_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=-1"

    CLASS
        NAME "< 25"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#fff5eb" "#fddab6"
            DATARANGE 0 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "25-40"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 40)
        STYLE
            COLORRANGE "#fddab6" "#fdab67"
            DATARANGE 25 40
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "40-55"
        EXPRESSION ([pixel] > 40 AND [pixel] <= 55)
        STYLE
            COLORRANGE "#fdab67" "#f67723"
            DATARANGE 40 55
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "55-70"
        EXPRESSION ([pixel] > 55 AND [pixel] <= 70)
        STYLE
            COLORRANGE "#f67723" "#d14501"
            DATARANGE 55 70
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "70-85"
        EXPRESSION ([pixel] > 70 AND [pixel] <= 85)
        STYLE
            COLORRANGE "#d14501" "#7f2704"
            DATARANGE 70 85
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 85"
        EXPRESSION ([pixel] > 85 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#7f2704" "#370f01"
            DATARANGE 83 150
            RANGEITEM "pixel"
        END
    END
END