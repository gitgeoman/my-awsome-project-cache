
#-----------------------------------------------------
# Demonstration maplayers: Mean Diameter Breast Height DBHg
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          dbhg_pred_demo_index
    GROUP         dbhg_pred_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042

    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Mean diameter DBHg (Index)"
        "ows_abstract"          "DEMO – Mean diameter DBHg (Index)"
        "ows_group_title"       "DEMO – Mean diameter DBHg (inches)"
        "ows_group_abstract"    "DEMO – Mean diameter DBHg (inches)"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_dbhg_pred_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          dbhg_pred_demo_raster
    GROUP         dbhg_pred_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Mean diameter DBHg"
        "ows_abstract"        "DEMO – Mean diameter DBHg"
        "ows_group_title"     "DEMO – Mean diameter DBHg (inches)"
        "ows_group_abstract"  "DEMO – Mean diameter DBHg (inches)"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "dbhg_pred_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=-1"

    CLASS
        NAME "< 3"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 3)
        STYLE
            COLORRANGE "#edf8fb" "#b5cde2"
            DATARANGE 0 3
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3-6"
        EXPRESSION ([pixel] > 3 AND [pixel] <= 6)
        STYLE
            COLORRANGE "#b5cde2" "#8c95c2"
            DATARANGE 3 6
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6-9"
        EXPRESSION ([pixel] > 6 AND [pixel] <= 9)
        STYLE
            COLORRANGE "#8c95c2" "#8856a7"
            DATARANGE 6 9
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "9-12"
        EXPRESSION ([pixel] > 9 AND [pixel] <= 12)
        STYLE
            COLORRANGE "#8856a7" "#810f7c"
            DATARANGE 9 12
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "12-15"
        EXPRESSION ([pixel] > 12 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#810f7c" "#5f0a5c"
            DATARANGE 12 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 15"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 20)
        STYLE
            COLORRANGE "#5f0a5c" "#240323"
            DATARANGE 15 20
            RANGEITEM "pixel"
        END
    END
END