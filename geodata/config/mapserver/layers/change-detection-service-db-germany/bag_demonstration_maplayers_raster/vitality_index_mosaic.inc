
#-----------------------------------------------------
# Demonstration maplayers: Vitality index
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          vitality_index_demo_index
    GROUP         vitality_index_demo

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "DEMO – Vitality index (Index)"
        "ows_abstract"          "DEMO – Vitality index (Index)"
        "ows_group_title"       "DEMO – Vitality index"
        "ows_group_abstract"    "DEMO – Vitality index"
        "ows_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry
        FROM
            bag_ai_demonstration_maplayers.tileindex_vitality_index_demo
    ) as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          vitality_index_demo_raster
    GROUP         vitality_index_demo
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       -9430780 5610967 -9342989 5699042
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "DEMO – Vitality index"
        "ows_abstract"        "DEMO – Vitality index"
        "ows_group_title"     "DEMO – Vitality index"
        "ows_group_abstract"  "DEMO – Vitality index"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "vitality_index_demo_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 0.14"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 0.14)
        STYLE
            COLORRANGE "#56ae42" "#b3de76"
            DATARANGE 0 0.14
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.14-0.29"
        EXPRESSION ([pixel] > 0.14 AND [pixel] <= 0.29)
        STYLE
            COLORRANGE "#b3de76" "#e6f4a7"
            DATARANGE 0.14 0.29
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.29-0.43"
        EXPRESSION ([pixel] > 0.29 AND [pixel] <= 0.43)
        STYLE
            COLORRANGE "#e6f4a7" "#fee8a5"
            DATARANGE 0.29 0.43
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.43-0.57"
        EXPRESSION ([pixel] > 0.43 AND [pixel] <= 0.57)
        STYLE
            COLORRANGE "#fee8a5" "#fdba6f"
            DATARANGE 0.43 0.57
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.57-0.71"
        EXPRESSION ([pixel] > 0.57 AND [pixel] <= 0.71)
        STYLE
            COLORRANGE "#fdba6f" "#ed6e43"
            DATARANGE 0.57 0.71
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.71-0.86"
        EXPRESSION ([pixel] > 0.71 AND [pixel] <= 0.86)
        STYLE
            COLORRANGE "#ed6e43" "#d7191c"
            DATARANGE 0.71 0.86
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 0.86"
        EXPRESSION ([pixel] > 0.86 AND [pixel] <= 1)
        STYLE
            COLORRANGE "#d7191c" "#a61416"
            DATARANGE 0.86 1
            RANGEITEM "pixel"
        END
    END
END