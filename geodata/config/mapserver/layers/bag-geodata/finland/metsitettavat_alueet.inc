LAYER
    STATUS        OFF
    
    NAME          metsitettavat_alueet
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_srs"             "EPSG:3067"
        "wms_title"           "Metsitettävät alueet"
        "wms_abstract"        "This layer is created directly from SYKE CORINE 2018 land cover geodata.The objective was to select potential areas for afforestation. Selected classes representing potential afforestable land: natural pastures, agro-forestry areas, transitional woodland/shrub (cc<10-%) and transitional woodland/shrub (cc 10-30% on mineral soil, peatland or on rocky soil)"
        "gml_include_items"   "all"
    END

    DATA   "/vsis3/bag-prod-geodata/public/finland/bitcomp_own/metsitettavat_alueet.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "0"
        EXPRESSION ([pixel] = 0)
        STYLE
            COLOR "#00000000"
        END
    END
    CLASS
        NAME "1"
        EXPRESSION ([pixel] = 1)
        STYLE
            COLOR "#8f00fc"
        END
    END
END