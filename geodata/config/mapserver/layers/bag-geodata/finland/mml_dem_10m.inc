LAYER
    STATUS          OFF
    
    NAME	        mml_dem_10m_tileindex
    GROUP	        mml_dem_10m
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "MML Korkeusmalli-indeksi (10m)"
        "ows_abstract"          "MML Korkeusmalli-indeksi (10m)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_name as bag_sourcefile,
            file_date as bag_source_ts,
            file_path as location,
            updated_at as bag_update_ts,
            geom as geometry
        from
            mml_korkeusmalli.korkeusmalli_hila_10m
    ) as foo using unique bag_id using srid=3067"
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 100000

    NAME          mml_dem_10m
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Korkeusmalli (10m)"
        "ows_abstract"        "MML Korkeusmalli (10m)"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "all"
    END

    TILEINDEX   "mml_dem_10m_tileindex"
    TILEITEM    "location"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME ">1200"
        EXPRESSION ([pixel] > 1200)
        STYLE
            COLOR "#594734"
        END
    END
    CLASS
        NAME "1000-1200"
        EXPRESSION ([pixel] > 1000 AND [pixel] <= 1200)
        STYLE
            COLOR "#84663B"
        END
    END
    CLASS
        NAME "900-1000"
        EXPRESSION ([pixel] > 900 AND [pixel] <= 1000)
        STYLE
            COLOR "#92723E"
        END
    END
    CLASS
        NAME "800-900"
        EXPRESSION ([pixel] > 800 AND [pixel] <= 900)
        STYLE
            COLOR "#84663B"
        END
    END
    CLASS
        NAME "700-800"
        EXPRESSION ([pixel] > 700 AND [pixel] <= 800)
        STYLE
            COLOR "#92723E"
        END
    END
    CLASS
        NAME "600-700"
        EXPRESSION ([pixel] > 600 AND [pixel] <= 700)
        STYLE
            COLOR "#A37E3E"
        END
    END
    CLASS
        NAME "550-600"
        EXPRESSION ([pixel] > 550 AND [pixel] <= 600)
        STYLE
            COLOR "#AF863E"
        END
    END
    CLASS
        NAME "500-550"
        EXPRESSION ([pixel] > 500 AND [pixel] <= 550)
        STYLE
            COLOR "#BB8D41"
        END
    END
    CLASS
        NAME "450-500"
        EXPRESSION ([pixel] > 450 AND [pixel] <= 500)
        STYLE
            COLOR "#CA9745"
        END
    END
    CLASS
        NAME "400-450"
        EXPRESSION ([pixel] > 400 AND [pixel] <= 450)
        STYLE
            COLOR "#DAA049"
        END
    END
    CLASS
        NAME "350-400"
        EXPRESSION ([pixel] > 350 AND [pixel] <= 400)
        STYLE
            COLOR "#E6A74B"
        END
    END
    CLASS
        NAME "300-350"
        EXPRESSION ([pixel] > 300 AND [pixel] <= 350)
        STYLE
            COLOR "#F5AF51"
        END
    END
    CLASS
        NAME "275-300"
        EXPRESSION ([pixel] > 275 AND [pixel] <= 300)
        STYLE
            COLOR "#F8BB56"
        END
    END
    CLASS
        NAME "250-275"
        EXPRESSION ([pixel] > 250 AND [pixel] <= 275)
        STYLE
            COLOR "#F8BB56"
        END
    END
    CLASS
        NAME "225-250"
        EXPRESSION ([pixel] > 225 AND [pixel] <= 250)
        STYLE
            COLOR "#FBDC68"
        END
    END
    CLASS
        NAME "200-225"
        EXPRESSION ([pixel] > 200 AND [pixel] <= 225)
        STYLE
            COLOR "#FBE76F"
        END
    END
    CLASS
        NAME "175-200"
        EXPRESSION ([pixel] > 175 AND [pixel] <= 200)
        STYLE
            COLOR "#FCF178"
        END
    END
    CLASS
        NAME "150-175"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 175)
        STYLE
            COLOR "#FAFA7D"
        END
    END
    CLASS
        NAME "125-150"
        EXPRESSION ([pixel] > 125 AND [pixel] <= 150)
        STYLE
            COLOR "#EBFB7F"
        END
    END
    CLASS
        NAME "100-125"
        EXPRESSION ([pixel] > 100 AND [pixel] <= 125)
        STYLE
            COLOR "#DAF87D"
        END
    END
    CLASS
        NAME "75-100"
        EXPRESSION ([pixel] > 75 AND [pixel] <= 100)
        STYLE
            COLOR "#C9F57E"
        END
    END
    CLASS
        NAME "60-75"
        EXPRESSION ([pixel] > 60 AND [pixel] <= 75)
        STYLE
            COLOR "#BAF380"
        END
    END
    CLASS
        NAME "50-60"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 60)
        STYLE
            COLOR "#ADF380"
        END
    END
    CLASS
        NAME "40-50"
        EXPRESSION ([pixel] > 40 AND [pixel] <= 50)
        STYLE
            COLOR "#9FE57A"
        END
    END
    CLASS
        NAME "30-40"
        EXPRESSION ([pixel] > 30 AND [pixel] <= 40)
        STYLE
            COLOR "#93D470"
        END
    END
    CLASS
        NAME "20-30"
        EXPRESSION ([pixel] > 20 AND [pixel] <= 30)
        STYLE
            COLOR "#84C166"
        END
    END
    CLASS
        NAME "10-20"
        EXPRESSION ([pixel] > 10 AND [pixel] <= 20)
        STYLE
            COLOR "#77AC5A"
        END
    END
    CLASS
        NAME "0.5-10"
        EXPRESSION ([pixel] > 0.5 AND [pixel] <= 10)
        STYLE
            COLOR "#6A9B53"
        END
    END
    CLASS
        NAME "-10 - 0,5"
        EXPRESSION ([pixel] > -10 AND [pixel] <= 0.5)
        STYLE
            COLOR "#BED3FD"
        END
    END
    CLASS
        NAME "-200 - -10"
        EXPRESSION ([pixel] > -200 AND [pixel] <= -10)
        STYLE
            COLOR "#366AFB"
        END
    END
END