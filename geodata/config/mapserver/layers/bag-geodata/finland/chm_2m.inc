#-----------------------------------------------------
# CHM layers
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 20000

    NAME          chm_2m_index
    GROUP         chm_2m

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "Metsäkeskus Kasvillisuuden korkeusmalli 2m (Index)"
        "ows_abstract"          "Metsäkeskus Kasvillisuuden korkeusmalli indeksikarttalehdet"
        "ows_group_title"       "Metsäkeskus Kasvillisuuden korkeusmalli 2m"
        "ows_group_abstract"    "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli teemoitus 2m välein"
        "ows_group_name"        "chm_2m"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            mapsheet,
            filename,
            location,
            year,
            geom as geometry
        FROM
            bag_metsakeskus_tileindex.metsakeskus_chm_2022
    ) as foo using unique bag_id using srid=3067"

END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 19999
    
    NAME          chm_2m_raster
    GROUP         chm_2m
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Kasvillisuuden korkeusmalli 2m (Raster)"
        "ows_abstract"        "Metsäkeskus Kasvillisuuden korkeusmalli indeksikarttalehdet"
        "ows_group_title"     "Metsäkeskus Kasvillisuuden korkeusmalli 2m"
        "ows_group_abstract"  "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli 2m"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "chm_2m_index"
    TILEITEM    "location"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "0-3"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 3)
        STYLE
            COLORRANGE "#FFFFFF" "#fce876"
            DATARANGE 0 3
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3-6"
        EXPRESSION ([pixel] > 3 AND [pixel] <= 6)
        STYLE
            COLORRANGE "#fce876" "#f9e927"
            DATARANGE 3 6
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6-9"
        EXPRESSION ([pixel] > 6 AND [pixel] <= 9)
        STYLE
            COLORRANGE "#f9e927" "#bcad01"
            DATARANGE 6 9
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "9-10"
        EXPRESSION ([pixel] > 9 AND [pixel] <= 10)
        STYLE
            COLORRANGE "#bcad01" "#82e22d"
            DATARANGE 9 10
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "10-11"
        EXPRESSION ([pixel] > 10 AND [pixel] <= 11)
        STYLE
            COLORRANGE "#82e22d" "#4eed92"
            DATARANGE 10 11
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "11-12"
        EXPRESSION ([pixel] > 11 AND [pixel] <= 12)
        STYLE
            COLORRANGE "#4eed92" "#67d863"
            DATARANGE 11 12
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "12-13"
        EXPRESSION ([pixel] > 12 AND [pixel] <= 13)
        STYLE
            COLORRANGE "#67d863" "#009159"
            DATARANGE 12 13
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "13-14"
        EXPRESSION ([pixel] > 13 AND [pixel] <= 14)
        STYLE
            COLORRANGE "#009159" "#04603d"
            DATARANGE 13 14
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "14-15"
        EXPRESSION ([pixel] > 14 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#04603d" "#406019"
            DATARANGE 14 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "15-16"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 16)
        STYLE
            COLORRANGE "#406019" "#003d01"
            DATARANGE 15 16
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "16-17"
        EXPRESSION ([pixel] > 16 AND [pixel] <= 17)
        STYLE
            COLORRANGE "#003d01" "#ff9c72"
            DATARANGE 16 17
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "17-18"
        EXPRESSION ([pixel] > 17 AND [pixel] <= 18)
        STYLE
            COLORRANGE "#ff9c72" "#ff8128"
            DATARANGE 17 18
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "18-19"
        EXPRESSION ([pixel] > 18 AND [pixel] <= 19)
        STYLE
            COLORRANGE "#ff8128" "#f96526"
            DATARANGE 18 19
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "19-20"
        EXPRESSION ([pixel] > 19 AND [pixel] <= 20)
        STYLE
            COLORRANGE "#f96526" "#f74900"
            DATARANGE 19 20
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "20-22"
        EXPRESSION ([pixel] > 20 AND [pixel] <= 22)
        STYLE
            COLORRANGE "#f74900" "#e04231"
            DATARANGE 20 22
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "22-24"
        EXPRESSION ([pixel] > 22 AND [pixel] <= 24)
        STYLE
            COLORRANGE "#e04231" "#cc123d"
            DATARANGE 22 24
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "24-27"
        EXPRESSION ([pixel] > 24 AND [pixel] <= 27)
        STYLE
            COLORRANGE "#cc123d" "#7a021e"
            DATARANGE 24 27
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 27"
        EXPRESSION ([pixel] > 27 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#7a021e" "#000000"
            DATARANGE 24 50
            RANGEITEM "pixel"
        END
    END
END

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 20000

    NAME          chm_2m_yearinfo
    GROUP         chm_2m

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "Metsäkeskus Kasvillisuuden korkeusmalli 2m (Vuosittaiset kartoitusalueet)"
        "ows_abstract"          "Metsäkeskus Kasvillisuuden korkeusmalli vuosittaiset kartoitusalueet"
        "ows_group_title"       "Metsäkeskus Kasvillisuuden korkeusmalli 2m"
        "ows_group_abstract"    "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli teemoitus 2m välein"
        "ows_group_name"        "chm_2m"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            vuosi,
            geom as geometry
        FROM
            bag_metsakeskus_tileindex.metsakeskus_chm_2022_yearinfo
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "vuosi"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "2008"
        EXPRESSION ([vuosi] = 2008) 
        STYLE
            COLOR "#f7fcf5"
            OUTLINECOLOR "#f7fcf5"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2009"
        EXPRESSION ([vuosi] = 2009) 
        STYLE
            COLOR "#ecf8e8"
            OUTLINECOLOR "#ecf8e8"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2010"
        EXPRESSION ([vuosi] = 2010) 
        STYLE
            COLOR "#DAFFB5"
            OUTLINECOLOR "#DAFFB5"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2011"
        EXPRESSION ([vuosi] = 2011) 
        STYLE
            COLOR "#caeac3"
            OUTLINECOLOR "#caeac3"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2012"
        EXPRESSION ([vuosi] = 2012) 
        STYLE
            COLOR "#b2e0ab"
            OUTLINECOLOR "#b2e0ab"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2013"
        EXPRESSION ([vuosi] = 2013) 
        STYLE
            COLOR "#98d593"
            OUTLINECOLOR "#98d593"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2014"
        EXPRESSION ([vuosi] = 2014) 
        STYLE
            COLOR "#7bc87c"
            OUTLINECOLOR "#7bc87c"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2015"
        EXPRESSION ([vuosi] = 2015) 
        STYLE
            COLOR "#5bb86a"
            OUTLINECOLOR "#5bb86a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2016"
        EXPRESSION ([vuosi] = 2016) 
        STYLE
            COLOR "#3da75a"
            OUTLINECOLOR "#3da75a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2017"
        EXPRESSION ([vuosi] = 2017) 
        STYLE
            COLOR "#2a924a"
            OUTLINECOLOR "#2a924a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2018"
        EXPRESSION ([vuosi] = 2018) 
        STYLE
            COLOR "#137e3a"
            OUTLINECOLOR "#137e3a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2019"
        EXPRESSION ([vuosi] = 2019) 
        STYLE
            COLOR "#006629"
            OUTLINECOLOR "#006629"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2020"
        EXPRESSION ([vuosi] = 2020) 
        STYLE
            COLOR "#005522"
            OUTLINECOLOR "#005522"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
        CLASS
        NAME "2021"
        EXPRESSION ([vuosi] = 2021) 
        STYLE
            COLOR "#003013"
            OUTLINECOLOR "#003013"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END