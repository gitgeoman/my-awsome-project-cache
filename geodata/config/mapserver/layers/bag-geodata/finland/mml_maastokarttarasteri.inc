#-----------------------------------------------------------------------------------------------------------------------------
# MML Maastokarttasarja / MML Topographic Map
#-----------------------------------------------------------------------------------------------------------------------------
LAYER
    STATUS          OFF
    NAME            "mml_maastokarttarasteri_index"
    
    METADATA
        "ows_title"           "MML Maastokarttarasteri (Index)"
        "ows_abstract"        "MML Maastokarttarasteri (Päivitysindeksi)"

        "ows_srs"             "EPSG:3067"
        "ows_enable_request"  "* !GetCapabilities"
        "wms_include_items"   "all"

        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            id as bag_id,
            file_name as bag_sourcefile,
            file_date as bag_source_ts,
            updated_at as bag_update_ts,
            geom as geometry
        FROM
            mml_maastokarttasarja_jhs180.peruskarttarasteri
    ) as foo using unique bag_id using srid=3067"
END

LAYER
    STATUS          OFF

    MINSCALEDENOM   4000000

    NAME            "mml_yleiskarttarasteri_8000k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Yleiskarttarasteri 8000k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.yleiskarttarasteri_8000k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   4500000
    
    NAME            "mml_yleiskarttarasteri_8000k_jhs180"
    GROUP           "mml_maastokarttarasteri"
    
    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Yleiskarttarasteri 8000k"
        "wms_group_title"       "MML Maastokarttasarja"
        "wms_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
    END

    TILEINDEX "mml_yleiskarttarasteri_8000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END 

LAYER
    STATUS          OFF

    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4500000

    NAME            "mml_yleiskarttarasteri_4500k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Yleiskarttarasteri 4500k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.yleiskarttarasteri_4500k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4500000
    
    NAME            "mml_yleiskarttarasteri_4500k_jhs180"
    GROUP           "mml_maastokarttarasteri"
    
    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Yleiskarttarasteri 4500k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yleiskarttarasteri_4500k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MINSCALEDENOM   1000000
    MAXSCALEDENOM   2000000

    NAME            "mml_yleiskarttarasteri_2000k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Yleiskarttarasteri 2000k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.yleiskarttarasteri_2000k
    ) as foo using unique bag_id using srid=3067"
END

LAYER
    STATUS          OFF
    
    MINSCALEDENOM   1000000
    MAXSCALEDENOM   2000000
    
    NAME            "mml_yleiskarttarasteri_2000k_jhs180"
    GROUP           "mml_maastokarttarasteri"

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"         "MML Yleiskarttarasteri 2000k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yleiskarttarasteri_2000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MINSCALEDENOM   500000
    MAXSCALEDENOM   1000000

    NAME            "mml_yleiskarttarasteri_1000k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Yleiskarttarasteri 1000k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.yleiskarttarasteri_1000k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   500000
    MAXSCALEDENOM   1000000
    
    NAME            "mml_yleiskarttarasteri_1000k_jhs180"
    GROUP           "mml_maastokarttarasteri"
    
    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Yleiskarttarasteri 1000k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yleiskarttarasteri_1000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MINSCALEDENOM   250000
    MAXSCALEDENOM   500000

    NAME            "mml_maastokarttarasteri_500k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Maastokarttarasteri 500k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.maastokarttarasteri_500k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   250000
    MAXSCALEDENOM   500000

    NAME            "mml_maastokarttarasteri_500k_jhs180"
    GROUP           "mml_maastokarttarasteri"
    
    TYPE		RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Maastokarttarasteri 500k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_maastokarttarasteri_500k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"

END

LAYER
    STATUS          OFF

    MINSCALEDENOM   250000
    MAXSCALEDENOM   500000

    NAME            "mml_maastokarttarasteri_250k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Maastokarttarasteri 250k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.maastokarttarasteri_250k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   100000
    MAXSCALEDENOM   250000
    
    NAME            "mml_maastokarttarasteri_250k_jhs180"
    GROUP           "mml_maastokarttarasteri"
        
    TYPE		    RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Maastokarttarasteri 250k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_maastokarttarasteri_250k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"

END

LAYER
    STATUS          OFF

    MINSCALEDENOM   20000
    MAXSCALEDENOM   100000

    NAME            "mml_maastokarttarasteri_100k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Maastokarttarasteri 100k (index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.maastokarttarasteri_100k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   20000
    MAXSCALEDENOM   100000
    
    NAME            mml_maastokarttarasteri_100k_jhs180
    GROUP           mml_maastokarttarasteri
    
    TYPE		    RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Maastokarttarasteri 100k"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_maastokarttarasteri_100k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000

    NAME            "mml_peruskarttarasteri_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Peruskarttarasteri index)"
        "ows_group_title"       "MML Maastokarttasarja"
        "ows_group_abstract"    "MML Maastokarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.peruskarttarasteri
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MAXSCALEDENOM   20000
    
    NAME            "mml_peruskarttarasteri_jhs180"
    GROUP           "mml_maastokarttarasteri"

    TYPE		    RASTER
    
    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Peruskarttarasteri"
        "wms_group_title"   "MML Maastokarttasarja"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_peruskarttarasteri_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END
