LAYER
    STATUS       OFF
    NAME        luke_forest_wind_damage_sensitivity_index

    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT      50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION  
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "LUKE tuulituhoriski (Index)"
        "ows_abstract"          "LUKE tuulituhoriski (Index)"

        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "* !GetCapabilities"
        "wms_include_items"     "all"

        "gml_include_items"     "all"
        "gml_featureid"         "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            geom as geometry
        FROM
            bag_luke_tileindex.luke_forest_wind_damage_sensitivity_tileindex
    ) as foo using unique bag_id using srid=3067"
END

LAYER
    STATUS        OFF

    NAME          luke_forest_wind_damage_sensitivity

    
    TYPE          RASTER
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
        
    METADATA
        "ows_title"             "LUKE tuulituhoriski"
        "ows_abstract"          "LUKE tuulituhoriski"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX   "luke_forest_wind_damage_sensitivity_index"
    TILEITEM    "location"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "-3.5-0"
        EXPRESSION ([pixel] <0 ) 
        STYLE
            COLORRANGE "#FFFFFF" "#ffffcc"
            DATARANGE -3.5 0
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0-0.01"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 0.01)
        STYLE
            COLORRANGE "#ffffcc" "#c7e9b4"
            DATARANGE 0 0.01
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.01-0.02"
        EXPRESSION ([pixel] > 0.01 AND [pixel] <= 0.02)
        STYLE
            COLORRANGE "#c7e9b4" "#7fcdbb"
            DATARANGE 0.01 0.02
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.02-0.03"
        EXPRESSION ([pixel] > 0.02 AND [pixel] <= 0.03)
        STYLE
            COLORRANGE "#7fcdbb" "#41b6c4"
            DATARANGE 0.02 0.03
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.03-0.04"
        EXPRESSION ([pixel] > 0.03 AND [pixel] <= 0.04)
        STYLE
            COLORRANGE "#41b6c4" "#2c7fb8"
            DATARANGE 0.03 0.04
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.04-0.05"
        EXPRESSION ([pixel] > 0.04 AND [pixel] <= 180)
        STYLE
            COLORRANGE "#2c7fb8" "#253494"
            DATARANGE 0.04 0.05
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "0.05-0.06"
        EXPRESSION ([pixel] > 0.05 AND [pixel] <= 0.06)
        STYLE
            COLORRANGE "#253494" "#0c2c84"
            DATARANGE 0.05 0.06
            RANGEITEM "pixel"
        END
    END
END