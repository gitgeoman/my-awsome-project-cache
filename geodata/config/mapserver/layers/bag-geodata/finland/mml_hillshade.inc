#--------------------------------------------------------
# Hillshade layers
#--------------------------------------------------------

LAYER
    STATUS          OFF
    
    MINSCALEDENOM   20001
    
    NAME	        mml_hillshade_index
    GROUP	        mml_hillshade
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "MML Rinnevalovarjostus (Index)"
        "ows_group_title"       "MML Rinnevalovarjostus"
        "ows_abstract"          "MML Rinnevalovarjostus indeksikarttalehdet"
        "ows_group_abstract"    "MML Laserkeilausaineistosta tuotettu rinnevalovarjostus"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_name as bag_sourcefile,
            file_date as bag_source_ts,
            file_path as location,
            updated_at as bag_update_ts,
            geom as geometry
        from
            mml_vinovalovarjoste.vinovalovarjoste_hila_2m
    ) as foo using unique bag_id using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#808080"
        END
    END
END

LAYER
    STATUS        OFF
    MAXSCALEDENOM 20000

    NAME          mml_hillshade_raster
    GROUP         mml_hillshade
    
    TYPE          RASTER
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
        
    METADATA
        "ows_title"             "MML Rinnevalovarjostus"
        "ows_group_title"       "MML Rinnevalovarjostus"
        "ows_abstract"          "MML Laserkeilausaineistosta tuotettu rinnevalovarjostus"
        "ows_group_abstract"    "MML Laserkeilausaineistosta tuotettu rinnevalovarjostus"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX   "mml_hillshade_index"
    TILEITEM    "location"

    PROCESSING  "RESAMPLE=NEAREST"

END