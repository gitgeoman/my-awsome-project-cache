#-----------------------------------------------------------------------------------------------------------------------------
# MML Taustakarttayhdistelmä / MML Background Map
#-----------------------------------------------------------------------------------------------------------------------------

LAYER
    STATUS          OFF
    NAME            "mml_taustakarttayhdistelma_index"
    
    METADATA
        "ows_title"           "MML Taustakarttayhdistelmä (Index)"
        "ows_abstract"        "MML Taustakarttayhdistelmä (Päivitysindeksi)"

        "ows_srs"             "EPSG:3067"
        "ows_enable_request"  "* !GetCapabilities"
        "wms_include_items"   "all"

        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            id as bag_id,
            file_name as bag_sourcefile,
            file_date as bag_source_ts,
            updated_at as bag_update_ts,
            geom as geometry
        FROM
            mml_maastokarttasarja_jhs180.peruskarttarasteri
    ) as foo using unique bag_id using srid=3067"
END

# MML Taustakarttarasteri 1:8m

LAYER
    STATUS          OFF

    MINSCALEDENOM   4000000

    NAME            "mml_yhdistelma_taustakarttarasteri_8000k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Taustakartta 8000k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelmä"
        "ows_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_8m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   4000000
    
    NAME            "mml_yhdistelma_taustakarttarasteri_8000k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 8000k"
        "wms_group_title"       "MML Taustakarttayhdistelmä"
        "wms_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_yhdistelma_taustakarttarasteri_8000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:4m

LAYER
    STATUS          OFF

    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4000000

    NAME            "mml_yhdistelma_taustakarttarasteri_4000k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Taustakartta 4000k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelmä"
        "ows_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_4m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4000000

    NAME            "mml_yhdistelma_taustakarttarasteri_4000k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
    
    TYPE            RASTER
    TEMPLATE        "foo"
    
    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 4000k"
        "wms_group_title"       "MML Taustakarttayhdistelmä"
        "wms_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_yhdistelma_taustakarttarasteri_4000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:2m

LAYER
    STATUS          OFF

    MINSCALEDENOM   800000
    MAXSCALEDENOM   2000000

    NAME            "mml_yhdistelma_taustakarttarasteri_2000k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Taustakartta 2000k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelmä"
        "ows_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_2m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   800000
    MAXSCALEDENOM   2000000

    NAME            "mml_yhdistelma_taustakarttarasteri_2000k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902   
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Taustakartta 2000k"
        "wms_group_title"       "MML Taustakarttayhdistelmä"
        "wms_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_yhdistelma_taustakarttarasteri_2000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:800k

LAYER
    STATUS          OFF

    MINSCALEDENOM   320000
    MAXSCALEDENOM   800000

    NAME            "mml_yhdistelma_taustakarttarasteri_800k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Taustakartta 800k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelmä"
        "ows_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_800k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   320000
    MAXSCALEDENOM   800000

    NAME            "mml_yhdistelma_taustakarttarasteri_800k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 800k"
        "wms_group_title"       "MML Taustakarttayhdistelmä"
        "wms_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_yhdistelma_taustakarttarasteri_800k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:320k

LAYER
    STATUS          OFF

    MINSCALEDENOM   160000
    MAXSCALEDENOM   320000

    NAME            "mml_yhdistelma_taustakarttarasteri_320k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Taustakartta 320k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelmä"
        "ows_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_320k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   160000
    MAXSCALEDENOM   320000

    NAME            "mml_yhdistelma_taustakarttarasteri_320k_jhs180"
    GROUP           "mml_taustakarttarasteri"
    
    TYPE            RASTER
    TEMPLATE        "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Taustakartta 320k"
        "wms_group_title"       "MML Taustakarttayhdistelmä"
        "wms_group_abstract"    "MML Taustakarttayhdistelmä (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_yhdistelma_taustakarttarasteri_320k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MINSCALEDENOM   100000
    MAXSCALEDENOM   320000

    NAME            "mml_yhdistelma_maastokarttarasteri_250k_jhs180_tindex"
    GROUP           "mml_maastokarttarasteri"

    METADATA
        "ows_title"             "MML Maastokarttarasteri 250k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelma"
        "ows_group_abstract"    "MML Taustakarttayhdistelma (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.maastokarttarasteri_250k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   100000
    MAXSCALEDENOM   320000
    
    NAME            "mml_yhdistelma_maastokarttarasteri_250k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
        
    TYPE		    RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Maastokarttarasteri 250k"
        "wms_group_title"   "MML Taustakarttayhdistelma"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yhdistelma_maastokarttarasteri_250k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"

END

LAYER
    STATUS          OFF

    MINSCALEDENOM   20000
    MAXSCALEDENOM   100000

    NAME            "mml_yhdistelma_maastokarttarasteri_100k_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Maastokarttarasteri 100k (index)"
        "ows_group_title"       "MML Taustakarttayhdistelma"
        "ows_group_abstract"    "MML Taustakarttayhdistelma (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.maastokarttarasteri_100k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   20000
    MAXSCALEDENOM   100000
    
    NAME            "mml_yhdistelma_maastokarttarasteri_100k_jhs180"
    GROUP           "mml_taustakarttayhdistelma"
    
    TYPE		    RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Maastokarttarasteri 100k"
        "wms_group_title"   "MML Taustakarttayhdistelma"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yhdistelma_maastokarttarasteri_100k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000

    NAME            "mml_yhdistelma_peruskarttarasteri_jhs180_tindex"
    GROUP           "mml_taustakarttayhdistelma"

    METADATA
        "ows_title"             "MML Peruskarttarasteri index)"
        "ows_group_title"       "MML Taustakarttayhdistelma"
        "ows_group_abstract"    "MML Taustakarttayhdistelma (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_maastokarttasarja_jhs180.peruskarttarasteri
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MAXSCALEDENOM   20000
    
    NAME            "mml_yhdistelma_peruskarttarasteri_jhs180"
    GROUP           "mml_taustakarttayhdistelma"

    TYPE		    RASTER
    
    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"         "MML Peruskarttarasteri"
        "wms_group_title"   "MML Taustakarttayhdistelma"
        "wms_srs"           "EPSG:3067"
    END

    TILEINDEX "mml_yhdistelma_peruskarttarasteri_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END
