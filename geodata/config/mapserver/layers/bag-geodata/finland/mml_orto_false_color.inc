LAYER
    STATUS OFF

    MAXSCALEDENOM   60000

    NAME            mml_orto_false_color
    GROUP           mml_orto_false_color

    TEMPLATE      "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    TYPE            RASTER
    PROCESSING      "RESAMPLE=NEAREST"

    CONNECTION "https://sopimus-karttakuva.maanmittauslaitos.fi/sopimus/service/wms?"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"             "EPSG:3067"
        "wms_name"            "ortokuva_vaaravari"
        "wms_title"           "MML Ortokuva (Vääräväri)"
        "wms_abstract"        "MML Vääräväri-ilmakuva"
        "wms_server_version"  "1.1.0"
        "wms_format"          "image/jpeg"
        "wms_auth_username"   "bitcomp1"
        "wms_auth_password"   "QAJfjwMQ2a9yfBOWqukI"
        "wms_bgcolor" 	      "0xFFFFFF"
        "wms_enable_request"   "*"
        "wms_info_format"      "application/json"

        "ows_group_title"     "MML Ortokuva (Vääräväri)"
        "ows_group_abstract"  "MML Ortokuva (Vääräväri)"
    END

END