LAYER
    STATUS        OFF
    
    MINSCALEDENOM 20000

    NAME          alhstrom_aerial_index
    GROUP         ahlstrom_aerial
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    TEMPLATE      "foo"
    TYPE          POLYGON
    
    METADATA
        "ows_title"             "Ahlstrom Ortokuva indeksi (RGB)"
        "ows_abstract"          "Ahlstrom Ortokuva indeksikarttalehdet (RGB)"
        "ows_group_title"       "Ahlstrom Ortokuva (RGB)"
        "ows_group_abstract"    "Ahlstrom Ortokuva (RGB)"
        "ows_group_name"        "ahlstrom_aerial"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    DATA          "layers/bag-geodata/finland/ahlstrom/aerial_2015_tileindex"
    
    LABELITEM     "year"
    CLASSITEM     "year"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "2015"
        EXPRESSION ([year] = 2015) 
        STYLE
            COLOR "#4eb3d3"
            OUTLINECOLOR "#4eb3d3"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 20000
    
    NAME          ahlstrom_aerial_raster
    GROUP         ahlstrom_aerial
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "Ahlstrom Ortokuva (RGB)"
        "ows_abstract"          "Ahlstrom Ortokuva (RGB)"
        "ows_group_title"       "Ahlstrom Ortokuva (RGB)"
        "ows_group_abstract"    "Ahlstrom Ortokuva (RGB)"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX   "layers/bag-geodata/finland/ahlstrom/aerial_2015_tileindex"
    TILEITEM    "location"

    TYPE        RASTER
    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"
    OFFSITE     0 0 0

END