#-----------------------------------------------------------------------------------------------------------------------------
# MML Taustakarttasarja / MML Background Map
#-----------------------------------------------------------------------------------------------------------------------------

LAYER
    STATUS          OFF
    NAME            "mml_taustakarttarasteri_index"
    
    METADATA
        "ows_title"           "MML Taustakarttarasteri (Index)"
        "ows_abstract"        "MML Taustakarttarasteri (Päivitysindeksi)"

        "ows_srs"             "EPSG:3067"
        "ows_enable_request"  "* !GetCapabilities"
        "wms_include_items"   "all"

        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
       SELECT
            id as bag_id,
            file_name as bag_sourcefile,
            file_date as bag_source_ts,
            updated_at as bag_update_ts,
            geom as geometry
        FROM
            mml_taustakarttasarja_jhs180.taustakartta_5k
    ) as foo using unique bag_id using srid=3067"
END

# MML Taustakarttarasteri 1:8m

LAYER
    STATUS          OFF

    MINSCALEDENOM   4000000

    NAME            "mml_taustakarttarasteri_8000k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 8000k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_8m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   4000000
    
    NAME            mml_taustakarttarasteri_8000k_jhs180
    GROUP           mml_taustakarttarasteri
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 8000k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_8000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:4m

LAYER
    STATUS          OFF

    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4000000

    NAME            "mml_taustakarttarasteri_4000k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 4000k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_4m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   2000000
    MAXSCALEDENOM   4000000

    NAME            mml_taustakarttarasteri_4000k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"
    
    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 4000k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_4000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:2m

LAYER
    STATUS          OFF

    MINSCALEDENOM   800000
    MAXSCALEDENOM   2000000

    NAME            "mml_taustakarttarasteri_2000k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 2000k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_2m
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   800000
    MAXSCALEDENOM   2000000

    NAME            mml_taustakarttarasteri_2000k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902   
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Taustakartta 2000k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_2000k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:800k

LAYER
    STATUS          OFF

    MINSCALEDENOM   320000
    MAXSCALEDENOM   800000

    NAME            "mml_taustakarttarasteri_800k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 800k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_800k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   320000
    MAXSCALEDENOM   800000

    NAME            mml_taustakarttarasteri_800k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 800k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_800k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:320k

LAYER
    STATUS          OFF

    MINSCALEDENOM   160000
    MAXSCALEDENOM   320000

    NAME            "mml_taustakarttarasteri_320k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 320k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_320k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   160000
    MAXSCALEDENOM   320000

    NAME            mml_taustakarttarasteri_320k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Taustakartta 320k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_320k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:160k 

LAYER
    STATUS          OFF

    MINSCALEDENOM   80000
    MAXSCALEDENOM   160000

    NAME            "mml_taustakarttarasteri_160k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 160k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_160k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF
    
    MINSCALEDENOM   80000
    MAXSCALEDENOM   160000
    
    NAME            mml_taustakarttarasteri_160k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
  
    METADATA
        "wms_title"             "MML Taustakartta 160k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_160k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:80k 

LAYER
    STATUS          OFF

    MINSCALEDENOM   40000
    MAXSCALEDENOM   80000

    NAME            "mml_taustakarttarasteri_80k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 80k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_80k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   40000
    MAXSCALEDENOM   80000
    
    NAME            mml_taustakarttarasteri_80k_jhs180
    GROUP           mml_taustakarttarasteri
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 80k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_80k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:40k 

LAYER
    STATUS          OFF

    MINSCALEDENOM   20000
    MAXSCALEDENOM   40000

    NAME            "mml_taustakarttarasteri_40k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 40k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_40k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   20000
    MAXSCALEDENOM   40000
    
    NAME            mml_taustakarttarasteri_40k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"             "MML Taustakartta 40k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_40k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:20k

LAYER
    STATUS          OFF

    MINSCALEDENOM   10000
    MAXSCALEDENOM   20000

    NAME            "mml_taustakarttarasteri_20k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 20k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_20k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   10000
    MAXSCALEDENOM   20000
    
    NAME            mml_taustakarttarasteri_20k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902  
    PROJECTION
        "init=epsg:3067"
    END
   
    METADATA
        "wms_title"             "MML Taustakartta 20k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_20k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:10k

LAYER
    STATUS          OFF

    MINSCALEDENOM   5000
    MAXSCALEDENOM   10000

    NAME            "mml_taustakarttarasteri_10k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 10k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_10k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MINSCALEDENOM   5000
    MAXSCALEDENOM   10000
    
    NAME            mml_taustakarttarasteri_10k_jhs180
    GROUP           mml_taustakarttarasteri
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
   
    METADATA
        "wms_title"             "MML Taustakartta 10k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_10k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END

# MML Taustakarttarasteri 1:5k

LAYER
    STATUS          OFF
    MAXSCALEDENOM   5000

    NAME            "mml_taustakarttarasteri_5k_jhs180_tindex"
    GROUP           "mml_taustakarttarasteri"

    METADATA
        "ows_title"             "MML Taustakartta 5k (index)"
        "ows_group_title"       "MML Taustakarttasarja"
        "ows_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id as bag_id,
            file_path as location,
            geom as geometry
        from
            mml_taustakarttasarja_jhs180.taustakartta_5k
    ) as foo using unique bag_id using srid=3067"
END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   5000
    
    NAME            mml_taustakarttarasteri_5k_jhs180
    GROUP           mml_taustakarttarasteri
    
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "wms_title"             "MML Taustakartta 5k"
        "wms_group_title"       "MML Taustakarttasarja"
        "wms_group_abstract"    "MML Taustakarttasarja (JHS-180)"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "mml_taustakarttarasteri_5k_jhs180_tindex"
    PROCESSING "RESAMPLE=AVERAGE"
END
