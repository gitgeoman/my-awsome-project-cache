LAYER
    STATUS        OFF
    
    NAME          metsakeskus_korjuukelpoisuus
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_srs"             "EPSG:3067"
        "wms_title"           "Metsäkeskus Korjuukelpoisuus"
        "wms_abstract"        "Metsäkeskus Korjuukelpoisuus"
        "gml_include_items"   "all"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "korjuukelpoisuus"
    END

    DATA   "/vsis3/bag-prod-geodata/public/finland/metsakeskus/korjuukelpoisuus/KKL_SMK_Suomi_2021_06_01_cog.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "Kelirikko"
        EXPRESSION ([pixel] = 1)
        STYLE
            COLOR "#09600B"
        END
    END
    CLASS
        NAME "Normaali kesä, kivennäismaa"
        EXPRESSION ([pixel] = 2)
        STYLE
            COLOR "#619B1A"
        END
    END
    CLASS
        NAME "Kuiva kesä, kivennäismaa"
        EXPRESSION ([pixel] = 3)
        STYLE
            COLOR "#A0DD2B"
        END
    END
    CLASS
        NAME "Normaali kesä, turvemaa"
        EXPRESSION ([pixel] = 4)
        STYLE
            COLOR "#C5BF34"
        END
    END
    CLASS
        NAME "Kuiva kesä, turvemaa"
        EXPRESSION ([pixel] = 5)
        STYLE
            COLOR "#FD8625"
        END
    END
    CLASS
        NAME "Talvi"
        EXPRESSION ([pixel] = 6)
        STYLE
           COLOR "#FC231C"
        END
    END
    CLASS
        NAME "Vesistö"
        EXPRESSION ([pixel] = 254)
        STYLE
           COLOR "#80FFFD"
        END
    END

END