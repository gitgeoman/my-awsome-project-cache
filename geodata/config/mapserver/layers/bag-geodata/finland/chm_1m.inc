#-----------------------------------------------------
# CHM layers
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 20000

    NAME          chm_1m_index
    GROUP         chm_1m

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "Metsäkeskus Kasvillisuuden korkeusmalli 1m (Index)"
        "ows_abstract"          "Metsäkeskus Kasvillisuuden korkeusmalli indeksikarttalehdet"
        "ows_group_title"       "Metsäkeskus Kasvillisuuden korkeusmalli 1m"
        "ows_group_abstract"    "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli teemoitus 1m välein"
        "ows_group_name"        "chm_1m"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            mapsheet,
            filename,
            location,
            year,
            geom as geometry
        FROM
            bag_metsakeskus_tileindex.metsakeskus_chm_2022
    ) as foo using unique bag_id using srid=3067"

END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 19999
    
    NAME          chm_1m_raster
    GROUP         chm_1m
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Kasvillisuuden korkeusmalli 1m (Raster)"
        "ows_abstract"        "Metsäkeskus Kasvillisuuden korkeusmalli indeksikarttalehdet"
        "ows_group_title"     "Metsäkeskus Kasvillisuuden korkeusmalli 1m"
        "ows_group_abstract"  "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli 1m"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "chm_1m_index"
    TILEITEM    "location"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

 	CLASS
        NAME "1"
        EXPRESSION ([pixel] >= 0.5 AND [pixel] < 1.5)
        STYLE
            COLORRANGE "#FFFFFF" "#CFEFFF"
            DATARANGE 0.5 1.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "2"
        EXPRESSION ([pixel] >= 1.5 AND [pixel] < 2.5)
        STYLE
            COLORRANGE "#CFEFFF" "#58C2FF"
            DATARANGE 1.5 2.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "3"
        EXPRESSION ([pixel] >= 2.5 AND [pixel] < 3.5)
        STYLE
            COLORRANGE "#58C2FF" "#6060FF"
            DATARANGE 2.5 3.5
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "4"
        EXPRESSION ([pixel] >= 3.5 AND [pixel] < 4.5)
        STYLE
            COLORRANGE "#6060FF" "#0000FF"
            DATARANGE 3.5 4.5
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5"
        EXPRESSION ([pixel] >= 4.5 AND [pixel] < 5.5)
        STYLE
            COLORRANGE "#0000FF" "#A6FFA7"
            DATARANGE 4.5 5.5
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6"
        EXPRESSION ([pixel] >= 5.5 AND [pixel] < 6.5)
        STYLE
            COLORRANGE "#A6FFA7" "#40FF40"
            DATARANGE 5.5 6.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "7"
        EXPRESSION ([pixel] >= 6.5 AND [pixel] < 7.5)
        STYLE
            COLORRANGE "#40FF40" "#00CC00"
            DATARANGE 6.5 7.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "8"
        EXPRESSION ([pixel] >= 7.5 AND [pixel] < 8.5)
        STYLE
            COLORRANGE "#00CC00" "#FFFF67"
            DATARANGE 7.5 8.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "9"
        EXPRESSION ([pixel] >= 8.5 AND [pixel] < 9.5)
        STYLE
            COLORRANGE "#FFFF67" "#FFFF0D"
            DATARANGE 8.5 9.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "10"
        EXPRESSION ([pixel] >= 9.5 AND [pixel] < 10.5)
        STYLE
            COLORRANGE "#FFFF0D" "#F1F000"
            DATARANGE 9.5 10.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "11"
        EXPRESSION ([pixel] >= 10.5 AND [pixel] < 11.5)
        STYLE
            COLORRANGE "#F1F000" "#FFC890"
            DATARANGE 10.5 11.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "12"
        EXPRESSION ([pixel] >= 11.5 AND [pixel] < 12.5)
        STYLE
            COLORRANGE "#FFC890" "#FF972E"
            DATARANGE 11.5 12.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "13"
        EXPRESSION ([pixel] >= 12.5 AND [pixel] < 13.5)
        STYLE
            COLORRANGE "#FF972E" "#DF6F00"
            DATARANGE 13.5 12.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "14"
        EXPRESSION ([pixel] >= 13.5 AND [pixel] < 14.5)
        STYLE
            COLORRANGE "#DF6F00" "#FF6C6C"
            DATARANGE 13.5 14.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "15"
        EXPRESSION ([pixel] >= 14.5 AND [pixel] < 15.5)
        STYLE
            COLORRANGE "#FF6C6C" "#FF2F2F"
            DATARANGE 14.5 15.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "16"
        EXPRESSION ([pixel] >= 15.5 AND [pixel] < 16.5)
        STYLE
            COLORRANGE "#FF2F2F" "#DC0000"
            DATARANGE 15.5 16.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "17"
        EXPRESSION ([pixel] >= 16.5 AND [pixel] < 17.5)
        STYLE
            COLORRANGE "#DC0000" "#FF0080"
            DATARANGE 16.5 17.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "18"
        EXPRESSION ([pixel] >= 17.5 AND [pixel] < 18.5)
        STYLE
            COLORRANGE "#FF0080" "#FF2FFF"
            DATARANGE 17.5 18.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "19"
        EXPRESSION ([pixel] >= 18.5 AND [pixel] < 19.5)
        STYLE
            COLORRANGE "#FF2FFF" "#C200C1"
            DATARANGE 18.5 19.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "20"
        EXPRESSION ([pixel] >= 19.5 AND [pixel] < 20.5)
        STYLE
            COLORRANGE "#C200C1" "#81007F"
            DATARANGE 19.5 20.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "21"
        EXPRESSION ([pixel] >= 20.5 AND [pixel] < 21.5)
        STYLE
            COLORRANGE "#81007F" "#994D00"
            DATARANGE 20.5 21.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "22"
        EXPRESSION ([pixel] >= 21.5 AND [pixel] < 22.5)
        STYLE
            COLORRANGE "#994D00" "#552C00"
            DATARANGE 21.5 22.5
            RANGEITEM "pixel"
        END
    END
	CLASS
        NAME "> 23"
        EXPRESSION ([pixel] >= 22.5 AND [pixel] < 50)
        STYLE
            COLORRANGE "#552C00" "#000040"
            DATARANGE 22.5 50
            RANGEITEM "pixel"
        END
    END

END

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 20000

    NAME          chm_1m_yearinfo
    GROUP         chm_1m

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"             "Metsäkeskus Kasvillisuuden korkeusmalli 1m (Vuosittaiset kartoitusalueet)"
        "ows_abstract"          "Metsäkeskus Kasvillisuuden korkeusmalli vuosittaiset kartoitusalueet"
        "ows_group_title"       "Metsäkeskus Kasvillisuuden korkeusmalli 1m"
        "ows_group_abstract"    "Metsäkeskus Laserkeilausaineistosta tuotettu kasvillisuuden korkeusmalli teemoitus 1m välein"
        "ows_group_name"        "chm_1m"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            vuosi,
            geom as geometry
        FROM
            bag_metsakeskus_tileindex.metsakeskus_chm_2022_yearinfo
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "vuosi"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "2008"
        EXPRESSION ([vuosi] = 2008) 
        STYLE
            COLOR "#f7fcf5"
            OUTLINECOLOR "#f7fcf5"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2009"
        EXPRESSION ([vuosi] = 2009) 
        STYLE
            COLOR "#ecf8e8"
            OUTLINECOLOR "#ecf8e8"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2010"
        EXPRESSION ([vuosi] = 2010) 
        STYLE
            COLOR "#DAFFB5"
            OUTLINECOLOR "#DAFFB5"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2011"
        EXPRESSION ([vuosi] = 2011) 
        STYLE
            COLOR "#caeac3"
            OUTLINECOLOR "#caeac3"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2012"
        EXPRESSION ([vuosi] = 2012) 
        STYLE
            COLOR "#b2e0ab"
            OUTLINECOLOR "#b2e0ab"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2013"
        EXPRESSION ([vuosi] = 2013) 
        STYLE
            COLOR "#98d593"
            OUTLINECOLOR "#98d593"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2014"
        EXPRESSION ([vuosi] = 2014) 
        STYLE
            COLOR "#7bc87c"
            OUTLINECOLOR "#7bc87c"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2015"
        EXPRESSION ([vuosi] = 2015) 
        STYLE
            COLOR "#5bb86a"
            OUTLINECOLOR "#5bb86a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2016"
        EXPRESSION ([vuosi] = 2016) 
        STYLE
            COLOR "#3da75a"
            OUTLINECOLOR "#3da75a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2017"
        EXPRESSION ([vuosi] = 2017) 
        STYLE
            COLOR "#2a924a"
            OUTLINECOLOR "#2a924a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2018"
        EXPRESSION ([vuosi] = 2018) 
        STYLE
            COLOR "#137e3a"
            OUTLINECOLOR "#137e3a"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2019"
        EXPRESSION ([vuosi] = 2019) 
        STYLE
            COLOR "#006629"
            OUTLINECOLOR "#006629"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "2020"
        EXPRESSION ([vuosi] = 2020) 
        STYLE
            COLOR "#005522"
            OUTLINECOLOR "#005522"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
        CLASS
        NAME "2021"
        EXPRESSION ([vuosi] = 2021) 
        STYLE
            COLOR "#003013"
            OUTLINECOLOR "#003013"
        END
        LABEL
            MINSCALEDENOM 20000
            MAXSCALEDENOM 200000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END