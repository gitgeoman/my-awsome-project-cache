LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 20000
    
    NAME          mml_dem_02m_contour
    
    TYPE           LINE
    CONNECTIONTYPE CONTOUR

    TEMPLATE       "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"           "MML Korkeuskäyrät (2m DEM)"
        "wms_abstract"        "MML Korkeuskäyrät (2m DEM)"
        "wms_srs"             "EPSG:3067"

        # disable wfs
        "wfs_enable_request"  "!GetCapabilities"
        
        "gml_include_items"   "all"
    END

    DATA "layers/bag-geodata/finland/mml_dem_02m_tileindex/mml_dem_02m.vrt"

    PROCESSING "BANDS=1"
    PROCESSING "CONTOUR_ITEM=elevation"
    PROCESSING "CONTOUR_INTERVAL=0,1000:1"
    PROCESSING "CONTOUR_INTERVAL=1000,10000:2"
    PROCESSING "CONTOUR_INTERVAL=10000,20000:5"

    GEOMTRANSFORM (smoothsia([shape], 3, 1))

    CLASS
        STYLE
            WIDTH 1
            COLOR "#AB5700"
        END
        LABEL
            MAXSCALEDENOM 5000
            TEXT (round([elevation], 2))
            TYPE TRUETYPE
            PARTIALS FALSE
            FONT scb
            SIZE 8
            MAXOVERLAPANGLE 22.5
            MINDISTANCE 500
            REPEATDISTANCE 1000
            ANGLE FOLLOW
            COLOR "#AB5700"
            OUTLINECOLOR 255 255 255
            OUTLINEWIDTH 1
            MINFEATURESIZE AUTO
            BUFFER 3
        END
    END
    
END