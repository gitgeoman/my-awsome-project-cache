LAYER
    STATUS        OFF
    
    NAME          luke_vmi_tilavuus_muulptukki
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Luke VMI 2019 Tilavuus (Muu lehtipuu -tukki)"
        "ows_abstract"        "Luke VMI 2019 Muu lehtipuu -tukki m3/ha"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-prod-geodata/public/finland/luke/vmi_2019/luke_vmi2019_tilavuus_muulptukki_opt.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "Tilavuus (m3/ha)"
        EXPRESSION ([pixel] < 0)
    END
    CLASS
        NAME "0-30"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 30)
        STYLE
            COLORRANGE "#FFFFFF" "#D9EF8B"
            DATARANGE 0 30
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "30-60"
        EXPRESSION ([pixel] > 30 AND [pixel] <= 60)
        STYLE
            COLORRANGE "#D9EF8B" "#a6d96a"
            DATARANGE 30 60
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "60-90"
        EXPRESSION ([pixel] > 60 AND [pixel] <= 90)
        STYLE
            COLORRANGE "#a6d96a" "#66bd63"
            DATARANGE 60 90
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "90-120"
        EXPRESSION ([pixel] > 90 AND [pixel] <= 120)
        STYLE
            COLORRANGE "#66bd63" "#1a9850"
            DATARANGE 90 120
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "120-150"
        EXPRESSION ([pixel] > 120 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#1a9850" "#006837"
            DATARANGE 120 150
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "150-180"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 180)
        STYLE
            COLORRANGE "#006837" "#fdae61"
            DATARANGE 150 180
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "180-210"
        EXPRESSION ([pixel] > 180 AND [pixel] <= 210)
        STYLE
            COLORRANGE "#fdae61" "#f46d43"
            DATARANGE 180 210
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "210-240"
        EXPRESSION ([pixel] > 210 AND [pixel] <= 240)
        STYLE
            COLORRANGE "#f46d43" "#d73027"
            DATARANGE 210 240
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "240-270"
        EXPRESSION ([pixel] > 240 AND [pixel] <= 270)
        STYLE
            COLORRANGE "#d73027" "#a50026"
            DATARANGE 240 270
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 270"
        EXPRESSION ([pixel] > 270 AND [pixel] <= 1000)
        STYLE
            COLORRANGE "#a50026" "#000000"
            DATARANGE 270 1000
            RANGEITEM "pixel"
        END
    END

END