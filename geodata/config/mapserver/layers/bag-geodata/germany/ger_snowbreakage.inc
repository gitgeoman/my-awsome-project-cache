LAYER
    STATUS        OFF

    NAME          ger_snowbreakage

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Snowbreakage of Kempten Region"
        "ows_abstract"        "Snowbreakage of Kempten Region"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/germany/snowbreakage/kempten_opt.tif"
    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "Snowbreakage (index value)"
        EXPRESSION ([pixel] < 10)
        STYLE
            COLOR "#00000000"
        END
    END
    CLASS
        NAME "1 -Low"
        EXPRESSION ([pixel] >= 10 AND [pixel] < 11.9611554)
        STYLE
            COLORRANGE "#f7fbff" "#c8ddf0"
            DATARANGE 10 11.9611554
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "2"
        EXPRESSION ([pixel] >= 11.9611554 AND [pixel] < 13.9223109)
        STYLE
            COLORRANGE "#c8ddf0" "#73b3d8"
            DATARANGE 11.9611554 13.9223109
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3"
        EXPRESSION ([pixel] >= 13.9223109 AND [pixel] < 15.8834663)
        STYLE
            COLORRANGE "#73b3d8" "#2879b9"
            DATARANGE 13.9223109 15.8834663
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "4"
        EXPRESSION ([pixel] >= 15.8834663 AND [pixel] < 17.8446217)
        STYLE
            COLORRANGE "#2879b9" "#08306b"
            DATARANGE 15.8834663 17.8446217
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5 -High"
        EXPRESSION ([pixel] >= 17.8446217)
        STYLE
            COLOR "#08306b"
        END
    END
END