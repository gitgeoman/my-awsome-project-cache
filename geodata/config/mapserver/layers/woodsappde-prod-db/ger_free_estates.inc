LAYER
    STATUS       OFF
    
    NAME         "free_estates"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 50000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Free estates"
        "ows_abstract"        "Free estates - Woodsapp Germany"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "estate_id"
    END

    INCLUDE "includes/connections/WDE_prod_db.inc"
    
    DATA "geometry from (select
        id as estate_id,
        cadastre_no,
        forest_org_id,
        area,
        description,
        gemarkung_id,
        government_district_id,
        location_name,
        geom as geometry
    from
        public.estates
    where
        owner_id is null 
        and 
        gemarkung_id = %filter_gemarkung_id%
    ) as foo using unique estate_id using srid=3857"
    
    VALIDATION
       'filter_gemarkung_id' '^[0-9]{1,5}$'
    END
    
    CLASS
        NAME "Free estates"
        STYLE
            OUTLINECOLOR "#ff9900"
            WIDTH 1
        END
    END

END