LAYER
    STATUS       OFF
    
    NAME         "ger_estates_for_forest_org_test"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 50000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Estates for Forest Organization TEST"
        "ows_abstract"        "Estates for Forest Organization - Woodsapp Germany TEST"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "estate_id"
    END

    INCLUDE "includes/connections/test-woodsappde-test-db.inc"
    
    DATA "geometry from (select
        id as estate_id,
        cadastre_no,
        forest_org_id,
        owner_id,
        area,
        description,
        gemarkung_id,
        government_district_id,
        location_name,
        geom as geometry
    from
        public.estates
    where 
        forest_org_id = %filter_forest_org_id%
    ) as foo using unique estate_id using srid=3857"
    
    VALIDATION
       'filter_forest_org_id' '^[0-9]{1,5}$'
    END
    
    CLASS
        NAME "Estates for Forest Organization TEST"
        STYLE
            OUTLINECOLOR "#ff9900"
            WIDTH 1
        END
    END

END
