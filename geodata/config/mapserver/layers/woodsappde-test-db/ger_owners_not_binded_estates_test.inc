LAYER
    STATUS       OFF
    
    NAME         "ger_owners_not_binded_estates_test"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 50000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Owners estates not binded to FO TEST"
        "ows_abstract"        "Owners estates not binded to - Woodsapp Germany TEST"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "estate_id"
    END

    INCLUDE "includes/connections/test-woodsappde-test-db.inc"
    
    DATA "geometry from (select
        id as estate_id,
        cadastre_no,
        area,
        description,
        gemarkung_id,
        government_district_id,
        location_name,
        geom as geometry
    from
        public.estates
    where
        owner_id is not null 
        and
        forest_org_id is null 
    ) as foo using unique estate_id using srid=3857"
    
    CLASS
        NAME "Estates for Forest Organization TEST"
        STYLE
            OUTLINECOLOR "#ff9900"
            WIDTH 1
        END
    END

END
