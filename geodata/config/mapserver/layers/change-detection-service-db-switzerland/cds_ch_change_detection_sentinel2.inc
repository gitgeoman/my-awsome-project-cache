LAYER
    STATUS          OFF
    MAXSCALEDENOM   50000

    NAME            "cds_ch_change_detection_sentinel2"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT        570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "CDS Change detection features (Switzerland)"
        "ows_abstract"        "CDS Change detection features (Switzerland)"
        "ows_group_title"     "CDS Change detection features"
        "ows_group_abstract"  "CDS Change detection features"
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "id,created_at,date1,date2,change_type,name"
        "gml_exclude_items"   "range,age"
        "gml_featureid"       "id"
        "WMS_LAYER_GROUP"     "/cds_eu_change_detection_sentinel2/cds_ch_change_detection_sentinel2"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        select
            id,
            created_at,
            date1,
            date2,
            to_char(date2::date, 'DD.MM.YYYY') as date_label,
            now()::date - date2::date AS age,
            change_typ as change_type,
            name,
            geom
        from
            change_detection_features_switzerland
        where
            date2::date >= (current_date - INTERVAL '6 months')
        order by
            date2 desc
    ) as foo using unique id using srid=3857"


    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))
    
    COMPOSITE
        OPACITY 50
    END

   CLASS
        NAME "< 2 Monate" #< 2 months#
        EXPRESSION ([age] <= 60)
        STYLE
            COLOR "#C70039"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[date_label]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "> 2 Monate" #> 2 months#
        EXPRESSION ([age] > 60)
        STYLE
            COLOR "#ff8000"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[date_label]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END