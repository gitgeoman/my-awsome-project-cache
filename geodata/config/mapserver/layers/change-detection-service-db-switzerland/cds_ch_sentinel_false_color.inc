LAYER
    STATUS          OFF
    # MINSCALEDENOM   100000

    NAME            "cds_ch_sentinel_false_color_tileindex"

    METADATA
        "ows_title"             "CDS Sentinel False Color (index)"
        "ows_group_title"       "CDS Sentinel False Color"
        "ows_group_abstract"    "CDS Sentinel False Color"
        "ows_srs"               "EPSG:3857"
        "ows_enable_request"    "!GetCapabilities"
        "gml_include_items"     "date"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"

    DATA "geometry from (
        select
            map_raster_switzerland.id,
            to_char(map_raster_switzerland.date, 'YYYY-MM-DD') as date,
            map_raster_switzerland.location,
            map_raster_switzerland.geometry
        from
            map_raster_switzerland
        join (select
            max(id) as id,
            max(date) as date,
            x_origin,
            y_origin
            from
                map_raster_switzerland
            where
                raster_type = 'FCI'
            group by
                x_origin,
                y_origin
            order by
                date desc
        ) as latest on
            map_raster_switzerland.id = latest.id
    ) as foo using unique id using srid=3857"

    # LABELITEM "date"

    # COMPOSITE
    #   OPACITY 50
    # END

    # CLASS
    #    STYLE
    #        OUTLINECOLOR "#808080"
    #        COLOR "#DF8AB2"
    #        WIDTH 2
    #    END
    #    LABEL
    #        MINSCALEDENOM 5000
    #        MAXSCALEDENOM 200000
    #        FONT scb
    #        TYPE truetype
    #        SIZE 10
    #        COLOR "#746E70"
    #        OUTLINECOLOR "#FFFFFF"
    #        OUTLINEWIDTH 1
    #        ALIGN CENTER
    #    END
    # END
END

LAYER
    STATUS          OFF
    MAXSCALEDENOM   200000
    
    NAME            cds_ch_sentinel_false_color
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT         570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"             "CDS Sentinel False Color"
        "ows_group_title"       "CDS Sentinel False Color"
        "ows_group_abstract"    "CDS Sentinel False Color"
        "wms_srs"               "EPSG:3857"
        "gml_include_items"     "all"
    END

    TILEINDEX   "cds_ch_sentinel_false_color_tileindex"
    TILEITEM    "location"
    
    PROCESSING "RESAMPLE=NEAREST"
END