LAYER
    STATUS       OFF
    MAXSCALEDENOM 100000
    NAME        "ai_fi_thinning_need_shrub_method"

    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine - thinning need areas OF (Shrub method)"
        "ows_abstract"        "AI Engine - thinning need areas OF (Shrub method)"
        "ows_group_title"     "AI Engine - thinning need areas and clusters OF (Shrub method)"
        "ows_group_abstract"  "AI Engine - thinning need areas and clusters OF (Shrub method)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,             
            det_type, 
            det_value, 
            det_rel, 
            source,
            model_ver,
            'thinning_need_shrub_index_areas' as output,
            geom as geometry 
        from 
            bag_ai_engine_output.thinning_needs_shrub_index_fi
        )as foo using unique bag_id using srid=3067"

    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))
    
    CLASS
        NAME "Thinning need"
        STYLE
            SYMBOL "hatch"
            COLOR "#00ccff"
            ANGLE 45
            SIZE 15.0
            WIDTH 2.0
            PATTERN 20 10 10 10 END
        END                  
        STYLE
            COLOR "#deb377"
            OPACITY 50
        END
        STYLE
            OUTLINECOLOR "#1E90FF"
            WIDTH 1
            PATTERN 5 5 10 5 5 10 END
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   100000
    NAME           "ai_fi_thinning_need_shrub_method_cluster"
    GROUP           "ai_fi_thinning_need_shrub_method"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine - thinning need areas OF (Shrub method) - Cluster"
        "ows_abstract"        "AI Engine - thinning need areas OF (Shrub method) - Cluster"
        "ows_group_title"     "AI Engine - thinning need areas and clusters OF (Shrub method)"
        "ows_group_abstract"  "AI Engine - thinning need areas and clusters OF (Shrub method)"
        
        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (select 
            bag_id,             
            pointforcluster as geometry
        from 
            bag_ai_engine_output.thinning_needs_shrub_index_fi
        ) as foo using unique bag_id using srid=3067"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 250
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_ALGORITHM=SIMPLE"
    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id"

    CLASS
      NAME "Klusteroidut kohteet"
      EXPRESSION ("[Cluster_FeatureCount]" != "1")
      STYLE
        SIZE 50
        SYMBOL "muinaisjaannos_cluster"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END