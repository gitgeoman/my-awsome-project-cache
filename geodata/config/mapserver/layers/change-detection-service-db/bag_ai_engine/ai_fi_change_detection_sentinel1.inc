LAYER
    STATUS       OFF
    MAXSCALEDENOM 100000

    NAME        "ai_fi_change_detection_sentinel1"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – change detection areas (Sentinel 1)"
        "ows_abstract"        "AI Engine – change detection areas (Sentinel 1)"
        "ows_group_title"     "AI Engine - Change detection features and clusters (Sentinel 1)"
        "ows_group_abstract"  "AI Engine - Change detection features and clusters (Sentinel 1)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"
        
        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,
            fid,
            det_type, 
            det_value, 
            det_rel, 
            source,
            model_ver,
            date1,
            date2,
            epsg,
            xtile,
            ytile,
            jobid,
            jobdate,
            filename,
            'sentinel1_change_model' as output,
            geom as geometry 
        from 
            bag_ai_engine_output.sentinel_1_chg_fi
        )as foo using unique bag_id using srid=3067"
    
    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))

    CLASS
        NAME "Detected change"
        STYLE
            OPACITY 50
            COLOR "#D40B4D"
        END
        STYLE
            OUTLINECOLOR "#000000"
            WIDTH 1
            PATTERN 5 5 END
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   100000
    NAME           "ai_fi_change_detection_sentinel1_cluster"
    GROUP           "ai_fi_change_detection_sentinel1"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – change detection areas (Sentinel 1) - Cluster"
        "ows_abstract"        "AI Engine – change detection areas (Sentinel 1) - Cluster"
        "ows_group_title"     "AI Engine - Change detection features and clusters (Sentinel 1)"
        "ows_group_abstract"  "AI Engine - Change detection features and clusters (Sentinel 1)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (
        select 
            bag_id,
            pointforcluster as geometry 
        from 
            bag_ai_engine_output.sentinel_1_chg_fi
        )as foo using unique bag_id using srid=3067"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 250
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_ALGORITHM=SIMPLE"
    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id"

    CLASS
      NAME "Klusteroidut kohteet"
      EXPRESSION ("[Cluster_FeatureCount]" != "1")
      STYLE
        SIZE 50
        SYMBOL "muinaisjaannos_cluster"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END