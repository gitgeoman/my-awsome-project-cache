LAYER
    STATUS       OFF
    MAXSCALEDENOM 100000
    
    NAME        "ai_fi_thinning_need_ai_model_of"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine - thinning need areas OF (AI approach)"
        "ows_abstract"        "AI Engine - thinning need areas OF (AI approach)"
        "ows_group_title"     "AI Engine - thinning need areas OF (AI approach)"
        "ows_group_abstract"  "AI Engine - thinning need areas OF (AI approach)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,
            uuid,
            fid,
            standid,
            estateid,
            stand_area,
            det_type,
            det_value,
            det_rel,
            source,
            model_ver,
            model_ver_no,
            feedback_uuid,
            value,
            given_on,
            dt,
            epsg,
            xtile,
            ytile,
            jobid,
            filename,
            area_share,
            observationclassifier,
            feedbackclassifier,
            created_on,
            update_on,
            'thinning-need-openforest' as output,
            geom as geometry
        from 
            bag_ai_engine_output.thinning_need_of_fi_feed
        )as foo using unique bag_id using srid=3067"

    CLASS
        NAME "Thinning need"
        STYLE
            SYMBOL "hatch"
            COLOR "#00ccff"
            ANGLE 45
            SIZE 15.0
            WIDTH 2.0
            PATTERN 20 10 10 10 END
        END
        STYLE
            COLOR "#deb377"
            OPACITY 50
        END
        STYLE
            OUTLINECOLOR "#1E90FF"
            WIDTH 1
            PATTERN 5 5 10 5 5 10 END
        END
    END
END

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          "ai_fi_thinning_need_ai_model_of_date"
    GROUP         "ai_fi_thinning_need_ai_model_of"

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine - thinning need areas OF (AI approach) - Date"
        "ows_abstract"          "AI Engine - thinning need areas OF (AI approach) - Date"
        "ows_group_title"       "AI Engine - thinning need areas OF (AI approach)"
        "ows_group_abstract"    "AI Engine - thinning need areas OF (AI approach)"
        
        "wms_srs"               "EPSG:3067"
        "wms_enable_request"    "!GetCapabilities"
        "wfs_enable_request"    "!GetCapabilities"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            source_image_date,
            now()::date - source_image_date::date AS lifespan,
            geom as geometry
        FROM
            bag_ai_engine_datetime.thinning_need_of_fi_datetime
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "source_image_date"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Index"
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END