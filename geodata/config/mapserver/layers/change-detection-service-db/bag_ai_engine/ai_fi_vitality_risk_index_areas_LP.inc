LAYER
    STATUS       OFF
    MAXSCALEDENOM 100000

    NAME        "ai_fi_vitality_risk_index_areas_lp"

    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – vitality risk areas LP (index method)"
        "ows_abstract"        "AI Engine – vitality risk areas LP (index method)"
        "ows_group_title"     "AI Engine – vitality risk areas LP (index method)"
        "ows_group_abstract"  "AI Engine – vitality risk areas LP (index method)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        select 
            bag_id,
            fid,
            uuid,
            standid,
            estateid,
            parcelid,
            customeraccountmanageractorid,
            org_id,
            det_type, 
            det_value, 
            det_rel, 
            source,
            model_ver,
            model_ver_no,
            feedback_uuid,
            value,
            given_on,
            dt,
            epsg,
            xtile,
            ytile,
            jobid,
            jobdate,
            filename,
            observationClassifier,
            median_volume,
            created_on,
            update_on,
            'health-stress-wsmap-leafpoint-mask' as output,
            geom as geometry 
        from 
            bag_ai_engine_output.vitality_risk_index_areas_lp_fi_feed
        )as foo using unique bag_id using srid=3067"

    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))

    COMPOSITE
        OPACITY 50
    END
    
    CLASS
        NAME "Riskialue"
        STYLE
            COLOR "#cb181d"
            OUTLINECOLOR "#000000"
            WIDTH 0.5
        END
    END
END

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          "ai_fi_vitality_risk_index_areas_lp_date"
    GROUP         "ai_fi_vitality_risk_index_areas_lp"

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – vitality risk areas LP (index method) - Date"
        "ows_abstract"          "AI Engine – vitality risk areas LP (index method) - Date"
        "ows_group_title"       "AI Engine – vitality risk areas LP (index method)"
        "ows_group_abstract"    "AI Engine – vitality risk areas LP (index method)"
        
        "wms_srs"               "EPSG:3067"
        "wms_enable_request"    "!GetCapabilities"
        "wfs_enable_request"    "!GetCapabilities"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            source_image_date,
            now()::date - source_image_date::date AS lifespan,
            geom as geometry
        FROM
            bag_ai_engine_datetime.vitality_risk_index_areas_lp_fi_datetime
    ) as foo using unique bag_id using srid=3067"

    #LABELITEM "source_image_date"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "< 1 vk"
        EXPRESSION ([lifespan] <= 7)
        STYLE
            COLOR "#2200cc"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END

    CLASS
        NAME "1 vk - 2 vk"
        EXPRESSION (([lifespan] > 7) AND ([lifespan] <= 14))
        STYLE
            COLOR "#2b00ff"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END

    CLASS
        NAME "2 vk - 4 vk"
        EXPRESSION (([lifespan] > 14) AND ([lifespan] <= 28))
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END

    CLASS
        NAME "1 kk - 2 kk"
        EXPRESSION (([lifespan] > 28) AND ([lifespan] <= 60))
        STYLE
            COLOR "#6a4dff"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END

    CLASS
        NAME "2 kk - 3 kk"
        EXPRESSION (([lifespan] > 60) AND ([lifespan] <= 90))
        STYLE
            COLOR "#8066ff"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END

    CLASS
        NAME "> 3 kk"
        EXPRESSION ([lifespan] > 90)
        STYLE
            COLOR "#aa99ff"
            OUTLINECOLOR "#000000"
        END
        LABEL
            #TEXT "[source_image_date], [lifespan]"
            TEXT "[source_image_date]"
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END