LAYER
    STATUS       OFF
    MAXSCALEDENOM   50000

    NAME        "diag_cds_fi_chg_kemera_mki_unfiltered_25"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "CDS Diagnostics - CHG-KEMERA-MKI - 25 %"
        "ows_abstract"        "How well change detection areas intersect kemera and mki areas. This layer fits 25 %"
        "ows_group_title"     "CDS Diagnostics - CHG-KEMERA-MKI - 25 %"
        "ows_group_abstract"  "How well change detection areas intersect kemera and mki areas. This layer fits 25 %"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (select 
            bag_id, 
            ch_type2,
            applct_pct,
            complt_pct,
            declrt_pct,
            swamp_pct,
            barren_pct,
            date1,
            date2,
            created_on,
            geom as geometry 
        from 
            bag_diagnostics.chg_kemera_mki_unfiltered
        where
            ((swamp_pct > 0.7 AND barren_pct > 0.7 AND n_pix_in_c > 50) AND (applct_pct >= 0.75 AND complt_pct >= 0.75 AND declrt_pct >= 0.75) AND (created_on > now() - interval '1 MONTH'))
            ) as foo using unique bag_id using srid=3067"
    
    LABELITEM "ch_type2"
        

    CLASS
        NAME "Osuu max 25%"
        #EXPRESSION ([applct_pct] >= 0.75 AND [complt_pct] >= 0.75 AND [declrt_pct] >= 0.75)
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#ffa200"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 30000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
        
    END

    
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   50000
    NAME            "diag_cds_fi_chg_kemera_mki_unfiltered_25_cluster"
    GROUP           "diag_cds_fi_chg_kemera_mki_unfiltered_25"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "CDS Diagnostics - CHG-KEMERA-MKI - 25 % - Cluster"
        "ows_abstract"        "How well change detection areas intersect kemera and mki areas - Cluster"
        
        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (select 
            bag_id, 
            swamp_pct,
            barren_pct,
            applct_pct,
            declrt_pct,
            complt_pct,
            pointforcluster as geometry
        from 
            bag_diagnostics.chg_kemera_mki_unfiltered
        where
            ((swamp_pct > 0.7 AND barren_pct > 0.7 AND applct_pct >= 0.75 AND complt_pct >= 0.75 AND declrt_pct >= 0.75 AND n_pix_in_c > 50) AND (created_on > now() - interval '1 MONTH'))
            ) as foo using unique bag_id using srid=3067"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 200
      REGION "ellipse"
      BUFFER 50
    END

    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id"

  
    CLASS
      NAME "Klusteri - osuu 25%"
      #EXPRESSION ("[Cluster_FeatureCount]" != "1") 
      STYLE
        SIZE 50
        SYMBOL "change_detection_less_2m"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END
