LAYER
    STATUS       OFF

    NAME        "diag_cds_fi_timeseries_index_1yr"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "CDS Diagnostics - index count current year"
        "ows_abstract"        "Index count of all accepted tiles this year"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (select 
            bag_id, 
            origin, 
            scene, 
            product1da, 
            product2da,
            product3da,
            count,
            count_1yr,
            count_3yr,
            created_on,
            geom as geometry 
        from 
            bag_diagnostics.timeseries
        ) as foo using unique bag_id using srid=3067"
    
    LABELITEM "count_1yr"


    CLASS
        NAME "Index count current year: 0"
        EXPRESSION ([count_1yr] = 0)
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#ff0000"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 1500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
        
    END

    CLASS
        NAME "Index count current year: 1 - 2"
        EXPRESSION ([count_1yr] < 3 AND [count_1yr] > 0)
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#ffa200"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 1500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
        
    END

    CLASS
        NAME "Index count current year: 3 or over"
        EXPRESSION ([count_1yr] >= 3)
        STYLE
            OUTLINECOLOR "#009900"
            WIDTH 0.5
        END
        STYLE
            COLOR "#52BE80CC"
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 1500000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 25
            MAXLENGTH 3
            ALIGN CENTER
        END
        
    END

END