
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_fi_stem_count_index
    GROUP         ai_fi_stem_count

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – Stem count (Index)"
        "ows_abstract"          "AI Engine – Stem count (Index)"
        "ows_group_title"       "AI Engine – Stem count (trees/ha)"
        "ows_group_abstract"    "AI Engine – Stem count (trees/ha)"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021.tileindex_n_fi
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ai_fi_stem_count_raster
    GROUP         ai_fi_stem_count
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – Stem count (trees/ha)"
        "ows_abstract"        "AI Engine – Stem count (trees/ha)"
        "ows_group_title"     "AI Engine – Stem count (trees/ha)"
        "ows_group_abstract"  "AI Engine – Stem count (trees/ha)"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_fi_stem_count_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 100"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 100)
        STYLE
            COLORRANGE "#eefcf5" "#e0f9eb"
            DATARANGE 0 100
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "100-250"
        EXPRESSION ([pixel] > 100 AND [pixel] <= 250)
        STYLE
            COLORRANGE "#e0f9eb" "#cbf4db"
            DATARANGE 100 250
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "250-500"
        EXPRESSION ([pixel] > 250 AND [pixel] <= 500)
        STYLE
            COLORRANGE "#cbf4db" "#adebbc"
            DATARANGE 250 500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "500-1000"
        EXPRESSION ([pixel] > 500 AND [pixel] <= 1000)
        STYLE
            COLORRANGE "#adebbc" "#72ce7c"
            DATARANGE 500 1000
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "1000-2500"
        EXPRESSION ([pixel] > 1000 AND [pixel] <= 2500)
        STYLE
            COLORRANGE "#72ce7c" "#005334"
            DATARANGE 1000 2500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME ">2500"
        EXPRESSION ([pixel] > 2500 AND [pixel] <= 7000)
        STYLE
            COLOR "#005334"
        END
    END
END