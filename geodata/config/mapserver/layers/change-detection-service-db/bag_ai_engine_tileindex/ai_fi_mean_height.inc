
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_fi_mean_height_index
    GROUP         ai_fi_mean_height

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – Mean height (Index)"
        "ows_abstract"          "AI Engine – Mean height (Index)"
        "ows_group_title"       "AI Engine – Mean height (m)"
        "ows_group_abstract"    "AI Engine – Mean height (m)"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021.tileindex_h_fi
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ai_fi_mean_height_raster
    GROUP         ai_fi_mean_height
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – Mean height (m)"
        "ows_abstract"        "AI Engine – Mean height (m)"
        "ows_group_title"     "AI Engine – Mean height (m)"
        "ows_group_abstract"  "AI Engine – Mean height (m)"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_fi_mean_height_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 5"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 5)
        STYLE
            COLORRANGE "#fff5eb" "#fed8b2"
            DATARANGE 0 5
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5-10"
        EXPRESSION ([pixel] > 5 AND [pixel] <= 10)
        STYLE
            COLORRANGE "#fed8b2" "#fda45d"
            DATARANGE 5 10
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "10-15"
        EXPRESSION ([pixel] > 10 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#fda45d" "#e6590a"
            DATARANGE 10 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "15-20"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 20)
        STYLE
            COLORRANGE "#e6590a" "#7f2704"
            DATARANGE 15 20
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "20-25"
        EXPRESSION ([pixel] > 20 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#7f2704" "#661d02"
            DATARANGE 20 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 25"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#661d02" "#370f01"
            DATARANGE 25 50
            RANGEITEM "pixel"
        END
    END
END