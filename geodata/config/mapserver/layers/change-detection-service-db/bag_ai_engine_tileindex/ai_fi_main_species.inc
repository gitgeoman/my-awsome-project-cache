
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_fi_main_species_index
    GROUP         ai_fi_main_species

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – Main species (Index)"
        "ows_abstract"          "AI Engine – Main species (Index)"
        "ows_group_title"       "AI Engine – Main species"
        "ows_group_abstract"    "AI Engine – Main species"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021.tileindex_mainsp_fi
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ai_fi_main_species_raster
    GROUP         ai_fi_main_species
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – Main species"
        "ows_abstract"        "AI Engine – Main species"
        "ows_group_title"     "AI Engine – Main species"
        "ows_group_abstract"  "AI Engine – Main species"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_fi_main_species_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

	CLASS
        NAME "Mänty"
        EXPRESSION ([pixel] > 0.5 AND [pixel] < 1.5)
        STYLE
            COLOR "#ff7f00"
        END
    END
    CLASS
        NAME "Kuusi"
        EXPRESSION ([pixel] > 1.5 AND [pixel] < 2.5)
        STYLE
            COLOR "#4daf4a"
        END
    END
    CLASS
        NAME "Koivu"
        EXPRESSION ([pixel] > 2.5 AND [pixel] < 4.5)
        STYLE
            COLOR "#377eb8"
        END
    END
    CLASS
        NAME "Muu"
        EXPRESSION ([pixel] > 4.5) 
        STYLE
            COLOR "#984ea3"
        END
    END
END