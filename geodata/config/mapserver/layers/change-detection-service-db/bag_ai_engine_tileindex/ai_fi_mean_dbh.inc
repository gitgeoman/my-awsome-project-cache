
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_fi_mean_dbh_index
    GROUP         ai_fi_mean_dbh

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – Mean diameter (Index)"
        "ows_abstract"          "AI Engine – Mean diameter (Index)"
        "ows_group_title"       "AI Engine – Mean diameter (cm)"
        "ows_group_abstract"    "AI Engine – Mean diameter (cm)"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021.tileindex_dbh_fi
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ai_fi_mean_dbh_raster
    GROUP         ai_fi_mean_dbh
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – Mean diameter (cm)"
        "ows_abstract"        "AI Engine – Mean diameter (cm)"
        "ows_group_title"     "AI Engine – Mean diameter (cm)"
        "ows_group_abstract"  "AI Engine – Mean diameter (cm)"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_fi_mean_dbh_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 5"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 5)
        STYLE
            COLORRANGE "#edf8fb" "#b5cde2"
            DATARANGE 0 5
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5-10"
        EXPRESSION ([pixel] > 5 AND [pixel] <= 10)
        STYLE
            COLORRANGE "#b5cde2" "#8c95c2"
            DATARANGE 5 10
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "10-15"
        EXPRESSION ([pixel] > 10 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#8c95c2" "#8856a7"
            DATARANGE 10 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "15-20"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 20)
        STYLE
            COLORRANGE "#8856a7" "#810f7c"
            DATARANGE 15 20
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "20-25"
        EXPRESSION ([pixel] > 20 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#810f7c" "#5f0a5c"
            DATARANGE 20 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 25"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 80)
        STYLE
            COLORRANGE "#5f0a5c" "#240323"
            DATARANGE 25 80
            RANGEITEM "pixel"
        END
    END
END