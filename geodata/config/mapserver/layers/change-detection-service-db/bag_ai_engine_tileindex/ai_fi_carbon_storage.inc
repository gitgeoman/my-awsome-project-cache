
#-----------------------------------------------------
# Ai-engine forest attribute rasters
#-----------------------------------------------------

LAYER
    STATUS        OFF
    
    MINSCALEDENOM 40000

    NAME          ai_fi_carbon_storage_index
    GROUP         ai_fi_carbon_storage

    TYPE          POLYGON
    TEMPLATE      "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "AI Engine – Carbon storage (Index)"
        "ows_abstract"          "AI Engine – Carbon storage (Index)"
        "ows_group_title"       "AI Engine – Carbon storage (tn/ha)"
        "ows_group_abstract"    "AI Engine – Carbon storage (tn/ha)"
        "ows_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geometry from (
        SELECT
            bag_id,
            location,
            src_srs,
            geom as geometry,
            datevalue
        FROM
            bag_ai_engine_tileindex_2021.tileindex_carbon_fi
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "datevalue"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        STYLE
            COLOR "#5633FF"
            OUTLINECOLOR "#000000"
        END
        LABEL
            MINSCALEDENOM 40000
            MAXSCALEDENOM 170000
            COLOR 50 50 50
            SIZE 10
            POSITION cc
        END
    END
END

LAYER
    STATUS        OFF
    
    MAXSCALEDENOM 40000
    
    NAME          ai_fi_carbon_storage_raster
    GROUP         ai_fi_carbon_storage
    
    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – Carbon storage (tn/ha)"
        "ows_abstract"        "AI Engine – Carbon storage (tn/ha)"
        "ows_group_title"     "AI Engine – Carbon storage (tn/ha)"
        "ows_group_abstract"  "AI Engine – Carbon storage (tn/ha)"
        "ows_srs"             "EPSG:3067"
        "gml_include_items"   "x,y,value_0"
        #"gml_value_0_alias"   "korkeus_m"
    END

    TILEINDEX   "ai_fi_carbon_storage_index"
    TILEITEM    "location"
    TILESRS     "src_srs"

    PROCESSING  "RESAMPLE=NEAREST"
    PROCESSING  "NODATA=0"

    CLASS
        NAME "< 25"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#fcfdbf" "#fec68a"
            DATARANGE 0 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "25-50"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#fec68a" "#fc8e64"
            DATARANGE 25 50
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "50-75"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 75)
        STYLE
            COLORRANGE "#fc8e64" "#ec5860"
            DATARANGE 50 75
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "75-100"
        EXPRESSION ([pixel] > 75 AND [pixel] <= 100)
        STYLE
            COLORRANGE "#ec5860" "#ba3878"
            DATARANGE 75 100
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "100-125"
        EXPRESSION ([pixel] > 100 AND [pixel] <= 125)
        STYLE
            COLORRANGE "#ba3878" "#812581"
            DATARANGE 100 125
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "125-150"
        EXPRESSION ([pixel] > 125 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#812581" "#52137c"
            DATARANGE 125 150
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 150"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 592.2)
        STYLE
            COLORRANGE "#52137c" "#21114e"
            DATARANGE 150 592.2
            RANGEITEM "pixel"
        END
    END
END