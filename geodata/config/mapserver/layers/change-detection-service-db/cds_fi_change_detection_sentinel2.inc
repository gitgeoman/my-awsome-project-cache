LAYER
    STATUS          OFF
    MAXSCALEDENOM   50000

    NAME            "cds_fi_change_detection_sentinel2"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "CDS change detection areas (Sentinel 2)"
        "ows_abstract"        "CDS change detection areas (Sentinel 2)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "id,created_at,ba_before,ba_change,date1,date2,change_type,change_text,name"
        "gml_exclude_items"   "range,age"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        select
            id,
            created_at,
            round(ba_before,2) as ba_before,
            round(ba_after,2) as ba_after,
            round(ba_change,2) as ba_change,
            date1,
            date2,
            concat_ws(' / ', date1, date2) as range,
            now()::date - date2::date AS age,
            change_typ as change_type,
            case
                when change_typ = 1 then 'Harvennushakkuu'
                when change_typ = 2 then 'Uudistushakkuu'
                else 'Unknown'
            end as change_text,
            name,
            geom
        from
            change_detection_features
        order by
            date2 desc
    ) as foo using unique id using srid=3067"


    GEOMTRANSFORM (smoothsia([shape], 4, 1, 'angle'))
    
    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Harvennushakkuu < 2 kk"
        EXPRESSION ([change_type] = 1 AND [age] <= 60)
        STYLE
            SYMBOL  "hatch"
            COLOR   "#C70039"
            ANGLE   45
            SIZE    10.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[range]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 8
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "Harvennushakkuu > 2 kk"
        EXPRESSION ([change_type] = 1 AND [age] > 60)
        STYLE
            SYMBOL  "hatch"
            COLOR   "#ff8000"
            ANGLE   45
            SIZE    10.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[range]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 8
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "Uudistushakkuu < 2 kk"
        EXPRESSION ([change_type] = 2 AND [age] <= 60)
        STYLE
            COLOR "#C70039"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[range]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 8
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "Uudistushakkuu > 2 kk"
        EXPRESSION ([change_type] = 2 AND [age] > 60)
        STYLE
            COLOR "#ff8000"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            TEXT "[range]"
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT scb
            TYPE truetype
            SIZE 8
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   50000
    NAME            "cds_fi_change_detection_sentinel2_cluster"
    GROUP           "cds_fi_change_detection_sentinel2"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"           "CDS change detection areas (Sentinel 2) (Pisteklusteri)"
        "wms_abstract"        "CDS change detection areas (Sentinel 2) (Pisteklusteri)"
        "wms_group_title"     "CDS change detection areas (Sentinel 2) (Piste)"
        "wms_group_abstract"  "CDS change detection areas (Sentinel 2)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities !GetLegendGraphic"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"
    DATA "geom from (
        select
            id,
            now()::date - date2::date AS age,
            ST_Centroid(geom) as geom
        from
            change_detection_features
    ) as foo using unique id using srid=3067"

    LABELITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 250
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=id"

    CLASS
      NAME "Muutokset (alle 60 päivää vanhat)"
      EXPRESSION ("[Cluster_FeatureCount]" != "1" AND [age] <= 60)
      STYLE
        SIZE 50
        SYMBOL "change_detection_less_2m"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        PRIORITY 10
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
    CLASS
      NAME "Muutokset (yli 60 päivää vanhat)"
      EXPRESSION ("[Cluster_FeatureCount]" != "1" AND [age] > 60)
      STYLE
        SIZE 30
        SYMBOL "change_detection_more_2m"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        PRIORITY 10
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END