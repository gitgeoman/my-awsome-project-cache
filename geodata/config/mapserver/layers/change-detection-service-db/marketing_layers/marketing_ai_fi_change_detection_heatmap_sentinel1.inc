LAYER
    STATUS OFF
    MAXSCALEDENOM 1000000000
    
    NAME "marketing_ai_fi_change_detection_heatmap_sentinel1"
    TYPE raster

    CONNECTIONTYPE kerneldensity
    CONNECTION "marketing_ai_fi_change_detection_heatmap_sentinel1_point"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "AI Engine – change detection areas marketing heatmap (Sentinel 1)"
        "ows_abstract"        "AI Engine – change detection areas marketing heatmap (Sentinel 1)"
        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "gml_include_items"   "all"
    END
   
    PROCESSING "RANGE_COLORSPACE=HSL"
    PROCESSING "KERNELDENSITY_RADIUS=100"
    PROCESSING "KERNELDENSITY_COMPUTE_BORDERS=ON"
    PROCESSING "KERNELDENSITY_NORMALIZATION=AUTO"
    OFFSITE 0 0 0

    CLASS
      STYLE
        COLORRANGE  "#0000ff00"  "#0000ffff"
        DATARANGE 0 32
      END # STYLE
      STYLE
        COLORRANGE  "#0000ffff"  "#ff0000ff"
        DATARANGE 32 255
      END # STYLE
    END # CLASS
END # LAYER

LAYER
    STATUS          OFF
    MAXSCALEDENOM 100000
    NAME           "marketing_ai_fi_change_detection_heatmap_sentinel1_point"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "AI Engine – change detection areas marketing heatmap (Sentinel 1) - Pointlayer"
        "ows_abstract"        "AI Engine – change detection areas marketing heatmap (Sentinel 1) - Pointlayer"


        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"    
    DATA "geometry from (
        select 
            bag_id,
            pointforcluster as geometry 
        from 
            bag_ai_engine_output.sentinel_1_chg_fi
        )as foo using unique bag_id using srid=3067"
END
