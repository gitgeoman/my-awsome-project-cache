LAYER
    STATUS          OFF
    # MINSCALEDENOM   100000
                    
    NAME            "cds_fi_sentinel_false_color_tileindex"
    GROUP           "cds_fi_sentinel_false_color"

    METADATA
        "ows_title"             "CDS Sentinel False Color (index)"
        "ows_group_title"       "CDS Sentinel False Color"
        "ows_group_abstract"    "CDS Sentinel False Color"
        "ows_srs"               "EPSG:3067"
        "ows_enable_request"    "!GetCapabilities"
        "gml_include_items"     "date"
    END
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db-reader.inc"

    DATA "geometry from (
        select
            map_raster.id,
            to_char(map_raster.date, 'YYYY-MM-DD') as date,
            map_raster.location,
            map_raster.geometry
        from
            map_raster
        join (select
            max(id) as id,
            max(date) as date,
            x_origin,
            y_origin
            from
                map_raster
            where
                raster_type = 'FCI'
            group by
                x_origin,
                y_origin
            order by
                date desc
        ) as latest on
            map_raster.id = latest.id
    ) as foo using unique id using srid=3067"

    # LABELITEM "date"

    # COMPOSITE
    #   OPACITY 50
    # END

    # CLASS
    #    STYLE
    #        OUTLINECOLOR "#808080"
    #        COLOR "#DF8AB2"
    #        WIDTH 2
    #    END
    #    LABEL
    #        MINSCALEDENOM 5000
    #        MAXSCALEDENOM 200000
    #        FONT scb
    #        TYPE truetype
    #        SIZE 10
    #        COLOR "#746E70"
    #        OUTLINECOLOR "#FFFFFF"
    #        OUTLINEWIDTH 1
    #        ALIGN CENTER
    #    END
    # END

END
LAYER
    STATUS          OFF
    MAXSCALEDENOM   200000
    
    NAME            cds_fi_sentinel_false_color
    GROUP           cds_fi_sentinel_false_color
        
    TYPE            RASTER
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"             "CDS Sentinel False Color"
        "ows_group_title"       "CDS Sentinel False Color"
        "ows_group_abstract"    "CDS Sentinel False Color"
        "wms_srs"               "EPSG:3067"
        "gml_include_items"     "all"
    END

    TILEINDEX  "cds_fi_sentinel_false_color_tileindex"

    PROCESSING "RESAMPLE=NEAREST"
END