LAYER
    STATUS       OFF
    
    NAME         "cadastre_north_rhine_westphalia"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 10000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "North Rhine-Westphalia Cadastre"
        "ows_abstract"        "Cadastre of North Rhine-Westphalia"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"
    
    DATA "geometry from (

        SELECT

            cadastre.bag_id
            
            -- cadastre data
            ,cadastre.flurstueck as estate_id
            ,fluren.gemeinde as gemeinden
            ,fluren.uebname as gemarkung
            ,cadastre.flurnummer as flur
            ,cadastre.zaehler as flurstück
            ,round((ST_Area(cadastre.geom)/10000)::numeric, 2) as parcel_area  -- in hectares

            -- forest types data in rounded hectares
            ,round(parcel_forest.coniferous_forest, 2) as coniferous_forest
            ,round(parcel_forest.deciduous_forest, 2) as deciduous_forest
            ,round(parcel_forest.mixed_forest, 2) as mixed_forest

            -- protected areas data
            ,round((protected_areas.total_protected_area/10000), 2) as total_protected_area
            ,round(protected_areas.protected_cadastre_area_ratio, 1) as protected_cadastre_area_ratio 
            ,round((protected_areas.landscape_protection/10000), 2) as landscape_protection
            ,round((protected_areas.nature_protection/10000), 2) as nature_protection
            ,round((protected_areas.fauna_flora_habitat/10000), 2) as fauna_flora_habitat
            ,round((protected_areas.protected_biotopes/10000), 2) as protected_biotopes
            ,round((protected_areas.bird_protection/10000), 2) as bird_protection
            ,round((protected_areas.fish_habitat/10000), 2) as fish_habitat
            ,round((protected_areas.goose_habitat/10000), 2) as goose_habitat
            ,round((protected_areas.strict_nature_reserve/10000), 2) as strict_nature_reserve
            ,round((protected_areas.conservation_agreement/10000), 2) as conservation_agreement
            
            -- forestry offices data
            ,forestry_offices.name as name_forestry_offices
            ,forestry_offices.link as website_link_forestry_offices
            ,forestry_offices.address as address_forestry_offices
            ,translate(forestry_offices.phone, '/-', '') as phone_number_forestry_offices
            ,forestry_offices.mail as email_forestry_offices
            
            -- owner associations data
            ,associations.name_fwv as name_associations_fwv
            ,associations.link_fwv as website_link_associations_fwv
            ,associations.addr_fwv as address_associations_fwv
            ,associations.cont_fwv as contact_person_name_associations_fwv
            ,translate(associations.phone_fwv, '- ', '') as phone_number_associations_fwv
            ,associations.mail_fwv as email_associations_fwv
            ,associations.name_sales as name_associations_sales
            ,associations.link_sales as website_link_associations_sales
            ,associations.addr_sales as address_associations_sales
            ,associations.cont_sales as contact_person_name_associations_sales
            ,translate(associations.phon_sales, '- ', '') as phone_number_associations_sales
            ,associations.mail_sales as email_associations_sales

            -- geometry
            ,cadastre.geom as geometry
            
        FROM
            bag_cadastre.cadastre_north_rhine_westphalia AS cadastre
        JOIN
            bag_administrative_units.administrative_ger_fluren as fluren
        ON
            cadastre.fluren_code = fluren.schluessel
        JOIN
            bag_forest.parcel_forest as parcel_forest
        ON
            cadastre.bag_id = parcel_forest.cadastre_id
        JOIN
            bag_protected_areas.protected_areas_in_cadastre AS protected_areas
        ON
            cadastre.bag_id = protected_areas.cadastre_id
        JOIN
            bag_service_provider.service_provider_forestry_offices AS forestry_offices
        ON
            cadastre.service_provider_forestry_offices_id = forestry_offices.bag_id
        LEFT JOIN
            bag_service_provider.service_provider_owner_associations AS associations
        ON
            cadastre.service_provider_owner_associations_id = associations.bag_id

    ) as foo using unique bag_id using srid=3857"

    CLASS
        NAME "Cadastre"
        STYLE
            OUTLINECOLOR "#ff0000"
            WIDTH 1
        END
    END

END