LAYER
    STATUS          OFF

    MINSCALEDENOM 0
    MAXSCALEDENOM 200000

    NAME            "ch_osm_municip_borders"
    #GROUP          "ch_osm_ad_borders"#

    TYPE            POLYGON
    TEMPLATE        "foo"

    Extent 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "CH OSM municipality borders" #OSM lvl 8 & lvl 8 simplified#
        "ows_abstract"        "CH OSM municipality borders" #OSM lvl 8 & lvl 8 simplified#
        "ows_group_abstract"  "CH OSM Administrative borders"
        "ows_group_title"     "CH OSM Administrative borders"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    SCALETOKEN
        NAME "%table%"
        VALUES
            "0"         "ch_ad_borders_lvl_8"
            "60000"     "ch_ad_borders_lvl_8_simplified"
            "250000"    ""
        END
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.%table%
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 100
    END

    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "name"
    
    CLASS
        NAME "Gemeinde" #Municipality#
        STYLE
            OUTLINECOLOR "#f79845"
            WIDTH 2
        END #style
        LABEL
            MINSCALEDENOM 25000
            MAXSCALEDENOM 200000
            FONT scb
            TYPE truetype
            MINSIZE 13
            MAXSIZE 15
            COLOR "#3b3b3b"
            OUTLINECOLOR "#fde4c4"
            OUTLINEWIDTH 1.2
            PARTIALS TRUE
            POSITION cc
            ALIGN LEFT
        END #label
    END #class
END #layer