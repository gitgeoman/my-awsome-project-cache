LAYER
    STATUS          OFF
    
    MINSCALEDENOM 0
    MAXSCALEDENOM 800000

    NAME            "ch_osm_district_borders"
    #GROUP          "ch_osm_ad_borders"#

    TYPE            POLYGON
    TEMPLATE        "foo"

    Extent          570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "CH OSM district borders" #OSM lvl 6 & lvl 6 simplified#
        "ows_abstract"        "CH OSM district borders" #OSM lvl 6 & lvl 6 simplified#
        "ows_group_abstract"  "CH OSM Administrative borders"
        "ows_group_title"     "CH OSM Administrative borders"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    SCALETOKEN
        NAME "%table%"
        VALUES
            "0"         "ch_ad_borders_lvl_6"
            "60000"     "ch_ad_borders_lvl_6_simplified"
            "1000000"   ""
        END
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"
    
    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.%table%
    )as foo using unique bag_id using srid=3857"
    
    COMPOSITE
        OPACITY 100
    END

    LABELITEM "name"
    
    PROCESSING "LABEL_NO_CLIP=on"
    
    CLASS
        NAME "Landkreis/Bezirk" #District#
        STYLE
            OUTLINECOLOR "#ffa353"
            WIDTH 3.55
        END #style
        STYLE
            OUTLINECOLOR "#3b3b3b"
            WIDTH 2
        END #style
        LABEL
            MINSCALEDENOM 200000
            MAXSCALEDENOM 800000
            FONT scb
            TYPE truetype
            MINSIZE 13
            MAXSIZE 15
            COLOR "#3b3b3b"
            OUTLINECOLOR "#ffffff"
            OUTLINEWIDTH 1.2
            ALIGN CENTER
            PARTIALS TRUE
            POSITION cc
        END #label
    END #class
END #layer