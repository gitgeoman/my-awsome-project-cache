LAYER
    STATUS          OFF
    
    MINSCALEDENOM   2000
    MAXSCALEDENOM   192000000
    

    NAME            "fra_osm_country_borders"
    #GROUP          "fra_osm_ad_borders"#

    TYPE            POLYGON
    TEMPLATE        "foo"

    Extent  -725776.89 4301523.79 2423225.80 7419245

    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "FRA OSM country borders" #OSM lvl 3 simplified#
        "ows_abstract"        "FRA OSM country borders" #OSM lvl 3 simplified#
        "ows_group_abstract"  "FRA OSM Administrative borders"
        "ows_group_title"     "FRA OSM Administrative borders"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.fra_ad_borders_lvl_3_simplified
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 100
    END
    
    CLASS
        NAME "Land" #Country#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 5
        END #style
        STYLE
            OUTLINECOLOR "#3b3b3b"
            WIDTH 2
        END #style
    END #class
END #layer