LAYER
    STATUS          OFF
    
    MAXSCALEDENOM 5000000
    MINSCALEDENOM 2000

    NAME            "fra_osm_state_borders"
    #GROUP          "fra_osm_ad_borders"#
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    Extent -725776.89 4301523.79 2423225.80 7419245
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "FRA OSM state borders" #OSM lvl 4 simplified#
        "ows_abstract"        "FRA OSM state borders" #OSM lvl 4 simplified#
        "ows_group_abstract"  "FRA OSM Administrative borders"
        "ows_group_title"     "FRA OSM Administrative borders"
        
        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            osm_id,
            name,    
            geom as geometry 
        from 
            bag_eu_osm_data.fra_ad_borders_lvl_4_simplified
    )as foo using unique bag_id using srid=3857"

    COMPOSITE
        OPACITY 100
    END
    
    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "name"

    CLASS
        NAME "Bundesland" #Federal state#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 4
        END #style
        LABEL
            MINSCALEDENOM 800000
            MAXSCALEDENOM 5000000
            FONT scb
            TYPE truetype
            MINSIZE 13
            MAXSIZE 15
            COLOR "#ffffff"
            OUTLINECOLOR "#3b3b3b"
            OUTLINEWIDTH 1.2
            ALIGN CENTER
            PARTIALS TRUE
            POSITION cc
        END #label
    END #class
END #layer