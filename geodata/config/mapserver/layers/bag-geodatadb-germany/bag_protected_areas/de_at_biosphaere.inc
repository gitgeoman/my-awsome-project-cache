LAYER
    STATUS       OFF

    NAME         "de_at_biosphaere"
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 400000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Biosphärenreservate Germany&Austria"
        "ows_abstract"        "Biosphärenreservate DE/AT"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "fid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        fid,
        geom as geometry
    from
        bag_protected_areas.de_at_biosphaere
    ) as foo using unique fid using srid=3857"

    COMPOSITE
        OPACITY 30
    END
    
    CLASS
        NAME "Biosphärenreservat" #Biosphere reserve#
        STYLE
            COLOR "#0046d0"
        END
    END
END