LAYER
    STATUS       OFF

    NAME         "de_naturpark"
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Naturparke"
        "ows_abstract"        "Naturpark Deutschland"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        id, 
        geom as geometry,
        name,
        flaeche
    from
        bag_protected_areas.de_naturpark
    ) as foo using unique id using srid=3857"

    COMPOSITE
        OPACITY 30
    END
    
    CLASS
        NAME "Naturparke"
        STYLE
            COLOR "#f7f739"
        END
    END
END