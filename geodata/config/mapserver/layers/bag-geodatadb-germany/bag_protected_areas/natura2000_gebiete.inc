LAYER
    STATUS       OFF

    MAXSCALEDENOM 400000

    NAME         "natura2000_gebiete"
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Natura2000"
        "ows_abstract"        "Natura2000"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "fid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        id, 
        sitecode,
        sitename,
        geom as geometry
    from
        bag_protected_areas.natura2000_gebiete
    where ms = 'DE' or ms = 'AT'
    ) as foo using unique id using srid=3857"

    COMPOSITE
        OPACITY 30
    END
    
    CLASS
        NAME "Natura2000-Gebiet" #Natura2000 areas#
        STYLE
            COLOR "#ff8901"
        END
    END
END