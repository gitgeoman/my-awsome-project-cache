LAYER
    STATUS       OFF

    NAME         "ger_vertragsflachen"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Vertragsflächen"
        "ows_abstract"        "Vertragsflächen of Nordrhein Westfalen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        bag_id
        ,kennung
        ,geom as geometry
    from
        bag_protected_areas.ger_vertragsflachen
    ) as foo using unique bag_id using srid=3857"

    CLASS
        NAME "Vertragsflächen"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#339966"
            ANGLE   45
            SIZE    5.0
            WIDTH   2
        END
        STYLE
            OUTLINECOLOR "#339966"
            WIDTH 1.0
        END
    END

END
