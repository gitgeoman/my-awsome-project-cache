LAYER
    STATUS       OFF

    NAME         "ger_gesetzlich_geschutzte_biotope_flache"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Gesetzlich geschützte Biotope_Fläche"
        "ows_abstract"        "Gesetzlich geschützte Biotope_Fläche of Nordrhein Westfalen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        bag_id
        ,kennung
        ,geom as geometry
    from
        bag_protected_areas.ger_gesetzlich_geschutzte_biotope_flache
    ) as foo using unique bag_id using srid=3857"

    CLASS
        NAME "Gesetzlich geschützte Biotope_Fläche"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#CC3399"
            ANGLE   45
            SIZE    12.0
            WIDTH   0.5
        END
        STYLE
            OUTLINECOLOR "#CC3399"
            WIDTH 1.0
        END
    END

END
