LAYER
    STATUS       OFF

    NAME         "ger_gesetzlich_geschutzte_biotope_punkt"

    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Gesetzlich geschützte Biotope_Punkt"
        "ows_abstract"        "Gesetzlich geschützte Biotope_Punkt of Nordrhein Westfalen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        bag_id
        ,kennung
        ,geom as geometry
    from
        bag_protected_areas.ger_gesetzlich_geschutzte_biotope_punkt
    ) as foo using unique bag_id using srid=3857"

    CLASS
        NAME "Gesetzlich geschützte Biotope_Punkt"
        STYLE
            SYMBOL "circle"
            COLOR "#CC3399"
            SIZE 4
        END
    END

END
