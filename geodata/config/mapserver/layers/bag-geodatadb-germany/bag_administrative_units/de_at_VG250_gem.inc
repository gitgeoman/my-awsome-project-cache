LAYER
    STATUS       OFF

    MINSCALEDENOM 0
    MAXSCALEDENOM 200000

    NAME         "de_at_VG250_gem"
    GROUP        "administrative_de_at"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Gemeinde DE/AT"
        "ows_group_title"     "Administrative Grenzen"
        "ows_abstract"        "Gemeinde DE/AT"
        "ows_group_abstract"  "Administrative Grenzen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "fid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        fid,
        geom as geometry,
        municip
    from
        bag_administrative_units.de_at_VG250_gem
    ) as foo using unique fid using srid=3857"

    COMPOSITE
        OPACITY 100
    END

    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "municip"
    
    CLASS
        NAME "Gemeinde" #Municipality#
        STYLE
            OUTLINECOLOR "#f79845"
            WIDTH 2
        END #style
        LABEL
            MINSCALEDENOM 25000
            MAXSCALEDENOM 200000
            FONT scb
            TYPE truetype
            MINSIZE 13
            MAXSIZE 15
            COLOR "#3b3b3b"
            OUTLINECOLOR "#fde4c4"
            OUTLINEWIDTH 1.2
            PARTIALS TRUE
            POSITION cc
            ALIGN LEFT
        END #label
    END #class
END #layer