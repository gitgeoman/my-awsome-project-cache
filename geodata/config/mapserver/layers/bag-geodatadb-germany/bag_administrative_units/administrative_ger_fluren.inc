LAYER
    STATUS       OFF
    
    NAME         "administrative_ger_fluren"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    MAXSCALEDENOM 50000

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Fluren"
        "ows_abstract"        "Fluren of Nordrhein Westfalen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        
        "gml_include_items"   "all"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"
    
    DATA "geometry from (select
        bag_id as id,
        bag_id,
        name,
        schluessel,
        gemeinde,
        geom as geometry
    from
        bag_administrative_units.administrative_ger_fluren
    ) as foo using unique id using srid=3857"

    CLASS
        NAME "Fluren"
        STYLE
            OUTLINECOLOR "#eb32e6"
            WIDTH 1
        END
    END

END