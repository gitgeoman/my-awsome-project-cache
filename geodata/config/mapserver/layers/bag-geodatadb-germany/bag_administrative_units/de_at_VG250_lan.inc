LAYER
    STATUS       OFF
    
    MAXSCALEDENOM 5000000
    MINSCALEDENOM 2000

    NAME         "de_at_VG250_lan"
    GROUP        "administrative_de_at"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Bundesland DE/AT"
        "ows_group_title"     "Administrative Grenzen"
        "ows_abstract"        "Bundesland DE/AT"
        "ows_group_abstract"  "Administrative Grenzen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "fid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        fid,
        geom as geometry,
        state
    from
        bag_administrative_units.de_at_VG250_lan
    ) as foo using unique fid using srid=3857"

    COMPOSITE
        OPACITY 100
    END
    
    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "state"

    CLASS
        NAME "Bundesland" #Federal state#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 4
        END #style
        LABEL
            MINSCALEDENOM 800000
            MAXSCALEDENOM 5000000
            FONT scb
            TYPE truetype
            MINSIZE 13
            MAXSIZE 15
            COLOR "#ffffff"
            OUTLINECOLOR "#3b3b3b"
            OUTLINEWIDTH 1.2
            ALIGN CENTER
            PARTIALS TRUE
            POSITION cc
        END #label
    END #class
END #layer