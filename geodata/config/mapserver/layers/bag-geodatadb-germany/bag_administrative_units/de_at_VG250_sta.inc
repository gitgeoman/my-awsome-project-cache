LAYER
    STATUS       OFF

    MAXSCALEDENOM   192000000
    MINSCALEDENOM   2000

    NAME         "de_at_VG250_sta"
    GROUP        "administrative_de_at"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END

    METADATA
        "ows_title"           "Staat DE/AT"
        "ows_group_title"     "Administrative Grenzen"
        "ows_abstract"        "Staat DE/AT"
        "ows_group_abstract"  "Administrative Grenzen"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "fid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-germany-reader.inc"

    DATA "geometry from (select
        fid,
        geom as geometry,
        country
    from
        bag_administrative_units.de_at_VG250_sta
    ) as foo using unique fid using srid=3857"

    COMPOSITE
        OPACITY 100
    END
    
    CLASS
        NAME "Land" #Country#
        STYLE
            OUTLINECOLOR "#b05500"
            WIDTH 5
        END #style
        STYLE
            OUTLINECOLOR "#3b3b3b"
            WIDTH 2
        END #style
    END #class
END #layer