LAYER
    STATUS        OFF

    NAME          bag_hillshade_germany

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Hillshading Germany"
        "ows_abstract"        "Hillshading Germany"
        "ows_srs"             "EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/hillshading/bag_hillshade_germany.tif"
    PROCESSING  "RESAMPLE=NEAREST"

END
