LAYER
    STATUS        OFF

    NAME          olpe_mean_height

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Baumhöhe"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_mean_height.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "< 50"
        EXPRESSION ([pixel] >= 0 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#ffffd4" "#ffe19c"
            DATARANGE 0 50
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "50"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#ffe19c" "#feb351"
            DATARANGE 50 150
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "150"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 250)
        STYLE
            COLORRANGE "#feb351" "#f0821e"
            DATARANGE 150 250
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "250"
        EXPRESSION ([pixel] > 250 AND [pixel] <= 350)
        STYLE
            COLORRANGE "#f0821e" "#cc560c"
            DATARANGE 250 350
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "350"
        EXPRESSION ([pixel] > 350 AND [pixel] <= 445)
        STYLE
            COLORRANGE "#cc560c" "#993404"
            DATARANGE 350 445
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 350"
        EXPRESSION ([pixel] > 445 AND [pixel] <= 450)
        STYLE
            COLOR "#993404"
        END
    END
END
