LAYER
    STATUS        OFF

    NAME          olpe_age

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Baumalter"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_age.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "< 25"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#ffffcc" "#c2e699"
            DATARANGE 0 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "25"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#c2e699" "#78c679"
            DATARANGE 25 50
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "50"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 75)
        STYLE
            COLORRANGE "#78c679" "#23783c"
            DATARANGE 50 75
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "75"
        EXPRESSION ([pixel] > 75 AND [pixel] <= 99)
        STYLE
            COLORRANGE "#23783c" "#05371e"
            DATARANGE 75 99
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 75"
        EXPRESSION ([pixel] > 99 AND [pixel] <= 100)
        STYLE
            COLOR "#05371e"
        END
    END
END
