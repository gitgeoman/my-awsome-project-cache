LAYER
    STATUS        OFF

    NAME          olpe_stem_vol_spruce

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Vorrat, Fichte"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_stem_volume_spruce.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "0"
        EXPRESSION ([pixel] <= 10)
        STYLE
            COLORRANGE "#fde725" "#9eda39"
            DATARANGE 0 10
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "10"
        EXPRESSION ([pixel] > 10 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#9eda39" "#4ac26d"
            DATARANGE 10 50
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "50"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 150)
        STYLE
            COLORRANGE "#4ac26d" "#1fa288"
            DATARANGE 50 150
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "150"
        EXPRESSION ([pixel] > 150 AND [pixel] <= 300)
        STYLE
            COLORRANGE "#1fa288" "#277f8e"
            DATARANGE 150 300
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "300"
        EXPRESSION ([pixel] > 300 AND [pixel] <= 500)
        STYLE
            COLORRANGE "#277f8e" "#365c8d"
            DATARANGE 300 500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "500"
        EXPRESSION ([pixel] > 500 AND [pixel] <= 750)
        STYLE
            COLORRANGE "#365c8d" "#45327f"
            DATARANGE 500 750
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "750"
        EXPRESSION ([pixel] > 750 AND [pixel] <= 999)
        STYLE
            COLORRANGE "#45327f" "#440154"
            DATARANGE 750 999
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 750"
        EXPRESSION ([pixel] > 999 AND [pixel] <= 1000)
        STYLE
            COLOR "#440154"
        END
    END
END
