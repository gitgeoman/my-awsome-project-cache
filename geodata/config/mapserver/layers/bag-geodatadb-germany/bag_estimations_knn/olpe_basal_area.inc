LAYER
    STATUS        OFF

    NAME          olpe_basal_area

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Grundfläche"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_basal_area.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "0"
        EXPRESSION ([pixel] >= 0 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#d7796b" "#fdae61"
            DATARANGE 0 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "15"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 25)
        STYLE
            COLORRANGE "#fdae61" "#ffffc0"
            DATARANGE 15 25
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "25"
        EXPRESSION ([pixel] > 25 AND [pixel] <= 40)
        STYLE
            COLORRANGE "#ffffc0" "#a6d96a"
            DATARANGE 25 40
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "40"
        EXPRESSION ([pixel] > 40 AND [pixel] <= 54)
        STYLE
            COLORRANGE "#a6d96a" "#1a9641"
            DATARANGE 40 54
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 55"
        EXPRESSION ([pixel] > 54)
        STYLE
            COLOR "#1a9641"
        END
    END
END
