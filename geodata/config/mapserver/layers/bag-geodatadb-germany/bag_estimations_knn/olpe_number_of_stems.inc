LAYER
    STATUS        OFF

    NAME          olpe_number_of_stems

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Stammzahl"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_number_of_stems.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "< 350"
        EXPRESSION ([pixel] > 0 AND [pixel] <= 350)
        STYLE
            COLOR "#fff288"
        END
    END
    CLASS
        NAME "2000"
        EXPRESSION ([pixel] > 350 AND [pixel] <= 2000)
        STYLE
            COLORRANGE "#fff288" "#98d000"
            DATARANGE 350 2000
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "3500"
        EXPRESSION ([pixel] > 2000 AND [pixel] <= 3500)
        STYLE
            COLORRANGE "#98d000" "#32be00"
            DATARANGE 2000 3500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "5000"
        EXPRESSION ([pixel] > 3500 AND [pixel] <= 5000)
        STYLE
            COLORRANGE "#32be00" "#19716b"
            DATARANGE 3500 5000
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "6500"
        EXPRESSION ([pixel] > 5000 AND [pixel] <= 6500)
        STYLE
            COLORRANGE "#19716b" "#0025d6"
            DATARANGE 5000 6500
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 6500"
        EXPRESSION ([pixel] > 6500)
        STYLE
            COLOR "#0025d6"
        END
    END
END
