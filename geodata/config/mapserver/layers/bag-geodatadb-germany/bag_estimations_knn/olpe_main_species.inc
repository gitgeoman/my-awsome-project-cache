LAYER
    STATUS        OFF

    NAME          olpe_main_species

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Hauptbaumart"
        "ows_abstract"        "Main tree species for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_main_species.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "nicht differenziert"
        EXPRESSION ([pixel] = 8)
        STYLE
            COLOR "#eaeac6"
        END
    END
    CLASS
        NAME "Buche"
        EXPRESSION ([pixel] = 1)
        STYLE
            COLOR "#18b129"
        END
    END
    CLASS
        NAME "andere Laubbäume (ALN)"
        EXPRESSION ([pixel] = 2)
        STYLE
            COLOR "#fdae61"
        END
    END
    CLASS
        NAME "Douglasie"
        EXPRESSION ([pixel] = 3)
        STYLE
            COLOR "#0b9586"
        END
    END
    CLASS
        NAME "Lärche"
        EXPRESSION ([pixel] = 4)
        STYLE
            COLOR "#96da0a"
        END
    END
    CLASS
        NAME "andere Laubbäume (ALH)"
        EXPRESSION ([pixel] = 5)
        STYLE
            COLOR "#923c3a"
        END
    END
    CLASS
        NAME "Eiche"
        EXPRESSION ([pixel] = 6)
        STYLE
            COLOR "#fb7537"
        END
    END
    CLASS
        NAME "Fichte"
        EXPRESSION ([pixel] = 7)
        STYLE
            COLOR "#2e6738"
        END
    END
END
