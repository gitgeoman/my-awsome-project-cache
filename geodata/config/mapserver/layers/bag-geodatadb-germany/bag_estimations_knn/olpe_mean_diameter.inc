LAYER
    STATUS        OFF

    NAME          olpe_mean_diameter

    TYPE          RASTER
    TEMPLATE      "foo"

    EXTENT 570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END


    METADATA
        "ows_title"           "Baumdurchmesser"
        "ows_abstract"        "Estimation for Olpe"
        "ows_srs"             "EPSG:25832 EPSG:3857"
        "gml_include_items"   "all"
        "gml_include_items"   "x,y,value_0"
        "gml_value_0_alias"   "volume"
    END

    DATA   "/vsis3/bag-test-geodata/public/germany/sentinel/olpe_mean_diameter.tif"

    PROCESSING  "RESAMPLE=NEAREST"

    CLASS
        NAME "< 7"
        EXPRESSION ([pixel] >= 0 AND [pixel] <= 7)
        STYLE
            COLORRANGE "#ffffd4" "#ffe19c"
            DATARANGE 0 7
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "7"
        EXPRESSION ([pixel] > 7 AND [pixel] <= 15)
        STYLE
            COLORRANGE "#ffe19c" "#feb351"
            DATARANGE 7 15
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "15"
        EXPRESSION ([pixel] > 15 AND [pixel] <= 35)
        STYLE
            COLORRANGE "#feb351" "#f0821e"
            DATARANGE 15 35
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "35"
        EXPRESSION ([pixel] > 35 AND [pixel] <= 50)
        STYLE
            COLORRANGE "#f0821e" "#cc560c"
            DATARANGE 35 50
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "50"
        EXPRESSION ([pixel] > 50 AND [pixel] <= 115)
        STYLE
            COLORRANGE "#cc560c" "#993404"
            DATARANGE 50 115
            RANGEITEM "pixel"
        END
    END
    CLASS
        NAME "> 50"
        EXPRESSION ([pixel] > 115 AND [pixel] <= 120)
        STYLE
            COLOR "#993404"
        END
    END
END
