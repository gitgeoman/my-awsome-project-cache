LAYER
    STATUS       OFF

    NAME         "syke_catchment_drainagebasin"
    
    TYPE         POLYGON
    TEMPLATE     "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "SYKE Valuma-alue"
        "ows_abstract"        "Suomen ympäristökeskuksen valuma-alueet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE         "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (select * from bag_syke.syke_catchment_drainagebasin) as foo using unique bag_id using srid=3067"

    LABELITEM "jako3tunnu"

    CLASS
        NAME "Valuma-alue"
        STYLE
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
        LABEL
            MAXSCALEDENOM 125000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END

END