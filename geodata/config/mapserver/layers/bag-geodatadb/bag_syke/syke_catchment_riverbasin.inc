LAYER
    STATUS       OFF

    NAME         "syke_catchment_riverbasin"
    TYPE         POLYGON
    TEMPLATE     "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "SYKE Valuma-alue (Päävesistöalueet)"
        "ows_abstract"        "Suomen ympäristökeskuksen valuma-alueet (Päävesistöalueet)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (select * from bag_syke.syke_catchment_riverbasin) as foo using unique bag_id using srid=3067"

    CLASS
        NAME "Pääjako"
        STYLE
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END

END