LAYER
    STATUS       OFF

    NAME         "syke_salmonrivers"

    MAXSCALEDENOM   100000

    TYPE         LINE
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE lohikalakannat"
        "ows_abstract"        "Suomen ympäristökeskuksen lohikalakannat"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select id, lohikala, uomanro, paajakonro, elyylyh, geom as geometry from bag_syke_arcgis.syke_salmonrivers) as foo using unique id using srid=3067"

    
    CLASS
        NAME "Lohijoet"
        EXPRESSION ([lohikala] = 1) 
        STYLE
            COLOR "#1E90FF"
            WIDTH 2
        END
    END
    CLASS
        NAME "Taimenjoet"
        EXPRESSION ([lohikala] = 0) 
        STYLE
            COLOR "#00BFFF"
            WIDTH 2
        END
    END
    CLASS
        NAME "Muut"
        EXPRESSION ([lohikala] = 3) 
        STYLE
            COLOR 255 0 0
            WIDTH 1
        END
    END
END