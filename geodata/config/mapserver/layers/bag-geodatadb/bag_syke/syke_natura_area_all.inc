LAYER
    STATUS       OFF

    NAME         "syke_natura_area_all"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA

        "ows_title"           "SYKE Natura-alue (Kaikki)"
        "ows_abstract"        "Suomen ympäristökeskuksen Natura-alueet (Kaikki)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"

    END

    PROCESSING "LABEL_NO_CLIP=ON"
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            bag_id,
            nimisuomi,
            nimiruotsi,
            naturatunn,
            suojeluper,
            versiotunn,
            aluetyyppi,
            paatosasia,
            geom as geometry
        from
            bag_syke.syke_natura_area_all
    ) as foo using unique bag_id using srid=3067"

    LABELITEM       "nimisuomi"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Natura-alue (Kaikki)"
        STYLE
            COLOR "#73FFE6CC"
            OUTLINECOLOR "#821EDC"
            WIDTH 1
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END

END