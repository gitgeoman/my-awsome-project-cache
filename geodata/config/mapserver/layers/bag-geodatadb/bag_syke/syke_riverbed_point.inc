LAYER
    STATUS          OFF
    MAXSCALEDENOM   100000

    NAME         "syke_riverbed_point"

    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Uomaverkosto (purkupiste)"
        "ows_abstract"        "Suomen ympäristökeskuksen uomaverkosto (purkupiste)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_syke.syke_riverbed_point) as foo using unique bag_id using srid=3067"

    CLASS
        NAME "Purkupiste"
        STYLE
            SYMBOL "square"
            COLOR "#cc0000"
            OUTLINECOLOR "#000000"
            SIZE 5
            WIDTH 1
        END
    END

END