LAYER
    STATUS          OFF
    MAXSCALEDENOM   100000

    NAME         "syke_catchment_point"

    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Valuma-alue (Purkupiste)"
        "ows_abstract"        "Suomen ympäristökeskuksen valuma-alueet (Purkupiste)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, purkutunnu, geom as geometry from bag_syke.syke_catchment_point) as foo using unique bag_id using srid=3067"

    LABELITEM       "purkutunnu"

    CLASS
        NAME "Valuma-alueet (Purkupiste)"
        STYLE
            SYMBOL "square"
            COLOR "#dee7e8"
            OUTLINECOLOR "#1f6c7a"
            SIZE 8
            WIDTH 2
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#4c4b4b"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION cr
        END
    END

END