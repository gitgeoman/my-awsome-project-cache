LAYER
    STATUS       OFF

    NAME         "syke_river_region"

    MAXSCALEDENOM   500000

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Ranta10 Joet (Alue)"
        "ows_abstract"        "Suomen ympäristökeskuksen Ranta10 Joet"
        "ows_group_title"     "Suomen ympäristökeskuksen Ranta10 Joet"
        "ows_group_abstract"  "Suomen ympäristökeskuksen Ranta10 Joet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"
        "WMS_LAYER_GROUP"     "/syke_river_group/syke_river_region/"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, jokinro, valtio, muutospvm, bag_source_ts, bag_update_ts, geom as geometry from bag_syke.syke_river_region) as foo using unique bag_id using srid=3067"
    
    CLASSITEM "bag_id"

    CLASS
        NAME "Joki alue"
        STYLE
            OUTLINECOLOR "#010eff"
            WIDTH 2
        END
        STYLE
            COLOR "#357cff"
            OPACITY 50
        END
    END
END