LAYER
    STATUS       OFF

    NAME         "syke_local_general_plan"
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Yleiskaava-alue"
        "ows_abstract"        "Suomen ympäristökeskuksen yleiskaava-alueet (aineisto kattaa ennen vuotta 2000 kunnanvaltuustojen hyväksymät yleiskaavat siltä osin, kuin niiden alueet on alistettu vahvistettaviksi ja ne ovat tulleet voimaan. Valitusprosessien keston takia aineiston viimeinen yleiskaava vahvistui huhtikuun lopussa 2006)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_syke.syke_local_general_plan) as foo using unique bag_id using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Yleiskaava-alue"
        STYLE
            COLOR "#FFFFFF"
            OUTLINECOLOR "#0000FF"
            WIDTH 1
        END
    END

END