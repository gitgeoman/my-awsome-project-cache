LAYER
    STATUS       OFF

    NAME         "syke_groundwater_area"
    TYPE         POLYGON
    TEMPLATE     "foo"
    
    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "SYKE Pohjavesialue"
        "ows_abstract"        "Suomen ympäristökeskuksen pohjavesialueet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (SELECT
        bag_id,
        po_pohjave,
        pvaluetunn,
        muodaluetu,
        pvaluenimi,
        pvalueluok,
        case
            when pvalueluok = 'Vedenhankintaa varten tärkeä  pohjavesialue (1)' then '( 1 )'
            when pvalueluok = 'Vedenhankintaa varten tärkeä pohjavesialue (I)' then '( I )'
            when pvalueluok = 'Vedenhankintaa varten tärkeä pohjavesialue, jonka pohjavedestä pintavesi- tai maaekosysteemi on suoraan riippuvainen (1E)' then '( 1E )'
            when pvalueluok = 'Muu vedenhankintakäyttöön soveltuva pohjavesialue (2)' then '( 2 )'
            when pvalueluok = 'Vedenhankintaan soveltuva pohjavesialue (II)' then '( II )'
            when pvalueluok = 'Muu vedenhankintakäyttöön soveltuva pohjavesialue, jonka pohjavedestä pintavesi- tai  maaekosysteemi on suoraan riippuvainen (2E)' then '( 2E )'
            when pvalueluok = 'Muu pohjavesialue (III)' then '( III )'
            when pvalueluok = 'Pohjavesialue jonka pohjavedestä pintavesi- tai maaekosysteemi on suoraan riippuvainen (E)' then '( E )'
        end as pvaluelabel,
        yht_yha,
        pvtunnuslo,
        suojsuunn,
        vhatunnus,
        digpohja,
        digorg,
        muutospvm,
        subtype,
        antoisuusa,
        kuntanimi,
        palapvalue,
        palamuodal,
        tilamaara,
        tilakemia,
        riskiarvio,
        shape_star,
        shape_stle,
        bag_sourcefile,
        bag_source_ts,
        bag_update_ts,
        geom
    FROM
        bag_syke.syke_groundwater_area) as foo using unique bag_id using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASSITEM 'pvalueluok'
    LABELITEM 'pvaluelabel'

    #1-luokka:
    #Vedenhankintaa varten tärkeä  pohjavesialue (1)
    #Vedenhankintaa varten tärkeä pohjavesialue (I)
    #Vedenhankintaa varten tärkeä pohjavesialue, jonka pohjavedestä pintavesi- tai maaekosysteemi on suoraan riippuvainen (1E)

    #2-luokka:
    #Vedenhankintaan soveltuva pohjavesialue (II)
    #Muu vedenhankintakäyttöön soveltuva pohjavesialue (2)
    #Muu vedenhankintakäyttöön soveltuva pohjavesialue, jonka pohjavedestä pintavesi- tai  maaekosysteemi on suoraan riippuvainen (2E)

    #Muu-luokka:
    #Muu pohjavesialue (III)
    #Pohjavesialue jonka pohjavedestä pintavesi- tai maaekosysteemi on suoraan riippuvainen (E)
    #Luokituksesta poistettu pohjavesialue

    CLASS
        EXPRESSION /^Vedenhankintaa varten.*$/
        NAME "Pohjavesialue 1-luokka"
        STYLE
            COLOR "#4A7CB9CC"
            OUTLINECOLOR "#4A7CB9"
            WIDTH 1
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

    CLASS
        EXPRESSION /soveltuva/
        NAME "Pohjavesialue 2-luokka"
        STYLE
            COLOR "#31A3B7CC"
            OUTLINECOLOR "#31A3B7"
            WIDTH 1
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

    CLASS
        EXPRESSION /^Muu pohjavesialue.*$|^Pohjavesialue jonka.*$|Luokituksesta poistettu pohjavesialue/
        NAME "Pohjavesialue Muu-luokka"
        STYLE
            COLOR "#7873B7CC"
            OUTLINECOLOR "#7873B7"
            WIDTH 1
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END