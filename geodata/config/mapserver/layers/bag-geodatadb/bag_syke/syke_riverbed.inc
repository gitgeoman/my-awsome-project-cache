LAYER
    STATUS       OFF

    NAME         "syke_riverbed"

    TYPE         LINE
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Uomaverkosto"
        "ows_abstract"        "Suomen ympäristökeskuksen uomaverkosto"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, uomaluokka, geom as geometry from bag_syke.syke_riverbed) as foo using unique bag_id using srid=3067"

    CLASSITEM "uomaluokka"

    CLASS
        NAME "Joki tai jokipseudo"
        EXPRESSION "joki tai jokipseudo"
        STYLE
            COLOR "#1E90FF"
            WIDTH 2
        END
    END
    CLASS
        NAME "Järvipseudo"
        EXPRESSION "järvipseudo"
        STYLE
            COLOR "#00BFFF"
            WIDTH 1
        END
    END

END