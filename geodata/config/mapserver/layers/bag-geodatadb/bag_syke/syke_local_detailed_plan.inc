LAYER
    STATUS       OFF

    NAME         "syke_local_detailed_plan"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Asemakaava-alue"
        "ows_abstract"        "Suomen ympäristökeskuksen asemakaava-alueet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    PROCESSING "LABEL_NO_CLIP=ON"
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_syke.syke_local_detailed_plan) as foo using unique bag_id using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Asemakaava-alue"
        STYLE
            COLOR "#999999"
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END

END