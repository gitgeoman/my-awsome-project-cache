LAYER
    STATUS       OFF

    MAXSCALEDENOM 400000

    NAME         "syke_wind_and_shoreline_deposits"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Arvokkaat tuuli- ja rantakerrostumat"
        "ows_abstract"        "Suomen ympäristökeskuksen arvokkaat tuuli- ja rantakerrostumat"
        "ows_group_title"     "SYKE Arvokkaat tuuli- ja rantakerrostumat"
        "ows_group_abstract"  "Suomen ympäristökeskuksen arvokkaat tuuli- ja rantakerrostumat"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, muodtunnus, nimi, luokka, muodtyyppi, arvoluokka, digorg, linkki, muutospvm, bag_source_ts, bag_update_ts, geom as geometry from bag_syke.syke_wind_and_shoreline_deposits) as foo using unique bag_id using srid=3067"

    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 1)
        STYLE
            COLOR "#ff0101"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 1)
        STYLE
            COLOR "#ff0101"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 1)
        STYLE
            COLOR "#ff0101"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 2)
        STYLE
            COLOR "#ffaf01"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 2)
        STYLE
            COLOR "#ffaf01"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 2)
        STYLE
            COLOR "#ffaf01"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 3)
        STYLE
            COLOR "#13c200"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 3)
        STYLE
            COLOR "#13c200"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 3)
        STYLE
            COLOR "#13c200"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 4)
        STYLE
            COLOR "#00329e"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 4)
        STYLE
            COLOR "#00329e"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
    CLASS
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 4)
        STYLE
            COLOR "#00329e"
            OPACITY 30
            OUTLINECOLOR "#000000"
        END
    END
END

LAYER
    STATUS          OFF
    NAME            "syke_wind_and_shoreline_deposits_symbol"
    GROUP           "syke_wind_and_shoreline_deposits"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "SYKE Arvokkaat tuuli- ja rantakerrostumat"
        "ows_abstract"        "Suomen ympäristökeskuksen arvokkaat tuuli- ja rantakerrostumat"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, muodtyyppi, arvoluokka, ST_Centroid(geom) as geometry from bag_syke.syke_wind_and_shoreline_deposits) as foo using unique bag_id using srid=3067"

    CLASS
        NAME "1 lk tuulikerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 1)
        STYLE
            SYMBOL "circle"
            SIZE 10
            COLOR "#ff0101"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "1 lk tuuli- ja rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 1)
        STYLE
            SYMBOL "square"
            SIZE 10
            COLOR "#ff0101"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "1 lk rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 1)
        STYLE
            SYMBOL "triangle_filled"
            SIZE 10
            COLOR "#ff0101"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "2 lk tuulikerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 2)
        STYLE
            SYMBOL "circle"
            SIZE 10
            COLOR "#ffaf01"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "2 lk tuuli- ja rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 2)
        STYLE
            SYMBOL "square"
            SIZE 10
            COLOR "#ffaf01"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "2 lk rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 2)
        STYLE
            SYMBOL "triangle_filled"
            SIZE 10
            COLOR "#ffaf01"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "3 lk tuulikerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 3)
        STYLE
            SYMBOL "circle"
            SIZE 10
            COLOR "#13c200"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "3 lk tuuli- ja rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 3)
        STYLE
            SYMBOL "square"
            SIZE 10
            COLOR "#13c200"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "3 lk rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 3)
        STYLE
            SYMBOL "triangle_filled"
            SIZE 10
            COLOR "#13c200"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "4 lk tuulikerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuulikerrostuma" AND [arvoluokka] = 4)
        STYLE
            SYMBOL "circle"
            SIZE 10
            COLOR "#00329e"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "4 lk tuuli- ja rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Tuuli- ja rantakerrostuma" AND [arvoluokka] = 4)
        STYLE
            SYMBOL "square"
            SIZE 10
            COLOR "#00329e"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
    CLASS
        NAME "4 lk rantakerrostuma"
        EXPRESSION ("[muodtyyppi]" = "Rantakerrostuma" AND [arvoluokka] = 4)
        STYLE
            SYMBOL "triangle_filled"
            SIZE 10
            COLOR "#00329e"
            OUTLINECOLOR "#000000"
            OPACITY 50
        END
    END
END