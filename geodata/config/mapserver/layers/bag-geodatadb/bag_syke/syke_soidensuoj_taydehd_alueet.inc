LAYER
    STATUS       OFF

    NAME         "syke_soidensuoj_taydehd_alueet"
    GROUP        "syke_soidensuoj_taydehd_all"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA

        "ows_title"           "SYKE soidensuojelu täydennysehdotukset (alueet)"
        "ows_group_title"     "SYKE soidensuojelu täydennysehdotukset (Kaikki)"
        "ows_abstract"        "Soidensuojelun täydennysehdotuskohteet: Soidensuojelun täydennysehdotuksen kohderajaukset."
        "ows_group_abstract"  "Soidensuojelun täydennysehdotuskohteet (Kaikki)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"

    END

    PROCESSING "LABEL_NO_CLIP=ON"
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, kohdenro, kohdenimi, geom as geometry from bag_syke.syke_soiden_suoj_tayd_ehd_alueet) as foo using unique bag_id using srid=3067"

    LABELITEM       "kohdenimi"

    COMPOSITE
        OPACITY 80
    END

    CLASS
        NAME "Soidensuojelun täydennysehdotuskohde (alueet)"
        STYLE
            #COLOR "#009900"
            OUTLINECOLOR "#ff0000"
            WIDTH 3.0
        END
         STYLE
            SYMBOL "hatch"
            COLOR "#ff0000"
            ANGLE 45
            SIZE 15.0
            WIDTH 1.0
        END                  
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END

END