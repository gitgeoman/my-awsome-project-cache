LAYER
    STATUS          OFF
    MAXSCALEDENOM   50000

    NAME            "bitcomp_fsc_buffer_parcel"

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Bitcomp FSC Vesistöbufferi"
        "ows_abstract"        "Bitcomp FSC Vesistöbufferi"


        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, tpteksti, geom as geometry from bag_analysis.fsc_buffer_parcel) as foo using unique bag_id using srid=3067"
    
    COMPOSITE
        OPACITY 75
    END
    CLASS
        NAME "Vesistöbufferi"
        STYLE
            COLOR "#3366ffcc"
            OUTLINECOLOR "#9900cc"
            WIDTH 1
        END
    END
END