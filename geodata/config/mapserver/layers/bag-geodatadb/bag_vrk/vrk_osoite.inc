LAYER
    STATUS        OFF
    MAXSCALEDENOM 10000

    NAME         "vrk_osoite"
    GROUP        "mml_vrk_tiesto_osoitteilla"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "VRK Osoite"
        "ows_abstract"        "Väestörekisterikeskuksen osoitteet"
        "ows_group_title"     "MML Tiestö ja VRK Osoitteet"
        "ows_group_abstract"  "MML Tiestö ja VRK Osoitteet tasoryhmä"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, katunumero, geom as geometry from bag_vrk.vrk_osoite) as foo using unique bag_id using srid=3067"

    LABELITEM "katunumero"
    SIZEUNITS meters

    CLASS
        NAME "Osoitepiste"
        STYLE
            SYMBOL "circle"
            COLOR "#9400D3"
            MINSIZE 5
            MAXSIZE 10
        END
        LABEL
            MAXSCALEDENOM 5000
            MINSIZE 8
            MAXSIZE 10
            FONT scb
            TYPE TRUETYPE
            COLOR "#746E70"
            OUTLINECOLOR 255 255 255
            OUTLINEWIDTH 1
            WRAP ' '
            MAXLENGTH 16
            ALIGN RIGHT
            PARTIALS TRUE
            POSITION ur
            BUFFER 0.5
            OFFSET 1 1
        END
    END

END