LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_developmentclass"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Kehitysluokka)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Kehitysluokka)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Kehitysluokka)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Kehitysluokka)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            developmentclass,
            developmentclasstext,
            geometry
        from bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 75
    END

    CLASSITEM "developmentclass"

    CLASS
        NAME "Aukea"
        EXPRESSION {A0}
        STYLE
            COLOR "#969696"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Taimikko, alle 1,3m"
        EXPRESSION {T1}
        STYLE
            COLOR "#addd8e"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
         NAME "Taimikko, yli 1,3m"
        EXPRESSION {T2}
        STYLE
            COLOR "#41ab5d"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Ylispuustoinen taimikko"
        EXPRESSION {Y1}
        STYLE
            COLOR "#006837"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Nuori kasvatusmetsikkö"
        EXPRESSION {02}
        STYLE
            COLOR "#fed976"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Varttunut kasvatusmetsikkö"
        EXPRESSION {03}
        STYLE
            COLOR "#fd8d3c"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Uudistuskypsä metsikkö"
        EXPRESSION {04}
        STYLE
            COLOR "#e31a1c"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Suojuspuumetsikkö"
        EXPRESSION {05}
        STYLE
            COLOR "#7fcdbb"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Siemenpuumetsikkö"
        EXPRESSION {S0}
        STYLE
            COLOR "#1d91c0"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Eri-ikäisrakenteinen metsikkö"
        EXPRESSION {ER}
        STYLE
            COLOR "#253494"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_developmentclass_shading"
    GROUP            "avoinkuviodata_theme_developmentclass"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Kehitysluokka-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Kehitysluokka-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Kehitysluokka)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Kehitysluokka)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wms_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            developmentclass,
            geometry
        from bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END