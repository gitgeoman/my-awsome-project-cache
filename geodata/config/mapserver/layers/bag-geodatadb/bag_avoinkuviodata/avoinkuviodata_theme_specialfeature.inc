LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000
    
    NAME            "avoinkuviodata_theme_specialfeature"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (ETE)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Metsälain tärkeät elinympäristöt)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            especialfeature,
	        especialadditionalfeaturetext,
            specialfeatures,
            specialfeaturesadditional,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
        where
            especialfeature = 43
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Metsälain tärkeä elinympäristökuvio"
        STYLE
            COLOR "#FF00FF"
            OUTLINECOLOR "#d500ff"
            WIDTH 2
        END
    END

END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_specialfeature_shading"
    GROUP            "avoinkuviodata_theme_specialfeature"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (ETE-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (ETE-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (ETE)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (ETE)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wms_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
        where
            especialfeature = 43
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 75
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END