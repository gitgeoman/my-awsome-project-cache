LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_maintreespecies"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Pääpuulaji)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Pääpuulaji)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Pääpuulaji)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Pääpuulaji)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            maintreespecies,
            maintreespeciestext,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
    END

    CLASSITEM "maintreespecies"

    CLASS
        NAME "Mänty"
        EXPRESSION ([maintreespecies] = 1)
        STYLE
            COLOR "#ff7f00"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Kuusi"
        EXPRESSION ([maintreespecies] = 2)
        STYLE
            COLOR "#4daf4a"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Koivu"
        EXPRESSION ([maintreespecies] = 3 or [maintreespecies] = 4)
        STYLE
            COLOR "#377eb8"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Muu"
        EXPRESSION ([maintreespecies] > 5)
        STYLE
            COLOR "#984ea3"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_maintreespecies_shading"
    GROUP            "avoinkuviodata_theme_maintreespecies"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Pääpuulaji-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Pääpuulaji-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Pääpuulaji)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Pääpuulaji)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wms_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            maintreespecies,
            maintreespeciestext,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END