LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_quality_pine"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Laatumännikkö)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Laatumännikkö)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Laatumännikkö)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Laatumännikkö)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            maintreespecies,
            maintreespeciestext,
            developmentclass,
            developmentclasstext,
            fertilityclass,
            fertilityclasstext,
            meanheight,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
        where maintreespecies = '1' AND (developmentclass = '04' OR developmentclass = '03') AND fertilityclass > '3' 
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 60
    END

    CLASSITEM "meanheight"

    CLASS
        NAME "16-19m"
        EXPRESSION ([meanheight] >= 16 AND [meanheight] <= 19)
        STYLE
            COLOR "#e7fcdc"
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END
    CLASS
        NAME "19-22m"
        EXPRESSION ([meanheight] > 19 AND [meanheight] <= 22)
        STYLE
            COLOR "#a1d99b"
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END
    CLASS
        NAME "22-25m"
        EXPRESSION ([meanheight] > 22 AND [meanheight] <= 25)
        STYLE
            COLOR "#238b45"
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END
    CLASS
        NAME "Yli 25m"
        EXPRESSION ([meanheight] > 25 )
        STYLE
            COLOR "#002a0f"
            OUTLINECOLOR "#000000"
            WIDTH 1
        END
    END  
END