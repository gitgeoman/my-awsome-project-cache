LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME            "avoinkuviodata_theme_distance_road"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Avoin kuviodata (Etäisyys tiehen)"
        "ows_abstract"        "Avoin kuviodata (Etäisyys tiehen)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"

    DATA "geometry from (
        select
            rp.standid,
            rp.min,
            rp.max,
            rp.mean,
            rp.median,
            rp.majority,
            rp.minority,
            rp.unique,
            rp.range,
            geometry
        from
            bag_avoinkuviodata_json.standinfo as si
        join
            bag_avoinkuviodata_json.road_proximity as rp
        on
            rp.standid = si.standid
    ) as foo using unique standid using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASSITEM "mean"

    CLASS
        NAME "0 - 100 m"
        EXPRESSION ([mean] <= 100)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#b2df8a"
            WIDTH 2
        END
    END
    CLASS
        NAME "100 - 200 m"
        EXPRESSION ([mean] > 100 and [mean] <= 200)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#febb80"
            WIDTH 2
        END
    END
    CLASS
        NAME "200 - 300 m"
        EXPRESSION ([mean] > 200 and [mean] <= 300)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#f8765c"
            WIDTH 2
        END
    END
    CLASS
        NAME "300 - 400 m"
        EXPRESSION ([mean] > 300 and [mean] <= 400)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#d3426e"
            WIDTH 2
        END
    END
    CLASS
        NAME "400 - 500 m"
        EXPRESSION ([mean] > 400 and [mean] <= 500)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#982c80"
            WIDTH 2
        END
    END
    CLASS
        NAME "> 500 m"
        EXPRESSION ([mean] > 500)
        STYLE
            OUTLINECOLOR "#808080"
            COLOR "#5e177f"
            WIDTH 2
        END
    END

END