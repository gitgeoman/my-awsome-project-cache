LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_cutting_proposal"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            fellingyear,
            fellingtype,
            fellingtext,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
        where
            fellingyear = date_part('year', CURRENT_DATE) or
            fellingyear = date_part('year', CURRENT_DATE) + 1
    ) as foo using unique standid using srid=3067"
    

    CLASSITEM "fellingtype"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Ylispuiden poisto"
        EXPRESSION ([fellingtype] = 1)
        STYLE
            COLOR "#a6cee3"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Ensiharvennus"
        EXPRESSION ([fellingtype] = 2)
        STYLE
            COLOR "#b2df8a"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Harvennushakkuu"
        EXPRESSION ([fellingtype] = 3)
        STYLE
            COLOR "#33a02c"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Kaistalehakkuu"
        EXPRESSION ([fellingtype] = 4)
        STYLE
            COLOR "#fb9a99"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Avohakkuu"
        EXPRESSION ([fellingtype] = 5)
        STYLE
            COLOR "#e31a1c"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Verhopuuhakkuu"
        EXPRESSION ([fellingtype] = 6)
        STYLE
            COLOR "#fdbf6f"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Suojuspuuhakkuu"
        EXPRESSION ([fellingtype] = 7)
        STYLE
            COLOR "#ff7f00"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Siemenpuuhakkuu"
        EXPRESSION ([fellingtype] = 8)
        STYLE
            COLOR "#cab2d6"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "Muu"
        STYLE
            COLOR "#cccccc"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
END

LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_cutting_proposal_shading"
    GROUP            "avoinkuviodata_theme_cutting_proposal"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Hakkuuehdotus-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Hakkuuehdotus-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Hakkuuehdotus)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wms_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
        where
            fellingyear = date_part('year', CURRENT_DATE) or
            fellingyear = date_part('year', CURRENT_DATE) + 1
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END