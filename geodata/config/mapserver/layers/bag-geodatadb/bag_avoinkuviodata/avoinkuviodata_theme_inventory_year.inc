LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_inventory_year"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Inventointivuosi)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Inventointivuosi)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Inventointivuosi)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Inventointivuosi)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            inventoryyear,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"

    COMPOSITE
        OPACITY 75
    END

    CLASSITEM "inventoryyear"

    CLASS
        NAME ">=2017"
        EXPRESSION ([inventoryyear] >= 2017)
        STYLE
            COLOR "#4575b4"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "2016"
        EXPRESSION ([inventoryyear] = 2016)
        STYLE
            COLOR "#91bfdb"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "2015"
        EXPRESSION ([inventoryyear] = 2015)
        STYLE
            COLOR "#e0f3f8"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "2014"
        EXPRESSION ([inventoryyear] = 2014)
        STYLE
            COLOR "#ffffbf"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "2013"
        EXPRESSION ([inventoryyear] = 2013)
        STYLE
            COLOR "#fee08b"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "2012-2008"
        EXPRESSION ([inventoryyear] >= 2008 and [inventoryyear] <= 2012)
        STYLE
            COLOR "#fc8d59"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "<2008"
        EXPRESSION ([inventoryyear] < 2008)
        STYLE
            COLOR "#d73027"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_inventory_year_shading"
    GROUP            "avoinkuviodata_theme_inventory_year"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Inventointivuosi-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Inventointivuosi-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Inventointivuosi)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Inventointivuosi)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            inventoryyear,
            geometry
        from
            bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    COMPOSITE
        OPACITY 50
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END