LAYER
    STATUS          OFF

    MAXSCALEDENOM   15000
    
    NAME            "avoinkuviodata_standnumber"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Kuvionumero)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Kuvionumero)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Kuviotiedot)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Kuviotiedot)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"
        "WMS_LAYER_GROUP"     "/avoinkuviodata_standgroup/avoinkuviodata_standnumber/"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            standnumber,
            standnumberextension,
            realestateid,
            realestatetext,
            area,
            areadecrease,
            maingroup,
            maingrouptext,
            subgroup,
            subgrouptext,
            fertilityclass,
            fertilityclasstext,
            soiltype,
            soiltypetext,
            drainagestate,
            drainagestatetext,
            ditchingyear,
            thinningyear, 
            developmentclass,
            developmentclasstext,
            standquality,
            standqualitytext,
            maintreespecies,
            maintreespeciestext,
            accessibility,
            accessibilitytext,
            standinfo,
            datasource,
            datasourcetext,
            growthplacedatasource,
            growthplacedatasourcetext,
            treestanddate,
            treestandtype,
            treestandtypetext,
            inventorydate,
            inventoryyear,
            meanage,
            basalarea, 
            stemcount,
            meandiameter,
            meanheight,
            volume,
            sawlogvolume,
            pulpwoodvolume,
            volumegrowth,
            fellingtype,
            fellingtext,
            fellingyear,
            fellingsrctext,
            silviculturaltype,
            silviculturaltext,
            silviculturalyear,
            silviculturalsrc,
            especialfeature,
            epecialfeaturetext,
            especialadditionalfeaturetext,
            specialfeatures,
            specialfeaturesadditional,
            restrictions,
            restrictionends,
            geometry
        from bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"

    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "standnumber"
        
    CLASS
        NAME "Numero"
        LABEL
            FONT scb
            TYPE truetype
            MINSIZE 9
            MAXSIZE 11
            COLOR "#3b3b3b"
            OUTLINECOLOR "#FFFFFF"
            #OUTLINECOLOR "#fde4c4"
            OUTLINEWIDTH 1.0
            PARTIALS TRUE
            POSITION cc
            ALIGN LEFT
        END
    END
END