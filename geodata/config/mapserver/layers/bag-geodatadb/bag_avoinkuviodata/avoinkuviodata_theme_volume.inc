LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_volume"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Tilavuus)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Tilavuus)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Tilavuus)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Tilavuus)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "standid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            volume,
            geometry
        from bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 75
    END

    CLASSITEM "volume"

    CLASS
        NAME "0-50"
        EXPRESSION ([volume] >= 0 and [volume] < 50)
        STYLE
            COLOR "#ECF8FA"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "50-100"
        EXPRESSION ([volume] >= 50 and [volume] < 100)
        STYLE
            COLOR "#CDECE5"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "100-150"
        EXPRESSION ([volume] >= 100 and [volume] < 150)
        STYLE
            COLOR "#9BD8C9"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "150-200"
        EXPRESSION ([volume] >= 150 and [volume] < 200)
        STYLE
            COLOR "#6AC1A4"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "200-250"
        EXPRESSION ([volume] >= 200 and [volume] < 250)
        STYLE
            COLOR "#47AD78"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME "250-300"
        EXPRESSION ([volume] >= 250 and [volume] < 300)
        STYLE
            COLOR "#2C8B49"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
    CLASS
        NAME ">= 300"
        EXPRESSION ([volume] >= 300)
        STYLE
            COLOR "#095829"
            OUTLINECOLOR "#006400"
            WIDTH 1
        END
    END
END
LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME             "avoinkuviodata_theme_volume_shading"
    GROUP            "avoinkuviodata_theme_volume"
    TYPE             POLYGON
    TEMPLATE         "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Avoin kuviodata (Tilavuus-aputaso)"
        "ows_abstract"        "Metsäkeskus Avoin kuviodata (Tilavuus-aputaso)"
        "ows_group_title"     "Metsäkeskus Avoin kuviodata (Tilavuus)"
        "ows_group_abstract"  "Metsäkeskus Avoin kuviodata (Tilavuus)"
        "ows_srs"             "EPSG:3067"
        
        # hide layer from GetCapabilities and GetFeatureInfo
        "wms_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wfs_enable_request"  "!GetCapabilities !GetFeatureInfo"
        "wms_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            standid,
            volume,
            geometry
        from bag_avoinkuviodata_json.standinfo
    ) as foo using unique standid using srid=3067"
    
    COMPOSITE
        OPACITY 50
        COMPFILTER "blur(10)"
    END
    CLASS
        STYLE
            GEOMTRANSFORM (buffer([shape],-6))
            COLOR 255 255 255
        END
    END
END