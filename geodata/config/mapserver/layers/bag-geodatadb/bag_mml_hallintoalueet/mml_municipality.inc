LAYER
    STATUS       OFF
    
    NAME         "mml_municipality"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Kuntajako (2022)"
        "ows_abstract"        "MML Kuntajako (2022)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select gid, natcode, namefin, geom as geometry from bag_mml_hallintoalueet.mml_municipality_latest) as foo using unique gid using srid=3067"

    CLASS
        NAME "Kunta"
        STYLE
            OUTLINECOLOR "#8A2BE2"
            WIDTH 1.0
        END
    END

END