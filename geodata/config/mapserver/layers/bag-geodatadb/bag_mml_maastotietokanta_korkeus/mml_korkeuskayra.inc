LAYER
    STATUS          ON

    MAXSCALEDENOM   25000

    NAME            "mml_korkeuskayra"

    TYPE            LINE
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Korkeuskäyrä"
        "ows_abstract"        "MML Maastotietokannan korkeuskäyrät"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select 
            bag_id, (korkeusarvo / 1000) as korkeusarvo,
            geom as geometry
        from
            bag_mml_maastotietokanta_korkeus.korkeuskayra
    ) as foo using unique bag_id using srid=3067"
    
    LABELITEM "korkeusarvo"
    SIZEUNITS meters
    
    CLASS
        NAME "Korkeuskäyrä"
        STYLE
            OUTLINECOLOR "#AB5700"
            WIDTH 1.5
        END
        LABEL
            TYPE TRUETYPE
            PARTIALS FALSE
            FONT scb
            SIZE 5
            MAXOVERLAPANGLE 22.5
            MINDISTANCE 1000
            REPEATDISTANCE 2000
            ANGLE FOLLOW
            COLOR "#AB5700"
            OUTLINECOLOR 255 255 255
            OUTLINEWIDTH 1
            MINFEATURESIZE AUTO
            BUFFER 3
        END
    END
END