LAYER
    STATUS       OFF

    NAME        "metsakeskus_metsamaski"
   
    TYPE        POLYGON
    TEMPLATE    "foo"

    MAXSCALEDENOM 50000

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Metsämaski"
        "ows_abstract"        "Metsäkeskus Metsämaski"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select 
            bag_id, 
            objectid, 
            area, 
            updatetime, 
            karttalehti, 
            geometry as geometry 
        from 
            bag_metsakeskus.metsakeskus_metsamaski_schedule
        )as foo using unique bag_id using srid=3067"

    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "Metsämaski"
        STYLE
            COLOR "#52BE80CC"
            OUTLINECOLOR "#009900"
            WIDTH 1
        END
    END

END