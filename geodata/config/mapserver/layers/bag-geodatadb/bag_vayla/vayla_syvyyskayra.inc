LAYER
    STATUS          ON

    MAXSCALEDENOM   25000

    NAME            "vayla_syvyyskayra"

    TYPE            LINE
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Väylä Syvyyskäyrä"
        "ows_abstract"        "Väylä Syvyyskäyrä"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select id, valdco as syvyysarvo, geom as geometry from bag_vayla.syvyyskayra) as foo using unique id using srid=3067"
    
    LABELITEM "syvyysarvo"

    CLASS
        NAME "Syvyyskäyräs"
        STYLE
            OUTLINECOLOR "#1E90FF"
            WIDTH 2
        END
        LABEL
            TYPE TRUETYPE
            PARTIALS FALSE
            FONT scb
            SIZE 12
            MAXOVERLAPANGLE 22.5
            MINDISTANCE 200
            COLOR "#1E90FF"
            OUTLINECOLOR 255 255 255
            OUTLINEWIDTH 1
            MINFEATURESIZE AUTO
            BUFFER 3
        END
    END
END