LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    NAME            "mml_parcel_polygon"
    GROUP           "mml_parcel"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Kiinteistöt (Palsta)"
        "ows_group_title"     "MML Kiinteistörajat"
        "ows_abstract"        "MML Kiinteistöraja-aineisto (Palsta)"
        "ows_group_abstract"  "MML Kiinteistöraja-aineisto"


        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select gid, tunnus, tpteksti, area, geom as geometry from bag_mml_kiinteistorekisterikartta.parcel) as foo using unique gid using srid=3067"
    
    CLASS
        NAME "Palsta"
        STYLE
            OUTLINECOLOR "#FF0000"
            WIDTH 1
        END
    END
END