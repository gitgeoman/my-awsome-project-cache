LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    LABELCACHE      ON

    NAME            "mml_parcel_boundary_point"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Kiinteistöt (Rajamerkki)"
        "ows_abstract"        "MML Kiinteistöraja-aineisto (Rajamerkki)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select gid, numero, geom as geometry from bag_mml_kiinteistorekisterikartta.parcel_boundary_point) as foo using unique gid using srid=3067"

    LABELITEM "numero"

    CLASS
        NAME "Rajamerkki"
        MAXSCALEDENOM 5000
        STYLE
            SYMBOL "circle"
            SIZE 6
            OUTLINECOLOR "#808080"
        END
        LABEL
            MAXSCALEDENOM 5000
            MINSCALEDENOM 1501
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            POSITION ur
            ALIGN left
        END
        LABEL
            MAXSCALEDENOM 1500
            FONT scb
            TYPE TRUETYPE
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            POSITION ur
            ALIGN left
        END 
    END
END