LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    LABELCACHE      ON

    NAME            "mml_parcel_reference_point"
    GROUP           "mml_parcel"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Kiinteistöt (Tunnuspiste)"
        "ows_group_title"     "MML Kiinteistörajat"
        "ows_abstract"        "MML Kiinteistöraja-aineisto (Tunnuspiste)"
        "ows_group_abstract"  "MML Kiinteistöraja-aineisto"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            gid,
            tunnus,
            tpteksti,
            geom as geometry 
        from
            bag_mml_kiinteistorekisterikartta.parcel_reference_point
    ) as foo using unique gid using srid=3067"

    LABELITEM "tpteksti"

    CLASS
        NAME "Tunnuspiste"
        MAXSCALEDENOM 5000
        STYLE
            SYMBOL "kiinteistotunnus"
            SIZE 5
        END
        LABEL
            MAXSCALEDENOM 5000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#423e3f"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            POSITION ur
            ALIGN left
        END
    END

END