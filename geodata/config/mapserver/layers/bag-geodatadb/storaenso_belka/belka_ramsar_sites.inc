LAYER
    STATUS       OFF

    NAME         "belka_ramsar_sites"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "Ramsar Sites"
        "ows_abstract"        "Ramsar Sites"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_ramsar_sites
    ) as foo using unique bag_id using srid=4326"

    COMPOSITE
        OPACITY 25
    END

    CLASS
        NAME "Ramsar Sites"
        STYLE
            COLOR "#00cc00"
            OUTLINECOLOR "#008000"
            WIDTH 1.0
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#008000"
            ANGLE   45
            SIZE    15.0
            WIDTH   2.0
        END
    END

END