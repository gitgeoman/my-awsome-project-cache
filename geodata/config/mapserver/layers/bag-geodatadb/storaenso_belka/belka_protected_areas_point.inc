LAYER
    STATUS       OFF

    NAME         "belka_existing_protected_areas_point"
    GROUP        "belka_existing_protected_areas_group"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "Existing Protected areas (point)"
        "ows_group_title"     "Existing Protected areas"
        "ows_abstract"        "Existing Protected areas (point)"
        "ows_group_abstract"  "Existing Protected areas"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_protected_areas_point
        where
            existing is true
    ) as foo using unique bag_id using srid=4326"

    LABELITEM "title"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Existing Protected area"
        STYLE
            SYMBOL  "circle"
            COLOR   "#0000FF"
            SIZE    15.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END

LAYER
    STATUS       OFF

    NAME         "belka_planned_protected_areas_point"
    GROUP        "belka_planned_protected_areas_group"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "Planned Protected areas"
        "ows_group_title"     "Planned Protected areas"
        "ows_abstract"        "Planned Protected areas"
        "ows_group_abstract"  "Planned Protected areas"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
           *
        from
            storaenso_belka.belka_protected_areas_point
        where
            planned is true
    ) as foo using unique bag_id using srid=4326"

    LABELITEM "title"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Planned Protected area (point)"
        STYLE
            SYMBOL  "circle"
            COLOR   "#FFFF00"
            SIZE    15.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END