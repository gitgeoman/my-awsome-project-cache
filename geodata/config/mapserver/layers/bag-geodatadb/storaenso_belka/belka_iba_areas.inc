LAYER
    STATUS       OFF

    NAME         "belka_iba_areas"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "IBA Areas"
        "ows_abstract"        "IBA Areas"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_iba_areas
    ) as foo using unique bag_id using srid=4326"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "IBA Areas"
        STYLE
            COLOR "#FFB95A"
            OUTLINECOLOR "#FF990D"
            WIDTH 1.0
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#FF990D"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
    END

END