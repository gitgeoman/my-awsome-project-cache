LAYER
    STATUS       OFF

    NAME         "belka_osm_roads"
    
    TYPE         LINE
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "OSM Roads"
        "ows_abstract"        "OSM Roads"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_osm_roads
        where
            fclass in (
                'residential',
                'living_street',
                'pedestrian',
                'primary',
                'primary_link',
                'secondary',
                'secondary_link',
                'tertiary',
                'tertiary_link',
                'trunk',
                'trunk_link',
                'motorway',
                'motorway_link',
                'unclassified'
                )
        order by
            code
        desc
    ) as foo using unique gid using srid=4326"

    CLASSITEM "fclass"
    LABELITEM "name"
    SIZEUNITS meters
    SYMBOLSCALEDENOM 50000
    
    CLASS
        NAME "Motorways"
        EXPRESSION {motorway,motorway_link}
        STYLE
           WIDTH 28
           OUTLINEWIDTH 1
           OUTLINECOLOR 193 181 157
        END
        STYLE
            WIDTH 28
            COLOR "#FFF4E5"
        END
        LABEL
            FONT scb
            TYPE TRUETYPE
            MINSIZE 10
            MAXSIZE 20
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 400
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Trunk"
        EXPRESSION {trunk,trunk_link}
        STYLE
           WIDTH 14
           OUTLINEWIDTH 1
           OUTLINECOLOR 193 181 157
        END
        STYLE
            WIDTH 14
            COLOR "#FFF4E5"
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            MINSIZE 10
            MAXSIZE 20
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 400
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Primary"
        MAXSCALEDENOM 500000
        EXPRESSION {primary,primary_link,secondary,secondary_link,tertiary, tertiary_link}
        STYLE
           WIDTH 12
           OUTLINEWIDTH 1
           OUTLINECOLOR 193 181 157
        END
        STYLE
            WIDTH 12
            COLOR "#FEEBB3"
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            MINSIZE 10
            MAXSIZE 20
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 400
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Residential"
        MAXSCALEDENOM 25000
        EXPRESSION {residential,living_street,pedestrian}
        STYLE
           WIDTH 8
           OUTLINEWIDTH 1
           OUTLINECOLOR 193 181 157
        END
        STYLE
            WIDTH 8
            COLOR "#cccccc"
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT scb
            TYPE TRUETYPE
            MINSIZE 10
            MAXSIZE 20
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 400
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Unclassified"
        MAXSCALEDENOM 25000
        EXPRESSION {unclassified}
        STYLE
           WIDTH 8
           OUTLINEWIDTH 1
           OUTLINECOLOR 193 181 157
        END
        STYLE
            COLOR "#F2C680"
            WIDTH 8
        END
    END

END