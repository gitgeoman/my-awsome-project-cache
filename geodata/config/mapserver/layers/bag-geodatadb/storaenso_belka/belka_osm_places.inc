LAYER
    STATUS       OFF

    NAME         "belka_osm_places"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "OSM Places"
        "ows_abstract"        "OSM Places"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_osm_places
        where
            fclass in ('village', 'hamlet')
    ) as foo using unique gid using srid=4326"

    LABELITEM "name"
    
    CLASS
        NAME "Places"
        STYLE
            MAXSCALEDENOM 250000
            SYMBOL  "circle"
            COLOR   "#9E9E9E"
            OUTLINECOLOR "#ffffff"
            SIZE    12.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MAXLENGTH 48
            POSITION uc
            ALIGN CENTER
        END
    END

END