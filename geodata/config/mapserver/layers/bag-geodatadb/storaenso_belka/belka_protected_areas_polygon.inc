LAYER
    STATUS       OFF

    NAME         "belka_existing_protected_areas_polygon"
    GROUP        "belka_existing_protected_areas_group"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "Existing Protected areas"
        "ows_group_title"     "Existing Protected areas"
        "ows_abstract"        "Existing Protected areas"
        "ows_group_abstract"  "Existing Protected areas"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_protected_areas_polygon
        where
            existing is true
    ) as foo using unique bag_id using srid=4326"

    LABELITEM "title"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Existing Protected area"
        STYLE
            COLOR "#7F7FFF"
            OUTLINECOLOR "#0000FF"
            WIDTH 1.0
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#0000FF"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END

LAYER
    STATUS       OFF

    NAME         "belka_planned_protected_areas_polygon"
    GROUP        "belka_planned_protected_areas_group"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "Planned Protected areas (polygon)"
        "ows_group_title"     "Planned Protected areas"
        "ows_abstract"        "Planned Protected areas (polygon)"
        "ows_group_abstract"  "Planned Protected areas"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_protected_areas_polygon
        where
            planned is true
    ) as foo using unique bag_id using srid=4326"

    LABELITEM "title"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Planned Protected area"
        STYLE
            COLOR "#FFFF00"
            OUTLINECOLOR "#FFA500"
            WIDTH 1.0
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#FFA500"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END