LAYER
    STATUS       OFF

    NAME         "belka_osm_railwaystations"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "OSM Railway stations"
        "ows_abstract"        "OSM Railway stations"

        "wms_srs"             "EPSG:4326"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_osm_transport
        where
            fclass = 'railway_station'
    ) as foo using unique gid using srid=4326"

    LABELITEM "name"

    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "Railway stations"
        STYLE
            MAXSCALEDENOM 250000
            SYMBOL  "circle"
            COLOR   "#2874A6"
            OUTLINECOLOR "#ffffff"
            SIZE    12
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT scb
            TYPE TRUETYPE
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            PARTIALS TRUE
            MAXLENGTH 48
            POSITION uc
            ALIGN CENTER
        END
    END

END