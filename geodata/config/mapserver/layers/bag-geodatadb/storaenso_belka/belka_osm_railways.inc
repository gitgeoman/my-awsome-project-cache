LAYER
    STATUS       OFF

    NAME         "belka_osm_railways"
    
    TYPE         LINE
    TEMPLATE     "foo"

    EXTENT       -180.0 -90.0 180.0 90.0
    PROJECTION
        "init=epsg:4326"
    END

    METADATA
        "ows_title"           "OSM Railways"
        "ows_abstract"        "OSM Railways"

        "wms_srs"             "EPSG:4326 EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:4326 EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_belka.belka_osm_railways
    ) as foo using unique gid using srid=4326"

    CLASS
        NAME "Railway"
        MAXSCALEDENOM 10000
        STYLE
            COLOR "#5A5A5A"
            WIDTH 4
        END
        STYLE
            COLOR "#FFFFFF"
            PATTERN 10 12 10 12 END
            WIDTH 2
        END
    END
    CLASS
        NAME "Railway"
        MINSCALEDENOM 10000
        MAXSCALEDENOM 60000
        STYLE
            COLOR "#5A5A5A"
            WIDTH 3
        END
        STYLE
            COLOR "#FFFFFF"
            PATTERN 4 10 4 10 END
            WIDTH 2
        END
    END
    CLASS
        NAME "Railway"
        MINSCALEDENOM 60000
        STYLE
            COLOR "#787878"
            WIDTH 2
        END
    END

END