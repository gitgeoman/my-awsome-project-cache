LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME            "mki_forestusedeclaration"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Metsänkäyttöilmoitukset"
        "ows_abstract"        "Metsäkeskus Metsänkäyttöilmoitukset"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_exclude_items"   "standarrivaldays,standarrivalage"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            id,
            forestusedeclarationnumber,
            declarationreference,
            processingareanumber,
            standnumber,
            standnumberextension,
            area,
            cuttingpurpose,
            cuttingpurposetext,
            cuttingrealizationpractice,
            cuttingrealizationpracticetext,
            standarrivaldate,
            extract(year from standarrivaldate)::int as standarrivalyear,
            standarrivaldays,
            standarrivalage,
            standarrivalmethod,
            standarrivalmethodtext,
            bag_sourcefile,
            bag_source_ts,
            bag_update_ts,
            geometry
        from
            bag_metsankayttoilmoitukset.forestusedeclarationinfo
        order by
            standarrivaldays desc
    ) as foo using unique id using srid=3067"
    LABELITEM     "standarrivalyear"
    
    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "< 1 kk"
        EXPRESSION ([standarrivalage] <= 1) 
        STYLE
            COLOR "#270560"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "1 - 6 kk"
        EXPRESSION ([standarrivalage] > 1 and [standarrivalage] <= 6)
        STYLE
            COLOR "#5e3c99"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "6 - 12 kk"
        EXPRESSION ([standarrivalage] > 6 and [standarrivalage] <= 12)
        STYLE
            COLOR "#b2abd2"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "12 - 24 kk"
        EXPRESSION ([standarrivalage] > 12 and [standarrivalage] <= 24)
        STYLE
            COLOR "#fdb863"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "24 - 36 kk"
        EXPRESSION ([standarrivalage] > 24 and [standarrivalage] <= 37)
        STYLE
            COLOR "#e66101"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END

    END
    CLASS
        NAME "> 36 kk"
        EXPRESSION ([standarrivalage] > 37)
        STYLE
            COLOR "#f7f7f7"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END

    END

END