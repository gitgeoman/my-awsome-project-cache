LAYER
    STATUS      ON
    
    NAME        "maaseutuvirasto_peltolohkorekisteri"
    
    TYPE        POLYGON
    TEMPLATE    "foo"

    MAXSCALEDENOM 50000

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Maaseutuvirasto Peltolohkorekisteri"
        "ows_abstract"        "Maaseutuviraston peltolohkorekisteri"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    PROCESSING "LABEL_NO_CLIP=ON"
    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_maaseutuvirasto.maaseutuvirasto_peltolohkorekisteri_2016) as foo using unique bag_id using srid=3067"

    # hidden because of GDPR (25.6.2019 -Otto)
    # LABELITEM "lohko"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Peltolohkorekisteri"
        STYLE
            COLOR "#FFDA49CC"
            OUTLINECOLOR "#96001B"
            WIDTH 1.1
        END
        
        # hidden because of GDPR (25.6.2019 -Otto)
        # LABEL
        #    MAXSCALEDENOM 10000
        #    FONT sc
        #    TYPE TRUETYPE
        #    SIZE 8
        #    COLOR "#746E70"
        #    OUTLINECOLOR "#FFFFFF"
        #    OUTLINEWIDTH 1
        #    PARTIALS TRUE
        #    MINDISTANCE 50
        #    REPEATDISTANCE 9999
        #    MAXLENGTH 32
        #    ALIGN CENTER
        # END

    END

END