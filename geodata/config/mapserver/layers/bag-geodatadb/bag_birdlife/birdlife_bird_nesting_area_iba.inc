LAYER
    STATUS       OFF
    
    NAME         "birdlife_bird_nesting_area_iba"
    GROUP        "birdlife_bird_nesting_area_all"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Birdlife Lintujen pesimäalueet (IBA)"
        "ows_group_title"     "Birdlife Lintujen pesimäalueet (IBA, FIN-IBA)"
        "ows_abstract"        "Birdlife Lintujen pesimäalueet (IBA)"
        "ows_group_abstract"  "Birdlife Lintujen pesimäalueet (IBA ja FIN-IBA)"


        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, nimi, geom as geometry from bag_birdlife.birdlife_bird_nesting_area_iba) as foo using unique bag_id using srid=3067"

    LABELITEM  "nimi"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Lintujen pesimäalue (IBA)"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#00CC00"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#00CC00"
            WIDTH 1.0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 48
            ALIGN CENTER
        END
    END

END