LAYER
    STATUS          OFF
    MAXSCALEDENOM   100000
    NAME            "mhy_endangered_species"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Uhanalaislajit"
        "ows_abstract"        "Uhanalaislajit"
        "ows_group_title"     "Uhanalaislajit"
        "ows_group_abstract"  "Uhanalaislajit"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "havainto_i"

        "gml_havainto_i_type"   "Integer"
        "gml_id_type"           "Integer"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    
    DATA "geometry from (select
        ogc_fid,
        ely,
        kunta,
        er_nimi,
        laji_id,
        tiet_nimi,
        suomi_nimi,
        iucn_luokk,
        lajin_stat,
        paikka_id,
        havainto_i,
        pvmalku,
        pvmloppu,
        laji_havai,
        maara,
        maara_yks,
        havainto_k,
        geom as geometry from bag_mhy.mhy_endangered_species
    ) as foo using unique havainto_i using srid=3067"

    LABELITEM       "suomi_nimi"

    CLASS
        NAME "Uhanalaislaji"
        STYLE
            SYMBOL  "uhanalaislaji"
            SIZE    30
            OFFSET  0 0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#1e1e1e"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION lc
        END
    END
END