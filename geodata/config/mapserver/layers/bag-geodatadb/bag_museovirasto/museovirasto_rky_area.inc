LAYER
    STATUS       OFF

    NAME         "museovirasto_rky_area"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Museovirasto RKY (Alue)"
        "ows_abstract"        "Museoviraston rakennetut kulttuuriympäristöt (alue)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            id,
            kunta,
            kohdenimi,
            nimi,
            url, 
            geom as geometry 
        from 
            bag_museovirasto.museovirasto_rky_area
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "kohdenimi"

    CLASS
        NAME "Alue"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#7113a0"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#7113a0"
            WIDTH 1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            #COLOR "#746E70"
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END
END