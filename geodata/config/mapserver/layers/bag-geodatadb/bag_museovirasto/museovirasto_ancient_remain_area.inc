LAYER
    STATUS       OFF

    NAME         "museovirasto_ancient_remain_area"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Museovirasto muinaismuisto (Alue)"
        "ows_abstract"        "Museoviraston muinaismuistoalueet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"

        "gml_bag_id_type"     "Integer"
        "gml_id_type"         "Integer"
        "gml_kohdenimi_type"   "Character"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    
    DATA "geometry from (
        select 
            bag_id, 
            bag_id as id, 
            kohdenimi as sitename, 
            mjtunnus, 
            kunta,
            laji, 
            geom as geometry 
        from 
            bag_museovirasto.museovirasto_ancient_remain_area
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "sitename"
    CLASSITEM "laji"
    
    CLASS
        NAME "kiinteä muinaisjäännös"
        EXPRESSION "kiinteä muinaisjäännös"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#FF0000"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#FF0000"
            WIDTH 1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            #COLOR "#746E70"
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END  

    CLASS
        NAME "muu kulttuuriperintökohde"
        EXPRESSION "muu kulttuuriperintökohde"
        STYLE
            SYMBOL  "hatch"
            COLOR   "#A52A2A"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
        STYLE
            OUTLINECOLOR "#A52A2A"
            WIDTH 1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            #COLOR "#746E70"
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END  
END