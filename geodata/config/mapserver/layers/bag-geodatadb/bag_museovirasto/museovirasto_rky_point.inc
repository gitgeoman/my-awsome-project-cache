LAYER
    STATUS          OFF
    MAXSCALEDENOM   80000
    NAME            "museovirasto_rky_point"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Museovirasto RKY (Piste)"
        "ows_abstract"        "Museoviraston rakennetut kulttuuriympäristöt (piste)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            id,
            kunta,
            kohdenimi,
            nimi,
            url, 
            geom as geometry 
        from 
            bag_museovirasto.museovirasto_rky_point
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "kohdenimi"

    CLASS
        NAME "Piste"
        STYLE
            SYMBOL  "rky_piste"
            OUTLINECOLOR "#7113a0"
            SIZE    30
            OFFSET  0 -5
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION cr
        END
    END

END
