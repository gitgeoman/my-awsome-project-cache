LAYER
    STATUS          OFF
    MAXSCALEDENOM   50000
    NAME            "museovirasto_ancient_remain_point"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Museovirasto muinaismuisto (Piste)"
        "ows_abstract"        "Museoviraston muinaismuistopisteet"
        "ows_group_title"     "Museovirasto muinaismuisto (Piste)"
        "ows_group_abstract"  "Museovirasto muinaismuistopisteet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
        
        "gml_bag_id_type"     "Integer"
        "gml_id_type"         "Integer"
        "gml_kohdenimi_type"   "Character"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            bag_id as id, 
            kohdenimi as sitename,
            mjtunnus,
            kunta,
            tyyppi,
            alatyyppi,
            ajoitus,
            laji, 
            geom as geometry 
        from 
            bag_museovirasto.museovirasto_ancient_remain_point
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "sitename"
    CLASSITEM "laji"

    CLASS
        NAME "kiinteä muinaisjäännös"
        EXPRESSION "kiinteä muinaisjäännös"
        STYLE
            SYMBOL  "kiintea_muinaisjaannos"
            SIZE    30
            OFFSET  0 -5
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION cr
        END
    END

    CLASS
        NAME "muu kulttuuriperintökohde"
        EXPRESSION "muu kulttuuriperintökohde"
        STYLE
            SYMBOL  "muu_kulttuuriperintokohde"
            SIZE    30
            OFFSET  0 -5
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 10
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION cr
        END
    END
END

LAYER
    STATUS          OFF
    MINSCALEDENOM   50000
    NAME            "museovirasto_ancient_remain_point_cluster"
    GROUP           "museovirasto_ancient_remain_point"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "wms_title"           "Museovirasto muinaismuisto (Pisteklusteri)"
        "wms_abstract"        "Museoviraston muinaismuistopisteet (Pisteklusteri)"
        "wms_group_title"     "Museovirasto muinaismuisto (Piste)"
        "wms_group_abstract"  "Museovirasto muinaismuistopisteet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "!GetCapabilities"
        "wfs_enable_request"  "!GetCapabilities"
        "gml_include_items"   "all"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"

    DATA "geometry from (
        select 
            bag_id, 
            bag_id as id, 
            kohdenimi,
            mjtunnus,
            kunta,
            tyyppi,
            alatyyppi,
            ajoitus,
            laji, 
            geom as geometry 
        from 
            bag_museovirasto.museovirasto_ancient_remain_point
    ) as foo using unique bag_id using srid=3067"

    LABELITEM "Cluster_FeatureCount"
    CLASSITEM "Cluster_FeatureCount"

    CLUSTER
      MAXDISTANCE 50
      REGION "ellipse"
    END

    PROCESSING "CLUSTER_GET_ALL_SHAPES=ON"
    PROCESSING "ITEMS=bag_id, id, kohdenimi"

    CLASS
      NAME "muinaisjäännös / muu kulttuuriperintökohde"
      EXPRESSION ("[Cluster_FeatureCount]" != "1")
      STYLE
        SIZE 30
        SYMBOL "muinaisjaannos_cluster"
      END
      LABEL
        FONT scb
        TYPE TRUETYPE
        SIZE 10
        COLOR 255 255 255
        ALIGN CENTER
        PRIORITY 10
        BUFFER 1
        PARTIALS TRUE
        POSITION cc
        FORCE TRUE
      END
    END
END