LAYER
    STATUS          OFF

    MAXSCALEDENOM   20000
    
    NAME            "kemera_application"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Metsäkeskus Kemera-hakemusilmoitus"
        "ows_abstract"        "Metsäkeskus Kemera-hakemusilmoitus"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_exclude_items"   "arrivalage"
        "gml_featureid"       "objectid"
    END
        
    #filter out hirvivahingot workcode 34 19.8.2021 

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            objectid,
            kemeratype,
            'https://www.metsakeskus.fi/kemera-tuet' as infourl,
            lawcode,
            cd_lawcode.selite_fi as lawcodetext,
            workcode,
            cd_workcode.selite_fi as workcodetext,
            applicationnumber,
            completiondeclarationnumber,
            applicationarrivaldate,
            arrivalmethod,
            cd_arrivalmethod.selite_fi as arrivalmethodtext,
            arrivaldate,
            extract(year from arrivaldate)::int as arrivalyear,
            estimatedstartdate,
            estimatedenddate,
            projectenddate,
            creationtime,
            updatetime,
            geometry,
            bag_sourcefile,
            bag_source_ts,
            bag_update_ts,
            (now()::date - applicationarrivaldate::date) / 30 AS arrivalage
        from
            bag_kemera.vw_application
        left join
            bag_code.avoinmetsatieto as cd_lawcode
        on
            cd_lawcode.koodistonro = 8001 and
            cd_lawcode.koodi = lawcode::int
        left join
            bag_code.avoinmetsatieto as cd_workcode
        on
            cd_workcode.koodistonro = 3001 and
            cd_workcode.koodi = workcode
        left join
            bag_code.avoinmetsatieto as cd_arrivalmethod
        on
            cd_arrivalmethod.koodistonro = 1008 AND
            cd_arrivalmethod.koodi = arrivalmethod
        where
            applicationarrivaldate > now() - interval '10 years' AND workcode != '34'
        order by
            arrivalage
    ) as foo using unique objectid using srid=3067"
    LABELITEM     "arrivalyear"
    
    COMPOSITE
        OPACITY 75
    END

    CLASS
        NAME "<= 3 kk"
        EXPRESSION ([arrivalage] <= 3) 
        STYLE
            COLOR "#00838F"
        END
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
            PATTERN 8 4 8 4 END
            LINECAP BUTT
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END    
    END
    CLASS
        NAME "3 kk - 1 vuosi"
        EXPRESSION ([arrivalage] > 3 and [arrivalage] <= 12)
        STYLE
            COLOR "#00c18d"
        END
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
            PATTERN 8 4 8 4 END
            LINECAP BUTT
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "1 - 7 vuotta"
        EXPRESSION ([arrivalage] > 12 and [arrivalage] <= 84)
        STYLE
            COLOR "#C0822F"
        END
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
            PATTERN 8 4 8 4 END
            LINECAP BUTT
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "7 - 10 vuotta"
        EXPRESSION ([arrivalage] > 84 and [arrivalage] <= 120)
        STYLE
            COLOR "#543005"
        STYLE
            OUTLINECOLOR "#303030"
            WIDTH 2
            PATTERN 8 4 8 4 END
            LINECAP BUTT
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 10
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
END