LAYER
    STATUS          OFF

    MAXSCALEDENOM   30000
    
    NAME            "kemera_metso"
    
    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Kemera toteutetut Metso-kohteet"
        "ows_abstract"        "Kemera toteutetut Metso-kohteet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "objectid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (
        select
            cd.objectid,
            cd.geometry,
            cd.lawcode,
            cd.workcode,
            cd_lawcode.selite_fi as workcode_txt
        from
            bag_kemera.vw_completiondeclaration as cd
        left join
            bag_code.avoinmetsatieto as cd_lawcode
        on
            cd_lawcode.koodistonro = 3001 and
            cd_lawcode.koodi = cd.workcode::int
        where
            cd.lawcode in ('10', '11') and
            cd.workcode in (40, 330, 331, 641)
        
    ) as foo using unique objectid using srid=3067"

    CLASSITEM "workcode"
    LABELITEM "workcode_txt"

    CLASS
        NAME "Ympäristötukikohteet"
        EXPRESSION {40,331}
        STYLE
            STYLE
            COLOR "#FFBEBECC"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END
    END
    CLASS
        NAME "Luonnonhoitokohteet"
        EXPRESSION {330,641}
        STYLE
            STYLE
            COLOR "#FFBEBECC"
            OUTLINECOLOR "#303030"
            WIDTH 2
        END
        LABEL
            MINSCALEDENOM 0
            MAXSCALEDENOM 16000
            FONT sc
            TYPE truetype
            SIZE 12
            COLOR "#746E70"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ALIGN CENTER
        END 
    END

END