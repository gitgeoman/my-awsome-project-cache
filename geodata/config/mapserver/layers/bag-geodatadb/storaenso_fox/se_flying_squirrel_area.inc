LAYER
    STATUS       OFF

    NAME         "se_flying_squirrel_area"

    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Liito-orava-alueet"
        "ows_abstract"        "Liito-orava-alueet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
        "gml_id_type"         "Integer"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (select * from storaenso_fox.se_flying_squirrel) as foo using unique id using srid=3067"

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Liito-oravat"
        STYLE
            COLOR   "#ff4000"
            OUTLINECOLOR "#ff0000"
            WIDTH 2.0
        END
    END
END