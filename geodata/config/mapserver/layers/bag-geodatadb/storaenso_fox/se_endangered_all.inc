LAYER
    STATUS          OFF
    MAXSCALEDENOM   50000
    NAME            "se_endangered_all"
    
    TYPE            POINT
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Uhanalaislajit"
        "ows_abstract"        "Uhanalaislajit"
        "ows_group_title"     "Uhanalaislajit"
        "ows_group_abstract"  "Uhanalaislajit"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"

        "gml_gid_type"        "Integer"
        "gml_id_type"         "Integer"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select
        gid,
        animalgrou,
        finnishnam,
        iucnclass,
        iucnclassd,
        conservati,
        conserva_1,
        startdate,
        duedate,
        id,
        info,
        geom as geometry from storaenso_fox.se_endangered_species
    ) as foo using unique gid using srid=3067"

    LABELITEM       "finnishnam"

    CLASS
        NAME "Uhanalaislaji"
        STYLE
            SYMBOL  "uhanalaislaji"
            SIZE    30
            OFFSET  0 0
        END
        LABEL
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#1e1e1e"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 1
            REPEATDISTANCE 9999
            MAXLENGTH 32
            POSITION lc
        END
    END
END