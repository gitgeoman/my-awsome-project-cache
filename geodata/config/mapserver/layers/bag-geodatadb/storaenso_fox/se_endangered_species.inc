LAYER
    STATUS        OFF
    MAXSCALEDENOM 30000

    NAME         "se_endangered_species"
    
    TYPE         POINT
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "Uhanalaislajit"
        "ows_abstract"        "Uhanalaislajit"
        "ows_group_title"     "Uhanalaislajit"
        "ows_group_abstract"  "Uhanalaislajit"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "gid"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (SELECT gid, id, tyyppi, finnishnam, nimi_ruotsi, uhanalaisuusluokka, havaintoaika, lisatiedot, kohteen_lisatiedot, pesapuu, geom as geometry FROM storaenso_fox.se_endangered_species_all) as foo using unique gid using srid=3067"

    CLASSITEM "tyyppi"

    CLASS
        NAME "Petolinnut"
        EXPRESSION "petolinnut"
        STYLE
            SYMBOL  "uhanalaislaji"
            SIZE    30
            OFFSET 0 0
        END
        LABEL
            TEXT '[finnishnam]![pesapuu]'
            WRAP "!"
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#1e1e1e"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            OFFSET 1 0
            POSITION CR
            FORCE TRUE
        END
    END
    CLASS
        NAME "Raakut"
        EXPRESSION "raakku"
        STYLE
            SYMBOL  "uhanalaislaji"
            SIZE    30
            OFFSET 0 0
        END
        LABEL
            TEXT '[finnishnam]'
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#1e1e1e"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            POSITION cr
            OFFSET 1 -10
            BUFFER 5
            FORCE TRUE
        END
    END
    CLASS
        NAME "Muut uhanalaislajit"
        EXPRESSION ('[tyyppi]' = 'seuranta' OR '[tyyppi]' = 'liitoorava' OR '[tyyppi]' = 'yksittaishav')
        STYLE
            SYMBOL  "uhanalaislaji"
            SIZE    30
            OFFSET 0 0
        END
        LABEL
            TEXT '[finnishnam]'
            MAXSCALEDENOM 50000
            FONT sc
            TYPE TRUETYPE
            SIZE 8
            COLOR "#1e1e1e"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            POSITION AUTO
            FORCE TRUE
        END
    END
END