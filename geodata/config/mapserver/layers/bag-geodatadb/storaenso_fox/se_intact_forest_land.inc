LAYER
    STATUS       OFF

    NAME         "se_intact_forest_land"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "Intact Forest Land (IFL)"
        "ows_abstract"        "Intact Forest Land (IFL)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
        
        "gml_id_type"         "Integer"
        "gml_ifl_id_type"     "Character"

    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            id,
            ifl_id,
            area_ha,
            geom
        from
            storaenso_fox.se_intact_forest_land
    ) as foo using unique id using srid=3067"

    COMPOSITE
        OPACITY 25
    END

    CLASS
        NAME "Intact Forest Land"
        STYLE
            COLOR "#FF0000"
            OUTLINECOLOR "#8B0000"
            WIDTH 1.0
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#8B0000"
            ANGLE   45
            SIZE    15.0
            WIDTH   1.0
        END
    END

END