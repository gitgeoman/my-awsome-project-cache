LAYER
    STATUS       OFF

    NAME         "se_sami_native_region"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "Sami Native Region"
        "ows_abstract"        "Sami Native Region"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
        
        "gml_id_type"         "Integer"
        "gml_ifl_id_type"     "Character"

    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            id,
            tyyppi,
            muutospvm,
            plk_nimi,
            geom
        from
            storaenso_fox.se_sami_native_region
    ) as foo using unique id using srid=3067"

    LABELITEM       "plk_nimi"
    
    CLASS
        NAME "Reindeer Husbandry Cooperatives"
        EXPRESSION ([tyyppi] in "Hallinnollinen alue")
        STYLE
            COLOR "#f79233"
            OUTLINECOLOR "#564e47"
            WIDTH 2.0
            OPACITY 50
        END
        LABEL
            MAXSCALEDENOM 1500000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            #COLOR "#746E70"
            COLOR "#707070"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MINDISTANCE 50
            REPEATDISTANCE 9999
            MAXLENGTH 32
            ALIGN CENTER
        END
    END
    CLASS
        NAME "Sami Native Region"
        EXPRESSION ([tyyppi] in "Saamelaisalue")
        STYLE
            OUTLINECOLOR "#580099"
            WIDTH 3.0
        END
    END

END