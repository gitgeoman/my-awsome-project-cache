LAYER
    STATUS       OFF

    MAXSCALEDENOM 50000

    NAME         "se_metry_hcv_area"
    
    TYPE         POLYGON
    TEMPLATE     "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "SE-FSC luontokohteet"
        "ows_abstract"        "SE-FSC luontokohteet"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
        
        "gml_id_type"         "Integer"

    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geom from (
        select
            *
        from
            storaenso_fox.se_metry_hcv_2019_luontokohteet
        where
            kohdeluokka > 0
    ) as foo using unique id using srid=3067"

    CLASSITEM "kohdeluokka"

    # Lynx-karttatasoa varten luokat niputetaan seitsemään teemaan, jotka ovat seuraavat (suluissa luontokohdeluokkanumero alkuperäisessä aineistossa):
    # Vanhapuustoiset ja luontoarvoiltaan erottuvat kallioelinympäristöt (1,2,3)
    # Runsaslahopuustoiset kangasmetsät (4)
    # Luontoarvoiltaan erottuvat lehdot (5,9)
    # Luonnontilaiset ja luonnontilaisen kaltaiset suot (10)
    # Luonnontilaiset ja luonnontilaisen kaltaiset kitu- ja joutomaat (11)
    # Luontoarvoiltaan erottuvat rantametsät sekä luonnontilaisten virtavesien ja lähteiden rannat (12,13,15)
    # Metsälaki 10§ -kohteet (99)

    COMPOSITE
        OPACITY 50
    END

    CLASS
        NAME "Vanhapuustoiset ja luontoarvoiltaan erottuvat kallioelinympäristöt"
        EXPRESSION ([kohdeluokka] in "1,2,3")
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   45
            SIZE    8
            WIDTH   2
        END
        STYLE
            OUTLINECOLOR "#999999"
            WIDTH 2
        END
    END
    CLASS
        NAME "Runsaslahopuustoiset kangasmetsät"
        EXPRESSION ([kohdeluokka] = 4)
        STYLE
            SYMBOL  "hatch"
            COLOR   "#FF9300"
            ANGLE   135
            SIZE    8
            WIDTH   2
        END
        STYLE
            OUTLINECOLOR "#FF9300"
            WIDTH 2
        END
    END
    CLASS
        NAME "Luontoarvoiltaan erottuvat lehdot"
        EXPRESSION ([kohdeluokka] in "5,9")
        STYLE
            COLOR "#00bb00"
        END
        STYLE
            OUTLINECOLOR "#00bb00"
            WIDTH 2
        END
    END
    CLASS
        NAME "Luonnontilaiset ja luonnontilaisen kaltaiset suot"
        EXPRESSION ([kohdeluokka] = 10)
        STYLE
            SYMBOL  "hatch"
            COLOR   "#00aa00"
            ANGLE   90
            SIZE    8
            WIDTH   2
        END
        STYLE
            OUTLINECOLOR "#00aa00"
            WIDTH 2
        END
    END
    CLASS
        NAME "Luonnontilaiset ja luonnontilaisen kaltaiset kitu- ja joutomaat"
        EXPRESSION ([kohdeluokka] = 11)
        STYLE
            SYMBOL  "hatch"
            COLOR   "#00aa00"
            ANGLE   0
            SIZE    8
            WIDTH   2
        END
        STYLE
            OUTLINECOLOR "#00oo00"
            WIDTH 2
        END
    END
    CLASS
        NAME "Luontoarvoiltaan erottuvat rantametsät sekä luonnontilaisten virtavesien ja lähteiden rannat"
        EXPRESSION ([kohdeluokka] in "12,13,15")
        STYLE
            COLOR "#0000FF"
            OUTLINECOLOR "#0000FF"
            WIDTH 2
        END
    END
    CLASS
        NAME "Metsälaki 10§ -kohteet"
        EXPRESSION ([kohdeluokka] = 99)
        STYLE
            COLOR "#FF0000"
            OUTLINECOLOR "#8b0000"
            WIDTH 2
        END
    END

END