LAYER
    STATUS OFF

    MAXSCALEDENOM   500000

    NAME            gtk_maapera_20k_pohjalajit
    TYPE            POLYGON

    TEMPLATE        "foo"

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "GTK Maaperä (Pohjamaalajit)"
        "ows_abstract"        "GTK Maaperä (Pohjamaalajit)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, pohjamaalaji_koodi, pohjamaalaji, tunnus, geom as geometry from bag_gtk.gtk_maapera_20k_maalajit) as foo using unique bag_id using srid=3067"

    CLASSITEM "pohjamaalaji_koodi"
    LABELITEM "tunnus"

    CLASS
        NAME "Kalliomaa, maanpeite enintään 1m"
        EXPRESSION ([pohjamaalaji_koodi] = 195111)
        STYLE
            COLOR "#FD807D"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Rapakallio (RpKA)"
        EXPRESSION ([pohjamaalaji_koodi] = 195113)
        STYLE
            COLOR "#FDA394"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Rakka (RaKa)"
        EXPRESSION ([pohjamaalaji_koodi] = 195112)
        STYLE
            COLOR "#FEBDBD"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL "hietikko"
            COLOR "#FD807D"
            SIZE 30
        END
    END
    CLASS
        NAME "Lohkareita (Lo)"
        EXPRESSION ([pohjamaalaji_koodi] = 195311)
        STYLE
            COLOR "#FEBDC2"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL "kivikko"
            COLOR "#FD807D"
            SIZE 200
        END
    END
    CLASS
        NAME "Kiviä (Ki)"
        EXPRESSION ([pohjamaalaji_koodi] = 195312)
        STYLE
            COLOR "#FEBDC2"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Hiekkamoreeni (Mr), Soramoreeni (SrMr)"
        EXPRESSION ([pohjamaalaji_koodi] = 195214 or [pohjamaalaji_koodi] = 195213)
        STYLE
            COLOR "#F6DDB7"
            OUTLINECOLOR "#999999"
        END   
    END
    CLASS
        NAME "Hienoaineismoreeni (HMr)"
        EXPRESSION ([pohjamaalaji_koodi] = 195215)
        STYLE
            COLOR "#FDD37E"
            OUTLINECOLOR "#999999"
        END   
    END
    CLASS
        NAME "Sora (Sr)"
        EXPRESSION /195313/
        STYLE
            COLOR "#74B475"
            OUTLINECOLOR "#999999"
        END  
    END
    CLASS
        NAME "Hiekka (Hk)"
        EXPRESSION {195314}
        STYLE
            COLOR "#CCE8B7"
            OUTLINECOLOR "#999999"
        END 
    END
    CLASS
        NAME "Liejuinen Hiekka, humuspitoisuus 2-6% (LjHk)"
        EXPRESSION ([pohjamaalaji_koodi] = 19531421)
        STYLE
            COLOR "#CCE8B7"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   135
            SIZE    10.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#303030"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MAXLENGTH 32
            ALIGN CENTER
        END 
    END
    CLASS
        NAME "Karkea Hieta (KHt)"
        EXPRESSION ([pohjamaalaji_koodi] = 195315)
        STYLE
            COLOR "#FFFEBC"
            OUTLINECOLOR "#999999"
        END 
    END
    CLASS
        NAME "Liejuinen Hieta (karkea), humuspitoisuus 2-6% (LjHt)"
        EXPRESSION {19531521}
        STYLE
            COLOR "#FFFEBC"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   135
            SIZE    8.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#303030"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MAXLENGTH 32
            ALIGN CENTER
        END 
    END
    CLASS
        NAME "Hieno Hieta (HHt)"
        EXPRESSION ([pohjamaalaji_koodi] = 195411)
        STYLE
            COLOR "#E8BEFD"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Liejuinen Hieno Hieta, humuspitoisuus 2-6% (LjHHt)"
        EXPRESSION ([pohjamaalaji_koodi] = 19541121)
        STYLE
            COLOR "#E8BEFD"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   135
            SIZE    8.0
            WIDTH   1.0
        END
        LABEL
            MAXSCALEDENOM 25000
            FONT sc
            TYPE TRUETYPE
            SIZE 12
            COLOR "#303030"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            MAXLENGTH 32
            ALIGN CENTER
        END 
    END
    CLASS
        NAME "Hiesu (Hs)"
        EXPRESSION ([pohjamaalaji_koodi] = 195412)
        STYLE
            COLOR "#DDDCFE"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Liejuhiesu, humuspitoisuus 2-6% (LjHs)"
        EXPRESSION ([pohjamaalaji_koodi] = 19541221)
        STYLE
            COLOR "#B2DCFB"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   135
            SIZE    4.0
            WIDTH   1.0
        END
    END
    CLASS
        NAME "Savi (sa)"
        EXPRESSION {195413}
        STYLE
            COLOR "#AFDDFD"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Liejusavi, humuspitoisuus 2-6% (LjSa)"
        EXPRESSION ([pohjamaalaji_koodi] = 19541321)
        STYLE
            COLOR "#AFDDFD"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   135
            SIZE    4.0
            WIDTH   1.0
        END
    END
    CLASS
        NAME "Lieju, humuspitoisuus yli 6% (Lj)"
        EXPRESSION ([pohjamaalaji_koodi] = 195511)
        STYLE
            COLOR "#8D8D8D"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Rahkaturve (St)"
        EXPRESSION ([pohjamaalaji_koodi] = 195513)
        STYLE
            COLOR "#F1F1F1"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Saraturve (Ct)"
        EXPRESSION ([pohjamaalaji_koodi] = 195512)
        STYLE
            COLOR "#CECECE"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Turvetuotantoalue (Tu)"
        EXPRESSION ([pohjamaalaji_koodi] = 195515)
        STYLE
            COLOR "#828282"
            OUTLINECOLOR "#999999"
        END
    END
    CLASS
        NAME "Täytemaa (Ta)"
        EXPRESSION ([pohjamaalaji_koodi] = 195601)
        STYLE
            COLOR "#FFFFFF"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   45
            SIZE    4.0
            WIDTH   1.0
        END
    END
    CLASS
        NAME "Kartoittamaton (0)"
        EXPRESSION ([pohjamaalaji_koodi] = 195602)
        STYLE
            COLOR "#FFFFFF"
            OUTLINECOLOR "#999999"
        END
        STYLE
            SYMBOL  "hatch"
            COLOR   "#999999"
            ANGLE   45
            SIZE    8.0
            WIDTH   1.0
        END
    END
    CLASS
        NAME "Vesi (Ve)"
        EXPRESSION ([pohjamaalaji_koodi] = 195603)
        STYLE
            OUTLINECOLOR "#999999"
        END
    END
END