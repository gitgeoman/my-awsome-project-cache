LAYER
    STATUS          OFF

    MAXSCALEDENOM   25000

    NAME            "mml_rakennus"

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Rakennus"
        "ows_abstract"        "MML Maastotietokannan rakennukset"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, kohdeluokka, geom as geometry from bag_mml_maastotietokanta.rakennus) as foo using unique bag_id using srid=3067"

    CLASSITEM "kohdeluokka"

    CLASS
        NAME "Liike- tai julkinen rakennus"
        EXPRESSION {42220,42221,42222}
        STYLE
            COLOR "#F9C3DC"
            OUTLINECOLOR "#484848"
        END
    END
    CLASS
        NAME "Teollisuusrakennus"
        EXPRESSION {42240,42241,42242}
        STYLE
            COLOR "#D3C9CE"
            OUTLINECOLOR "#484848"
        END
    END
    CLASS
        NAME "Kirkollinen rakennus, kirkko"
        EXPRESSION {42250,42251,42252,42170,42270}
        STYLE
            COLOR "#D0ABD6"
            OUTLINECOLOR "#484848"
        END
    END
    CLASS
        NAME "Lomarakennus"
        EXPRESSION {42230,42231,42232}
        STYLE
            COLOR "#9BD6D0"
            OUTLINECOLOR "#484848"
        END
    END
    CLASS
        NAME "Muu rakennus"
        EXPRESSION {42260,42261,42262}
        STYLE
            COLOR "#E9EBEB"
            OUTLINECOLOR "#484848"
        END
    END
    CLASS
        STYLE
            COLOR "#BEC0C2"
            OUTLINECOLOR "#484848"
        END
    END
END