LAYER
    STATUS          OFF

    MAXSCALEDENOM   25000

    NAME            "mml_meri"
    GROUP           "mml_vesistot"

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Meri"
        "ows_group_title"     "MML Vesialueet"
        "ows_abstract"        "MML Maastotietokannan merialueet"
        "ows_group_abstract"  "MML Maastotietokannan vesialueet (meri, järvi, virtavesi)"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_mml_maastotietokanta.meri) as foo using unique bag_id using srid=3067"

    CLASS
        NAME "Meri"
        STYLE
            COLOR "#B4E5F9"
            OUTLINECOLOR "#B4E5F9"
            OUTLINEWIDTH 1
        END
    END
END