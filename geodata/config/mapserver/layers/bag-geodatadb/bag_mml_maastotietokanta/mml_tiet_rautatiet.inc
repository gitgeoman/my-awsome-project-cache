LAYER
    STATUS          OFF
    MAXSCALEDENOM   25000
    
    NAME            "mml_tiet_rautatiet"
    GROUP           "mml_vrk_tiesto_osoitteilla"
    

    TYPE            LINE
    TEMPLATE        "foo"

    EXTENT  50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    METADATA
        "ows_title"           "MML Tiet & Rautatiet"
        "ows_abstract"        "MML Maastotietokannan tiet ja rautatiet"
        "ows_group_title"     "MML Tiestö ja VRK Osoitteet"
        "ows_group_abstract"  "MML Tiestö ja VRK Osoitteet tasoryhmä"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select geom as geometry, bag_id, kohdeluokka, nimi_suomi from bag_mml_maastotietokanta.tiet_rautatiet WHERE valmiusaste = 0 ORDER BY tasosijainti ASC, kohdeluokka DESC) as foo using unique bag_id using srid=3067"
    
    SIZEUNITS meters
    CLASSITEM "kohdeluokka"
    LABELITEM "nimi_suomi"

    CLASS
        NAME "Rautatie"
        EXPRESSION {14112,14121,14111,14131}
        STYLE
            COLOR "#484848"
            WIDTH 5
        END
        STYLE
            COLOR "#99FFFF"
            PATTERN 8 12 8 12 END
            WIDTH 2
        END
    END
    CLASS
        NAME "Polku"
        EXPRESSION /12313/
        STYLE
            COLOR "#606060"
            PATTERN 4 6 4 6 END
            WIDTH 1.5
        END
    END
    CLASS
        NAME "Ajopolku"
        EXPRESSION /12316/
        STYLE
            COLOR "#606060"
            PATTERN 4 6 4 6 END
            WIDTH 1.5
        END
    END
    CLASS
        NAME "Kävely- ja pyörätie"
        EXPRESSION /12314/
        STYLE
            COLOR "#606060"
            PATTERN 12 4 12 4 END
            WIDTH 2
        END
    END
    CLASS
        NAME "Ajotie"
        EXPRESSION /12141/
        STYLE
            WIDTH 3
            MAXWIDTH 300
            COLOR "#606060"
        END
        LABEL
            MAXSCALEDENOM 21000
            FONT sc
            TYPE TRUETYPE
            MINSIZE 10
            MAXSIZE 14
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie IIIb"
        EXPRESSION /12132/
        STYLE
            WIDTH 4
            MAXWIDTH 400
            OUTLINECOLOR "#484848"
            OUTLINEWIDTH 1
        END
        STYLE
            WIDTH 4
            MAXWIDTH 400
            COLOR "#E68080"
        END
        LABEL
            MAXSCALEDENOM 21000
            FONT sc
            TYPE TRUETYPE
            MINSIZE 11
            MAXSIZE 15
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie IIIa"
        EXPRESSION /12131/
        STYLE
            WIDTH 5
            MAXWIDTH 500
            OUTLINECOLOR "#484848"
            OUTLINEWIDTH 1
        END
        STYLE
            WIDTH 5
            MAXWIDTH 500
            COLOR "#E68080"
        END
        LABEL
            MAXSCALEDENOM 21000
            FONT sc
            TYPE TRUETYPE
            MINSIZE 11
            MAXSIZE 15
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie IIb"
        EXPRESSION /12122/
        STYLE
            WIDTH 6.5
            MAXWIDTH 650
            OUTLINECOLOR "#484848"
            OUTLINEWIDTH 1
        END
        STYLE
            WIDTH 6.5
            MAXWIDTH 650
            COLOR "#E68080"
        END
        LABEL
            MAXSCALEDENOM 21000
            MINSIZE 12
            MAXSIZE 18
            FONT sc
            TYPE TRUETYPE
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie IIa"
        EXPRESSION /12121/
        STYLE
            WIDTH 8
            MAXWIDTH 800
            OUTLINECOLOR "#484848"
            OUTLINEWIDTH 1
        END
        STYLE
            WIDTH 8
            MAXWIDTH 800
            COLOR "#E68080"
        END
        LABEL
            MAXSCALEDENOM 21000
            MINSIZE 12
            MAXSIZE 18
            FONT sc
            TYPE TRUETYPE
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie Ib"
        EXPRESSION /12112/
        STYLE
            WIDTH 9
            MAXWIDTH 900
            OUTLINEWIDTH 1
            OUTLINECOLOR "#484848"
        END
        STYLE
            WIDTH 9
            MAXWIDTH 900
            COLOR "#E68080"
        END
        LABEL
            MAXSCALEDENOM 21000
            MINSIZE 15
            MAXSIZE 19
            FONT sc
            TYPE TRUETYPE
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 1
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
    CLASS
        NAME "Autotie Ia"
        EXPRESSION /12111/
        STYLE
            WIDTH 9
            MAXWIDTH 900
            OUTLINEWIDTH 1
            OUTLINECOLOR "#484848"
        END
        STYLE
            WIDTH 9
            MAXWIDTH 900
            COLOR "#E68080"
        END
        LABEL
            MINSIZE 15
            MAXSIZE 19
            FONT sc
            TYPE TRUETYPE
            COLOR "#000000"
            OUTLINECOLOR "#FFFFFF"
            OUTLINEWIDTH 2
            ANGLE FOLLOW
            MINDISTANCE 200
            REPEATDISTANCE 400
        END
    END
END