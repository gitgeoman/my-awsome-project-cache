LAYER
    STATUS          OFF

    MAXSCALEDENOM   25000

    NAME            "mml_maatalousmaa"

    TYPE            POLYGON
    TEMPLATE        "foo"

    EXTENT       50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    METADATA
        "ows_title"           "MML Maatalousmaa"
        "ows_abstract"        "MML Maastotietokannan maatalousmaa"

        "wms_srs"             "EPSG:3067"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3067"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "bag_id"
    END

    INCLUDE "includes/connections/master-prod-geodatadb-reader.inc"
    DATA "geometry from (select bag_id, geom as geometry from bag_mml_maastotietokanta.maatalousmaa) as foo using unique bag_id using srid=3067"

    CLASS
        NAME "Maatalousmaa"
        STYLE
            COLOR "#FEFBD5"
            OUTLINECOLOR "#A1A1A1"
            OUTLINEWIDTH 1
        END
    END
END