LAYER
    STATUS       OFF

    NAME        "diag_ai_at_imagedate"
    TYPE        POLYGON
    TEMPLATE    "foo"

    EXTENT       570403 5676651 1966753 7506238
    PROJECTION
        "init=epsg:3857"
    END
    
    METADATA
        "ows_title"           "Processing date for satellite images (Austria)"
        "ows_abstract"        "Processing date for satellite images (Austria)"

        "wms_srs"             "EPSG:3857"
        "wms_enable_request"  "*"
        "wms_include_items"   "all"

        "wfs_srs"             "EPSG:3857"
        "wfs_enable_request"  "*"
        "gml_include_items"   "all"
        "gml_featureid"       "id"
    END

    INCLUDE "includes/connections/master-prod-change-detection-service-db.inc"
    DATA "geometry from (select 
            id, 
            date,
            (now()::date - date::date) / 30 AS arrivalage,
            geometry 
        from 
            mat_map_raster_austria_wsmap3
        ) as foo using unique id using srid=3857"
    
    PROCESSING "LABEL_NO_CLIP=on"

    LABELITEM "date"

    CLASS
        NAME "< 2022-05-09"
        EXPRESSION ('[date]' < '2022-05-09') 
        STYLE
            OUTLINECOLOR "#404040"
            WIDTH 0.5
        END
        STYLE
            COLOR "#ac0404"
            OPACITY 25
        END
        LABEL
            MINSCALEDENOM 1400
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            MINSIZE 14
            MAXSIZE 16
            COLOR "#404040"
            OUTLINECOLOR "#ffffff"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            ALIGN CENTER
            POSITION cc
        END 
    END

	CLASS
        NAME ">= 2022-05-09"
        EXPRESSION ('[date]' >= '2022-05-09') 
        STYLE
            OUTLINECOLOR "#404040"
            WIDTH 0.5
        END
        STYLE
            COLOR "#28ff68"
            OPACITY 25
        END
        LABEL
            MINSCALEDENOM 1400
            MAXSCALEDENOM 500000
            FONT sc
            TYPE TRUETYPE
            MINSIZE 14
            MAXSIZE 16
            COLOR "#404040"
            OUTLINECOLOR "#ffffff"
            OUTLINEWIDTH 1
            PARTIALS TRUE
            ALIGN CENTER
            POSITION cc
        END 
    END

END