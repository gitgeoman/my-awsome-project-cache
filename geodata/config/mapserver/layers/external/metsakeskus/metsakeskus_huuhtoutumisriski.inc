LAYER
    STATUS          OFF

    MINSCALEDENOM   25000

    NAME            metsakeskus_huuhtoutumisriski_kattavuus
    GROUP           metsakeskus_huuhtoutumisriski

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "http://aineistot.metsakeskus.fi/metsakeskus/services/Vesiensuojelu/Vesiuomien_maa_aineksen_huuhtoutumisriski/MapServer/WMSServer?"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "3"
        "wms_title"             "Metsäkeskus Huuhtoutumisriski kattavuus"
        "wms_abstract"          "Metsäkeskus Huuhtoutumisriski kattavuus"
        "wms_group_abstract"    "Metsäkeskus Huuhtoutumisriski"
        "wms_server_version"    "1.1.1"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END


LAYER
    STATUS          OFF
    MAXSCALEDENOM   25000

    NAME            metsakeskus_huuhtoutumisriski_rajanopeus
    GROUP           metsakeskus_huuhtoutumisriski

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "http://aineistot.metsakeskus.fi/metsakeskus/services/Vesiensuojelu/Vesiuomien_maa_aineksen_huuhtoutumisriski/MapServer/WMSServer?"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "2"
        "wms_title"             "Metsäkeskus Huuhtoutumisriski rajanopeus"
        "wms_abstract"          "Metsäkeskus Huuhtoutumisriski rajanopeus"
        "wms_group_title"       "Metsäkeskus Huuhtoutumisriski"
        "wms_group_abstract"    "Metsäkeskus Huuhtoutumisriski"
        "wms_server_version"    "1.1.1"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END