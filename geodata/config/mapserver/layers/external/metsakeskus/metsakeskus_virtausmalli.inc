LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    NAME            metsakeskus_virtausverkko_nopeus

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "https://rajapinnat.metsaan.fi/geoserver/Vesiensuojelu/Pintavesien_virtausmalli/wms"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "virtausmalli_nopeus"
        "wms_title"             "Metsäkeskus Virtausmalli (Nopeus)"
        "wms_abstract"          "Metsäkeskus Virtausmalli (Nopeus)"
        "wms_server_version"    "1.1.1"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END


LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    NAME            metsakeskus_virtausverkko_kaltevuus
    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "https://rajapinnat.metsaan.fi/geoserver/Vesiensuojelu/Pintavesien_virtausmalli/wms"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "virtausmalli_kaltevuus"
        "wms_title"             "Metsäkeskus Virtausmalli (Kaltevuus)"
        "wms_abstract"          "Metsäkeskus Virtausmalli (Kaltevuus)"
        "wms_server_version"    "1.1.1"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END

LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    NAME            metsakeskus_virtausverkko_ala
    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END
    
    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "https://rajapinnat.metsaan.fi/geoserver/Vesiensuojelu/Pintavesien_virtausmalli/wms"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "virtausmalli_ala"
        "wms_title"             "Metsäkeskus Virtausmalli (Ala)"
        "wms_abstract"          "Metsäkeskus Virtausmalli (Ala)"
        "wms_server_version"    "1.1.1"
        "wms_format"             "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END
