LAYER
    STATUS          OFF
    MAXSCALEDENOM   100000

    NAME            metsakeskus_kosteusindeksi_1ha

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "http://aineistot.metsakeskus.fi/metsakeskus/services/Vesiensuojelu/Kosteusindeksi_DTW_1ha/MapServer/WmsServer?"
    CONNECTIONTYPE WMS

    COMPOSITE
        OPACITY 50
    END

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "1"
        "wms_title"             "Metsäkeskus kosteusindeksi 1ha"
        "wms_abstract"          "DTW-indeksi aineisto koostuu yhden ja neljän hehtaarin virtuaaliuoman raja-arvoilla lasketuista kartoista. Yhden hehtaarin raja-arvolla lasketun kartan voidaan ajatella kuvaavan maan pintakerroksen märkyyttä sateisena ajanjaksona tai keväällä ja syksyllä."
        "wms_server_version"    "1.3.0"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END
END

LAYER
    STATUS          OFF
    MAXSCALEDENOM   100000

    NAME            metsakeskus_kosteusindeksi_4ha

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "http://aineistot.metsakeskus.fi/metsakeskus/services/Vesiensuojelu/Kosteusindeksi_DTW_4ha/MapServer/WmsServer"
    CONNECTIONTYPE WMS

    COMPOSITE
        OPACITY 50
    END

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "1"
        "wms_title"             "Metsäkeskus kosteusindeksi 4ha"
        "wms_abstract"          "DTW-indeksi aineisto koostuu yhden ja neljän hehtaarin virtuaaliuoman raja-arvoilla lasketuista kartoista. Kartan, jossa raja-arvona on käytetty neljää hehtaaria, voidaan ajatella kuvaavan kuivempaa ajanjaksoa."
        "wms_server_version"    "1.3.0"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END
END