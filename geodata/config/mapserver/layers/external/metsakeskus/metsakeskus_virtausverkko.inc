LAYER
    STATUS          OFF
    MAXSCALEDENOM   20000

    NAME            metsakeskus_virtausverkko

    TYPE            RASTER

    EXTENT          50199.4814 6582464.0358 761274.6247 7799839.8902
    PROJECTION
        "init=epsg:3067"
    END

    PROCESSING      "RESAMPLE=BILINEAR"

    CONNECTION "http://aineistot.metsakeskus.fi/metsakeskus/services/Vesiensuojelu/Virtausverkko/MapServer/WMSServer"
    CONNECTIONTYPE WMS

    METADATA
        "wms_srs"               "EPSG:3067"
        "wms_name"              "0"
        "wms_title"             "Metsäkeskus Virtausverkko"
        "wms_abstract"          "Metsäkeskus Virtausverkko"
        "wms_server_version"    "1.1.1"
        "wms_format"            "image/png"
        "wms_cache_to_disk"     "1"
        "wms_connectiontimeout" "15"
    END

END