#-----------------------------------------------------------------------------
# Nginx cache and proxy buffering setup
#-----------------------------------------------------------------------------

proxy_cache_path /tmp/cache levels=1:2 keys_zone=geocache:10m max_size=2g inactive=12h use_temp_path=off;

#-----------------------------------------------------------------------------
# Geodata Services Proxy
#-----------------------------------------------------------------------------

server {

    # dont show server tokens and server name in headers
    server_tokens      off;
    more_clear_headers Server;

    listen 80;

    # gzip settings 
    gzip on;
    gzip_types      text/plain text/xml application/json application/xml;
    gzip_min_length 1000;

    #-----------------------------------------------------------------------------
    # Root showing the landing page
    #-----------------------------------------------------------------------------

    location / {
        root /opt/www;
    }

    #-----------------------------------------------------------------------------
    # Service endpoints
    #-----------------------------------------------------------------------------
    
    # common ows service endpoint
    location /geodata/common/ows {
        rewrite ^/geodata/common/ows(.*) /mapserver-common?map=/opt/mapserver/common.map;
    }

    # common wmts service endpoint
    location /geodata/common/wmts {
        rewrite ^/geodata/common/wmts(.*) /mapcache-common?&;
    }

    # ahlstrom ows service endpoint
    location /geodata/ahlstrom/ows {
        rewrite ^/geodata/ahlstrom/ows(.*) /mapserver-common?map=/opt/mapserver/ahlstrom.map;
    }

    # laatukuva ows service endpoint
    location /geodata/laatukuva/ows {
        rewrite ^/geodata/laatukuva/ows(.*) /mapserver-common?map=/opt/mapserver/laatukuva.map;
    }

    # mhy leafpoint service ows endpoint
    location /geodata/mhy/leafpoint/ows {
        rewrite ^/geodata/mhy/leafpoint/ows(.*) /mapserver-common?map=/opt/mapserver/mhy-leafpoint.map;
    }
    
    # mhy leafpoint service wmts endpoint
    location /geodata/mhy/leafpoint/wmts {
        rewrite ^/geodata/mhy/leafpoint/wmts(.*) /mapcache-mhy-leafpoint?&;
    }

    #-----------------------------------------------------------------------------
    # Internal common services
    #-----------------------------------------------------------------------------
    
    # mapserver-common location
    location /mapserver-common {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-ows-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-ows-header.lua;

        # cors
        include                  /opt/cors/geodata-cors.conf;

        # proxy to mapserver
        proxy_pass          http://mapserver-common-service:8080/mapserver;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
    }

    # mapcache-common location
    location /mapcache-common {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-common-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host $http_host;
        proxy_set_header    X-Forwarded-Server $http_host;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    Host $http_host;
        proxy_set_header    X-Real-IP $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    #-----------------------------------------------------------------------------
    # Germany service locations
    #-----------------------------------------------------------------------------
    
    # public germany ows service endpoint
    location /geodata/germany/ows {
        rewrite ^/geodata/germany/ows(.*) /mapserver-germany?map=/opt/mapserver/germany.map;
    }

    # public germany wmts service endpoint
    location /geodata/germany/wmts {
        rewrite ^/geodata/germany/wmts(.*) /mapcache-germany?&;
    }

    # internal mapserver-germany location
    location /mapserver-germany {
        internal;

        # auth
        # access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        # body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-ows-body.lua;
        # header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-ows-header.lua;

        # cors
        include                  /opt/cors/geodata-cors.conf;

        # proxy to mapserver
        proxy_pass          http://mapserver-germany-service:8080/mapserver;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
    }

    # internal mapcache-germany location
    location /mapcache-germany {
        internal;

        # auth
        # access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        # body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        # header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-germany-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host $http_host;
        proxy_set_header    X-Forwarded-Server $http_host;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    Host $http_host;
        proxy_set_header    X-Real-IP $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    #-----------------------------------------------------------------------------
    # Stora Enso service locations
    #-----------------------------------------------------------------------------
    
    # FOX ows
    location /geodata/storaenso/fox/ows {
        rewrite ^/geodata/storaenso/fox/ows(.*) /mapserver-storaenso?map=/opt/mapserver/storaenso-fox.map;
    }

    # FOX wmts
    location /geodata/storaenso/fox/wmts {
        rewrite ^/geodata/storaenso/fox/wmts(.*) /mapcache-storaenso-fox?&;
    }

    # Belka ows
    location /geodata/storaenso/belka/ows {
        rewrite ^/geodata/storaenso/belka/ows(.*) /mapserver-storaenso?map=/opt/mapserver/storaenso-belka.map;
    }

    # Belka wmts
    location /geodata/storaenso/belka/wmts {
        rewrite ^/geodata/storaenso/belka/wmts(.*) /mapcache-storaenso-belka?&;
    }


    # internal mapserver-storaenso location
    location /mapserver-storaenso {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-ows-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-ows-header.lua;

        # cors
        include                  /opt/cors/geodata-cors.conf;

        # proxy to mapserver
        proxy_pass          http://mapserver-storaenso-service:8080/mapserver;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
    }

    # internal mapcache-storaenso-fox location
    location /mapcache-storaenso-fox {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-storaenso-fox-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    # internal mapcache-storaenso-fox location
    location /mapcache-storaenso-belka {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-storaenso-belka-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    #-----------------------------------------------------------------------------
    # Tornator service locations
    #-----------------------------------------------------------------------------
    
    # public tornamarket ows endpoint
    location /geodata/tornator/market/ows {
        rewrite ^/geodata/tornator/market/ows(.*) /mapserver-tornator?map=/opt/mapserver/tornator-market.map;
    }

    # public tornamarket wmts service endpoint
    location /geodata/tornator/market/wmts {
        rewrite ^/geodata/tornator/market/wmts(.*) /mapcache-tornator?&;
    }

    # internal mapserver-tornator location
    location /mapserver-tornator {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-tornamarket-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-ows-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-ows-header.lua;

        # cors
        include                  /opt/cors/geodata-cors.conf;

        # proxy to mapserver
        proxy_pass          http://mapserver-tornator-service:8080/mapserver;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
    }

    # internal mapcache-tornator location
    location /mapcache-tornator {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-tornamarket-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-tornator-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host    $http_host;
        proxy_set_header    X-Forwarded-Server  $http_host;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    #-----------------------------------------------------------------------------
    # MHY service locations
    #-----------------------------------------------------------------------------
    
    # mapcache-mhy-leafpoint location
    location /mapcache-mhy-leafpoint {
        internal;

        # auth
        access_by_lua_file        /opt/auth/lua/geodata-auth.lua;
        body_filter_by_lua_file   /opt/auth/lua/geodata-capabilities-wmts-body.lua;
        header_filter_by_lua_file /opt/auth/lua/geodata-capabilities-wmts-header.lua;

        # cors
        include                   /opt/cors/geodata-cors.conf;

        # proxy to mapcache service
        proxy_pass          http://mapcache-mhy-leafpoint-service:8080/mapcache/wmts;
        proxy_set_header    X-Forwarded-Host $http_host;
        proxy_set_header    X-Forwarded-Server $http_host;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    Host $http_host;
        proxy_set_header    X-Real-IP $remote_addr;

        # nginx cache
        proxy_cache             geocache;
        proxy_cache_valid	    200 304 24h;
        proxy_cache_use_stale   error timeout invalid_header;

        expires                 24h;

        add_header              Cache-Control "public";
        add_header              X-Proxy-Cache $upstream_cache_status;

    }

    #-----------------------------------------------------------------------------
    # Custom error pages
    #-----------------------------------------------------------------------------
    
    error_page 401 /401.html;
    location = /401.html {
        root /opt/www;
        internal;
    }

    error_page 403 /403.html;
    location = /403.html {
        root /opt/www;
        internal;
    }

    error_page 404 /404.html;
    location = /404.html {
        root /opt/www;
        internal;
    }

}
