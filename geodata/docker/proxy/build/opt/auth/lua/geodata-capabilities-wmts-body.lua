-- if we have maptoken as argument
if ngx.var.arg_maptoken and ngx.var.arg_maptoken ~= "" then
    -- lowercase args
    local args = {}
    for k, v in pairs(ngx.req.get_uri_args()) do args[k:lower()] = v:lower() end
    -- only if request is getCapabilities
    if args['request'] == 'getcapabilities' then
        if args['service'] == "wmts" then
            -- common
            if string.find(ngx.arg[1], "geodata/common") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/common/wmts%?", "geodata/common/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- germany
            if string.find(ngx.arg[1], "geodata/germany") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/germany/wmts%?", "geodata/germany/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- storaenso belka
            if string.find(ngx.arg[1], "geodata/storaenso/belka") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/storaenso/belka/wmts%?", "geodata/storaenso/belka/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- storaenso fox
            if string.find(ngx.arg[1], "geodata/storaenso/fox") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/storaenso/fox/wmts%?", "geodata/storaenso/fox/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- laatukuva
            if string.find(ngx.arg[1], "geodata/laatukuva") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/laatukuva/wmts%?", "geodata/laatukuva/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- tornator
            if string.find(ngx.arg[1], "geodata/tornator/market") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/tornator/market/wmts%?", "geodata/tornator/market/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- changedetection finland
            if string.find(ngx.arg[1], "geodata/changedetection/finland") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/changedetection/finland/wmts%?", "geodata/changedetection/finland/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            --  ultrahack
            if string.find(ngx.arg[1], "geodata/ultrahack") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/ultrahack/wmts%?", "geodata/ultrahack/wmts?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
        end
    end
end