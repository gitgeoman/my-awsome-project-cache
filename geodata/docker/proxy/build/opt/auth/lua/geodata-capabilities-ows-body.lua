-- if we have maptoken as argument
if ngx.var.arg_maptoken and ngx.var.arg_maptoken ~= "" then
    -- lowercase args
    local args = {}
    for k, v in pairs(ngx.req.get_uri_args()) do args[k:lower()] = v:lower() end
    -- only if request is getCapabilities
    if args['request'] == 'getcapabilities' then
        -- ows
        if args['service'] == "wms" or args['service'] == 'wfs' then
            -- common
            if string.find(ngx.arg[1], "geodata/common") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/common/ows%?", "geodata/common/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- ahlstrom
            if string.find(ngx.arg[1], "geodata/ahlstrom") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/ahlstrom/ows%?", "geodata/ahlstrom/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- germany
            if string.find(ngx.arg[1], "geodata/germany") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/germany/ows%?", "geodata/germany/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- storaenso belka
            if string.find(ngx.arg[1], "geodata/storaenso/belka") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/storaenso/belka/ows%?", "geodata/storaenso/belka/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- storaenso fox
            if string.find(ngx.arg[1], "geodata/storaenso/fox") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/storaenso/fox/ows%?", "geodata/storaenso/fox/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- laatukuva
            if string.find(ngx.arg[1], "geodata/laatukuva") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/laatukuva/ows%?", "geodata/laatukuva/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
            -- tornator market
            if string.find(ngx.arg[1], "geodata/tornator/market") ~= nil then
                ngx.arg[1] = string.gsub(ngx.arg[1], "geodata/tornator/market/ows%?", "geodata/tornator/market/ows?maptoken=" ..ngx.var.arg_maptoken .."&amp;")
            end
        end
    end
end