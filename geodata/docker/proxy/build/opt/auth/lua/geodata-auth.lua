local cjson = require("cjson")
local jwt = require("resty.jwt")
---------------------------------------------------------------
SECRET = "3eXGtcnUGbq63yBcV6n5ChRL"
CATALOG = cjson.decode(io.open("/opt/auth/geodata-maplayers.json", "r"):read("*all"))
ALLOWED = { }
NAMESPACE = "geodata:"
---------------------------------------------------------------
-- Helper functions
---------------------------------------------------------------

-- Filter table with function
function filter(func, tbl)
    local t = {}
    for i,v in pairs(tbl) do
        if func(v) then
            t[i]=v
        end
    end
    return t
end

-- Check if given layer_name (str) is in allowed layers list
function inAllowed(layerName)
    if ALLOWED[layerName] == true then
        return true
    else
        return false
    end
end

-- Split string to table
function split(s, delimiter)
    local r = { }
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(r, match)
    end
    return r
end

-- Parse allowed basic http and maptoken credentials
function credentialsFrom(file)
    local a = { }
    local jsonObj = cjson.decode(io.open(file, "r"):read("*all"))
    local users = jsonObj["users"]
    for index in ipairs(users) do
        a[index] = {
            ['authorization'] = users[index].authorization,
            ['maptoken'] = users[index].maptoken,
        }
    end
    return a
end

-- Convert http auth to token
function convertBasic(auth)
    local users = credentialsFrom("/opt/auth/geodata-users.json")
    for i in ipairs(users) do
        if users[i]['authorization'] == auth  then
            return users[i]['maptoken']
        end
    end
end

-- Add header for displaying message
function addHeader(msg)
    ngx.header['bag-geodata-access'] = msg
end

---------------------------------------------------------------
-- Authentication and authorization
---------------------------------------------------------------
function authorize(token, args_original)

    -- deny by default
    reqAllowed = false
    denyReason = ''

    -- lowercase uri arg keys
    local args = { }
    for k, v in pairs(args_original) do args[k:lower()] = v:lower() end

    -- populate allowed layers table from token
    for k,v in pairs(token["l"]) do
        -- check that maplayers catalog has given integer
        local layerName = CATALOG[tostring(v)]
        -- only push to allowed when index found
        if layerName then ALLOWED[CATALOG[tostring(v)]] = true end
    end

    -- allow getCapabilities
    if args['request'] == 'getcapabilities' or args['request'] == nil then
        reqAllowed = true
    end

    -- wms and wmts
    if args['layer'] then
        if ALLOWED[args['layer']] then
            reqAllowed = true
        else
            addHeader("no-permission-for-layer: "..args['layer'])
        end
    end

    -- wms getMap
    if args['layers'] then
        local strList = args['layers']
        local requested = split(strList, ",")
        local onlyAllowed = filter(inAllowed, requested)
        local countOfAllowed = table.getn(onlyAllowed)

        if countOfAllowed >= 1 then
            reqAllowed = true
            if table.getn(requested) ~= countOfAllowed then
                -- modify the nginx args sent to backend mapserver where not allowed layers are removed
                local newLayers = table.concat(onlyAllowed, ",")
                args.layers = newLayers
                ngx.req.set_uri_args(args)
                addHeader("modified-layers-param-to: " ..newLayers)
            end
        else
            reqAllowed = false
            addHeader("no-permission-for-layer: " ..args['layers'])
        end
    end

    -- wfs 1.0
    if args['typename'] then
        -- remove namespace "geodata:"
        local layer = string.gsub(args['typename'], NAMESPACE, "")
        -- and check that it's allowed
        if ALLOWED[layer] then
            reqAllowed = true
        else
            addHeader("no-permission-for-layer: " ..layer)
        end
    end

    -- wfs 2.0
    if args['typenames'] then
        -- remove namespace "geodata:"
        local layer = string.gsub(args['typenames'], NAMESPACE, "")
        -- and check that it's allowed
        if ALLOWED[layer] then
            reqAllowed = true
        else
            addHeader("no-permission-for-layer: "..layer)
        end
    end

    -- 403 or continue request to backends
    if reqAllowed == false then
        ngx.exit(403)
    end

end

function authenticate(bag_token, arg_maptoken, http_auth, args)

    local token = nil

    if bag_token then token = bag_token end                 -- token as header
    if arg_maptoken then token = arg_maptoken end           -- token as url param
    if http_auth then token = convertBasic(http_auth) end   -- token converted from basic auth

    -- we got some token 
    if token and token ~= "" then
        -- convert to object and check if valid and verified
        local jwt_obj = jwt:verify(SECRET, token)
        if jwt_obj["verified"] and jwt_obj["valid"] then
            -- we have valid token with payload, so authorize request
            authorize(jwt_obj["payload"], args)
        else
            addHeader("not-valid-token")
            ngx.exit(401)
        end
    else
        addHeader("no-auth-provided")
        ngx.exit(401)
    end
end

---------------------------------------------------------------
-- Authenticate and Authorize request
---------------------------------------------------------------

-- possible auth methods
m1 = ngx.req.get_headers()["Bag-Map-Token"]
m2 = ngx.var.arg_maptoken
m3 = ngx.var.http_authorization

authenticate(m1, m2, m3, ngx.req.get_uri_args())