-- if we have maptoken as argument
if ngx.var.arg_maptoken and ngx.var.arg_maptoken ~= "" then
    -- lowercase args
    local args = {}
    for k, v in pairs(ngx.req.get_uri_args()) do args[k:lower()] = v:lower() end
    -- only if request is getCapabilities
    if args['request'] == 'getcapabilities' then
        if args['service'] == "wms" or args['service'] == "wfs" then
            -- modify content lenght header
            ngx.header["Content-Length"] = nil
        end
    end
end