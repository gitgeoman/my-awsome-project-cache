#!/bin/bash -e

# replace mapserver proxy url in mapfiles
for file in /opt/mapserver/*.map; do
    if [ -f $file ]; then
        sed -i 's#http://maps.bitcomp.dev#'"${OWS_PROXY_URL_DOMAIN}"'#' $file
    fi
done

# replace mapcache proxy url and mapcache cache location in mapcache files
for file in /opt/mapcache/*.xml; do
    if [ -f $file ]; then
        sed -i 's#http://tiles.bitcomp.dev#'"${WMTS_PROXY_URL_DOMAIN}"'#' $file
        sed -i 's#<cache>dev</cache>#<cache>'"${TARGET_ENV}"'</cache>#g' $file
        sed -i 's#^.*remove in '"${TARGET_ENV}"'.*$##g' $file
        sed -i 's#^.*comment start dev.*$#  <!--#g' $file
        sed -i 's#^.*comment end dev.*$#  -->#g' $file
    fi
done

exec "$@"